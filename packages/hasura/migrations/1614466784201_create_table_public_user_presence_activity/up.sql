create table "public"."user_presence_activity"
(
    id               uuid default gen_random_uuid() not null
        constraint user_presence_activity_pkey
            primary key,
    user_presence_id uuid                           not null
        constraint user_presence_activity_user_presence_id_fkey
            references user_presence
            on update restrict on delete restrict,
    application_id   text,
    created_at       timestamp with time zone       not null,
    details          text,
    emoji            text,
    name             text                           not null,
    party_id         text,
    party_size       integer,
    state            text,
    start            timestamp with time zone,
    "end"            timestamp with time zone,
    type             text                           not null
);
