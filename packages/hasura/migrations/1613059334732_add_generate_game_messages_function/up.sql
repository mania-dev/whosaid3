CREATE FUNCTION generate_game_messages(guild text, min_length integer, n_questions integer)
    RETURNS SETOF message AS
$$
    BEGIN
    RETURN QUERY
    SELECT m.*
    FROM message m
             JOIN channel c on m.channel_id = c.id
             JOIN game_player gp
                  ON m.user_id = gp.user_id
                      AND c.guild_id = gp.guild_id
    WHERE c.guild_id = guild
      AND length(content) > min_length
    ORDER BY random()
    LIMIT n_questions;

    END;
    $$ LANGUAGE plpgsql STABLE;
