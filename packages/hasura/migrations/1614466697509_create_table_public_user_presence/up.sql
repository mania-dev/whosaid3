create table "public"."user_presence"
(
    user_id               text                                               not null
        constraint user_presence_user_id_fkey
            references "user"
            on update restrict on delete restrict,
    id                    uuid                     default gen_random_uuid() not null
        constraint user_presence_pkey
            primary key,
    client_status_web     text,
    guild_id              text,
    status                text                                               not null,
    client_status_mobile  text,
    client_status_desktop text,
    timestamp             timestamp with time zone default now()
);
