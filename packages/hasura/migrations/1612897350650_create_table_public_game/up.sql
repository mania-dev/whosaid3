CREATE EXTENSION IF NOT EXISTS pgcrypto;
CREATE TABLE "public"."game"("id" uuid NOT NULL DEFAULT gen_random_uuid(), "channel_id" text NOT NULL, "started_at" timestamptz NOT NULL, "finished" boolean NOT NULL, PRIMARY KEY ("id") , FOREIGN KEY ("channel_id") REFERENCES "public"."channel"("id") ON UPDATE restrict ON DELETE restrict);
