import { Controller, Get } from '@nestjs/common'

@Controller()
export class TestController {
  @Get('/')
  async test(): Promise<string> {
    return 'Hello'
  }
}
