import { buildClientSchema, IntrospectionQuery } from 'graphql'
import * as introspectionResult from './generated/schema.json'

export const schema = buildClientSchema(
  (introspectionResult as unknown) as IntrospectionQuery,
)
