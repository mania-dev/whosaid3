import { gql } from 'graphql-tag'

export const InsertChannel = gql`
  mutation InsertChannel($channel_insert_input: channel_insert_input!) {
    insert_channel_one(object: $channel_insert_input) {
      id
    }
  }
`
