import { gql } from 'graphql-tag'

export const InsertMember = gql`
  mutation InsertMember($member_insert_input: member_insert_input!) {
    insert_member_one(object: $member_insert_input) {
      guild_id
      user_id
    }
  }
`
