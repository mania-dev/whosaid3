import { gql } from 'graphql-tag'

export const GetUserById = gql`
  query GetUserById($id: String!) {
    user_by_pk(id: $id) {
      id
      avatar
      bot
      locale
      username
      tag
    }
  }
`
