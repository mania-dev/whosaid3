import { gql } from 'graphql-tag'

export const GetChannels = gql`
  query GetChannels {
    channel {
      id
    }
  }
`
