import { gql } from 'graphql-tag'

export const GetGuildById = gql`
  query getGuildById($id: String!) {
    guild_by_pk(id: $id) {
      id
    }
  }
`
