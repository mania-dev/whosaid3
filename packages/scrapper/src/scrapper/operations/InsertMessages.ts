import { gql } from 'graphql-tag'

export const InsertMessages = gql`
  mutation InsertMessages($messages: [message_insert_input!]!) {
    insert_message(objects: $messages) {
      affected_rows
    }
  }
`
