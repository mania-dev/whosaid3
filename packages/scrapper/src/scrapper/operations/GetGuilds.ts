import { gql } from 'graphql-tag'

export const GetGuilds = gql`
  query GetGuilds {
    guild {
      id
      joined_at
      name
      owner_id
    }
  }
`
