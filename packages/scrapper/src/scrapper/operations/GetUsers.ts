import { gql } from 'graphql-tag'

export const GetUsers = gql`
  query GetUsers {
    user {
      id
      avatar
      bot
      locale
      username
      tag
    }
  }
`
