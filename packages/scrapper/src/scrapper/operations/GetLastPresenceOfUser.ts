import { gql } from 'graphql-tag'

export const GetLastPresenceOfUser = gql`
  query GetLastPresenceOfUser($user_id: String!) {
    user_presence(
      where: { user_id: { _eq: $user_id } }
      limit: 1
      order_by: { timestamp: desc }
    ) {
      id
      activities {
        application_id
        created_at
        details
        emoji
        end
        id
        name
        party_id
        party_size
        start
        state
        type
      }
      client_status_desktop
      client_status_mobile
      client_status_web
      guild_id
      id
      status
      timestamp
      user_id
    }
  }
`
