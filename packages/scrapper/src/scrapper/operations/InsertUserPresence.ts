import { gql } from 'graphql-tag'

export const InsertUserPresence = gql`
  mutation InsertUserPresence($user_presence_input: user_presence_insert_input!) {
    insert_user_presence_one(object: $user_presence_input) {
      id
    }
  }
`
