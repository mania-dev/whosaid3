import { gql } from 'graphql-tag'

export const GetChannelNewestMessage = gql`
  query getChannelNewestMessage($channel_id: String!) {
    message(
      limit: 1
      where: { channel_id: { _eq: $channel_id } }
      order_by: { message_created_at: desc }
    ) {
      id
      message_created_at
    }
  }
`
