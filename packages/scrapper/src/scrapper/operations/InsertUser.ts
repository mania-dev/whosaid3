import { gql } from 'graphql-tag'

export const InsertUser = gql`
  mutation InsertUser($user_insert_input: user_insert_input!) {
    insert_user_one(object: $user_insert_input) {
      id
    }
  }
`
