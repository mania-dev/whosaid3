import { gql } from 'graphql-tag'

export const GetMemberById = gql`
  query getMemberById($user_id: String!, $guild_id: String!) {
    member_by_pk(user_id: $user_id, guild_id: $guild_id) {
      guild_id
      user_id
      nickname
      member_joined_at
    }
  }
`
