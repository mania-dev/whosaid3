import { gql } from 'graphql-tag'

export const GetChannelById = gql`
  query getChannelById($id: String!) {
    channel_by_pk(id: $id) {
      id
    }
  }
`
