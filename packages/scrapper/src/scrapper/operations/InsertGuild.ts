import { gql } from 'graphql-tag'

export const InsertGuild = gql`
  mutation InsertGuild($guild_insert_input: guild_insert_input!) {
    insert_guild_one(object: $guild_insert_input) {
      id
    }
  }
`
