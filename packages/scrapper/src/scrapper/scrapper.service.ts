import {Inject, Injectable, OnModuleInit} from '@nestjs/common'
import {Client, Presence, TextChannel} from 'discord.js'
import {LoggerService} from '@whosaid/common'
import {ScrapperDto} from './scrapper.dto'
import {Interval} from '@nestjs/schedule'
import {GetLastPresenceOfUserQuery} from '../generated/graphql'

@Injectable()
export class ScrapperService implements OnModuleInit {
  private isCurrentlyUpdatingData = false

  constructor(
    private readonly logger: LoggerService,
    private readonly scrapperDto: ScrapperDto,
    @Inject('DISCORD_CLIENT') private readonly client: Client,
  ) {}

  onModuleInit(): void {
    this.updateData()
  }

  @Interval(1000 * 5 * 60) // Every 5 minutes
  updateData(): void {
    this.logger.log(`Update data`)
    if (!this.client.readyAt) {
      this.client.on('ready', () => this.doUpdateData())
    } else {
      this.doUpdateData()
    }
  }

  public async doUpdateData(): Promise<void> {
    if (this.isCurrentlyUpdatingData) {
      this.logger.log(`Update running, skipping`)
      return
    }
    this.isCurrentlyUpdatingData = true
    this.logger.log(`Client was ready at ${this.client.readyAt}`)
    this.logger.log(
      `Scrapper has started, with ${this.client.users.cache.size} users, in ${this.client.channels.cache.size} channels of ${this.client.guilds.cache.size} guilds.`,
    )

    let newUsers = 0,
      newGuilds = 0,
      newMembers = 0,
      newChannels = 0,
      updateUsersPresencesData = 0,
      newMessages = 0

    try {
      newUsers = await this.updateUsersData()
      newGuilds = await this.updateGuildsData()
      newMembers = await this.updateMembersData()
      newChannels = await this.updateChannelsData()
      newMessages = await this.updateMessagesData()
      updateUsersPresencesData = await this.updateUsersPresencesData()
    } catch (err) {
      this.logger.error(`Error while updating data: ${err.toString()}`, '')
    }

    this.logger.log(
      `Scrapper has ended: ${JSON.stringify({
        newUsers,
        newMembers,
        newGuilds,
        newChannels,
        newMessages,
        updateUsersPresencesData,
      })}`,
    )

    this.isCurrentlyUpdatingData = false
  }

  public async updateUsersData(): Promise<number> {
    this.logger.log(`ScrapperService - updateUsersData`)
    let scrappedUsers = 0
    const discordUsers = this.client.users.cache.values()

    for (const user of discordUsers) {
      const foundUser = await this.scrapperDto.getUserById(user.id)
      if (foundUser) {
        this.logger.debug(
          `User ${user.tag} already exist in database, skipping`,
        )
        continue
      }

      await this.scrapperDto.insertUser({
        id: user.id,
        avatar: user.avatar,
        bot: user.bot,
        locale: user.locale,
        username: user.username,
        tag: user.tag,
        user_created_at: user.createdAt,
      })
      scrappedUsers++
    }

    return scrappedUsers
  }

  private hasPresenceChanged(
    oldPresence: GetLastPresenceOfUserQuery['user_presence'][0],
    newPresence: Presence,
  ): boolean {
    if (!oldPresence) {
      this.logger.debug('No old presence found. Set as changed')
      return true
    }

    if (
      oldPresence.client_status_web !== (newPresence.clientStatus?.web ?? null)
    ) {
      this.logger.debug('Web client status changed.')
      return true
    }

    if (
      oldPresence.client_status_desktop !==
      (newPresence.clientStatus?.desktop ?? null)
    ) {
      this.logger.debug('Desktop client status changed.')
      return true
    }

    if (
      oldPresence.client_status_mobile !==
        (newPresence.clientStatus?.mobile ?? null) ||
      oldPresence.status !== newPresence.status
    ) {
      this.logger.debug('Mobile client status changed.')
      return true
    }

    if (
      oldPresence.activities.length !==
      newPresence.activities.filter(
        (activity) => activity.type !== 'CUSTOM_STATUS',
      ).length
    ) {
      this.logger.debug(
        `Activities changed from ${oldPresence.activities.length} to ${newPresence.activities.length}`,
      )
      return true
    }

    for (const oldActivity of oldPresence.activities) {
      const newActivity = newPresence.activities.find(
        (activity) => activity.name === oldActivity.name,
      )

      if (!newActivity) {
        this.logger.debug(
          `Cannot find activity ${oldActivity.name} anymore. Set as changed`,
        )
        return true
      }

      if (oldActivity.application_id !== (newActivity.applicationID ?? null)) {
        this.logger.debug(
          `Activity ${oldActivity.name} applicationID has changed. Set as changed`,
        )
        return true
      }

      if (
        (oldActivity.created_at.getTime() ?? null) !==
        (new Date(newActivity.createdAt).getTime() ?? null)
      ) {
        this.logger.debug(
          `Activity ${oldActivity.name} createdAt has changed. Set as changed`,
        )
        return true
      }

      if (oldActivity.details !== (newActivity.details ?? null)) {
        this.logger.debug(
          `Activity ${oldActivity.name} details has changed. Set as changed`,
        )
        return true
      }

      if (oldActivity.emoji !== (newActivity.emoji?.toString() ?? null)) {
        this.logger.debug(
          `Activity ${oldActivity.name} emoji has changed. Set as changed`,
        )
        return true
      }

      if (oldActivity.party_id !== (newActivity.party?.id ?? null)) {
        this.logger.debug(
          `Activity ${oldActivity.name} party id has changed. Set as changed`,
        )
        return true
      }

      if (oldActivity.party_size !== (newActivity.party?.size?.[0] ?? null)) {
        this.logger.debug(
          `Activity ${oldActivity.name} party size has changed. Set as changed`,
        )
        return true
      }

      if (oldActivity.state !== (newActivity.state ?? null)) {
        this.logger.debug(
          `Activity ${oldActivity.name} state has changed. Set as changed`,
        )
        return true
      }

      if (
        (oldActivity.start?.getTime() ?? null) !==
        (newActivity.timestamps?.start
          ? new Date(newActivity.timestamps?.start).getTime()
          : null)
      ) {
        this.logger.debug(
          `Activity ${oldActivity.name} start timestamp has changed. Set as changed`,
        )
        return true
      }

      if (
        (oldActivity.end?.getTime() ?? null) !==
        (newActivity.timestamps?.end
          ? new Date(newActivity.timestamps?.end).getTime()
          : null)
      ) {
        this.logger.debug(
          `Activity ${oldActivity.name} end timestamp has changed. Set as changed`,
        )
        return true
      }

      if (oldActivity.type !== (newActivity.type ?? null)) {
        this.logger.debug(
          `Activity ${oldActivity.name} type has changed. Set as changed`,
        )
        return true
      }
    }
    return false
  }

  public async updateUsersPresencesData(): Promise<number> {
    this.logger.log(`ScrapperService - updateUsersPresencesData`)
    let scrappedPresences = 0
    const discordUsers = this.client.users.cache.values()

    for (const user of discordUsers) {
      const foundUser = await this.scrapperDto.getUserById(user.id)
      if (!foundUser) {
        this.logger.debug(
          `User ${user.tag} does not exist in database, skipping`,
        )
        continue
      }

      if (user.bot) {
        continue
      }

      const lastPresence = await this.scrapperDto.getLastPresenceOfUser(user.id)

      if (!this.hasPresenceChanged(lastPresence, user.presence)) {
        this.logger.debug(`${user.username} is still doing the same thing`)
        continue
      }

      this.logger.log(`${user.username} presence changed`)

      await this.scrapperDto.insertUserPresence({
        user_id: user.id,
        guild_id: user.presence.guild?.id,
        client_status_web: user.presence.clientStatus?.web,
        client_status_desktop: user.presence.clientStatus?.desktop,
        client_status_mobile: user.presence.clientStatus?.mobile,
        activities: {
          data: user.presence.activities
            .filter((activity) => activity.type !== 'CUSTOM_STATUS')
            .map((activity) => ({
              application_id: activity.applicationID,
              created_at: activity.createdAt,
              details: activity.details,
              emoji: activity.emoji?.toString(),
              name: activity.name,
              party_id: activity.party?.id,
              party_size: activity.party?.size?.[0],
              state: activity.state,
              start: activity.timestamps?.start,
              end: activity.timestamps?.end,
              type: activity.type,
            })),
        },
        status: user.presence.status,
      })
      scrappedPresences++
    }

    return scrappedPresences
  }

  public async updateGuildsData(): Promise<number> {
    this.logger.log(`ScrapperService - updateGuildsData`)
    let scrappedGuilds = 0

    for (const guild of this.client.guilds.cache.values()) {
      const foundGuild = await this.scrapperDto.getGuildById(guild.id)
      if (foundGuild) {
        this.logger.debug(
          `Guild ${guild.name} already exist in database, skipping`,
        )
        continue
      }

      const owner = await this.scrapperDto.getUserById(guild.ownerID)

      if (!owner) {
        this.logger.warn(
          `Could not find owner of guild ${guild.name} with id: ${guild.ownerID}`,
        )
      }

      await this.scrapperDto.insertGuild({
        guild_created_at: guild.createdAt,
        id: guild.id,
        joined_at: guild.joinedAt,
        name: guild.name,
        owner_id: guild.ownerID,
        region: guild.region,
      })
      scrappedGuilds++
    }
    return scrappedGuilds
  }

  public async updateMembersData(): Promise<number> {
    this.logger.log(`ScrapperService - updateMembersData`)
    let scrappedMembers = 0

    for (const guild of this.client.guilds.cache.values()) {
      for (const member of guild.members.cache.values()) {
        const foundMember = await this.scrapperDto.getMemberById(
          guild.id,
          member.id,
        )
        if (foundMember) {
          this.logger.debug(
            `Member ${member.user.username} already exist for guild ${guild.name} in database, skipping`,
          )
          continue
        }

        await this.scrapperDto.insertMember({
          member_joined_at: member.joinedAt,
          guild_id: guild.id,
          user_id: member.id,
          display_name: member.displayName,
          nickname: member.nickname,
        })
        scrappedMembers++
      }
    }
    return scrappedMembers
  }

  public async updateChannelsData(): Promise<number> {
    this.logger.log(`ScrapperService - updateChannelsData`)
    let scrappedChannels = 0

    for (const channel of this.client.channels.cache.values()) {
      if (!(channel instanceof TextChannel)) {
        this.logger.debug(`Channel ${channel.id} is not text based, skipping`)
        continue
      }
      const foundChannel = await this.scrapperDto.getChannelById(channel.id)
      if (foundChannel) {
        this.logger.debug(
          `Channel ${channel.name} already exist in database, skipping`,
        )
        continue
      }

      await this.scrapperDto.insertChannel({
        channel_created_at: channel.createdAt,
        guild_id: channel.guild.id,
        id: channel.id,
        name: channel.name,
        nsfw: channel.nsfw,
        topic: channel.topic,
        type: channel.type,
      })
      scrappedChannels++
    }

    return scrappedChannels
  }

  public async updateMessagesData(): Promise<number> {
    this.logger.log(`ScrapperService - updateMessagesData`)
    let scrappedMessages = 0

    for (const channel of this.client.channels.cache.values()) {
      if (!(channel instanceof TextChannel)) {
        this.logger.debug(`Channel ${channel.id} is not text based, skipping`)
        continue
      }

      if (!channel.viewable) {
        this.logger.debug(`Channel ${channel.name} is viewable, skipping`)
        continue
      }

      // Loop backward
      while (true) {
        const oldestChannelMessage = await this.scrapperDto.getOldestMessageForChannel(
          channel.id,
        )

        this.logger.verbose(
          oldestChannelMessage
            ? `Oldest message for channel ${channel.name} of guild ${channel.guild.name} was at ${oldestChannelMessage.message_created_at}`
            : `No saved message for channel ${channel.name} of guild ${channel.guild.name}`,
        )

        const messages = await channel.messages.fetch({
          limit: 100,
          before: oldestChannelMessage?.id,
        })

        const messagesArray = messages.array()

        if (messagesArray.length === 0) {
          break
        }

        const insertResult = await this.scrapperDto.insertMessages(
          messagesArray.map((msg) => ({
            channel_id: msg.channel.id,
            content: msg.cleanContent,
            id: msg.id,
            message_created_at: msg.createdAt,
            user_id: msg.member?.id,
          })),
        )
        scrappedMessages += insertResult?.affected_rows || 0
      }

      // Loop forward
      while (true) {
        const newestChannelMessage = await this.scrapperDto.getNewestMessageForChannel(
          channel.id,
        )

        this.logger.debug(
          newestChannelMessage
            ? `Newest message for channel ${channel.name} of guild ${channel.guild.name} was at ${newestChannelMessage.message_created_at}`
            : `No saved message for channel ${channel.name} of guild ${channel.guild.name}`,
        )

        const messages = await channel.messages.fetch({
          limit: 100,
          after: newestChannelMessage?.id,
        })

        const messagesArray = messages.array()

        if (messagesArray.length === 0) {
          break
        }

        const insertResult = await this.scrapperDto.insertMessages(
          messagesArray.map((msg) => ({
            channel_id: msg.channel.id,
            content: msg.cleanContent,
            id: msg.id,
            message_created_at: msg.createdAt,
            user_id: msg.member?.id,
          })),
        )
        scrappedMessages += insertResult?.affected_rows || 0
      }
    }
    return scrappedMessages
  }
}
