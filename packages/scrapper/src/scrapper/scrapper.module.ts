import { Module } from '@nestjs/common'
import { ScrapperService } from './scrapper.service'
import { ScrapperDto } from './scrapper.dto'
import { DiscordModule, HasuraModule } from '@whosaid/common'
import { ScheduleModule } from '@nestjs/schedule'

@Module({
  imports: [DiscordModule, HasuraModule, ScheduleModule.forRoot()],
  providers: [ScrapperService, ScrapperDto],
  exports: [ScrapperService],
})
export class ScrapperModule {}
