import { ApolloClient, NormalizedCacheObject } from '@apollo/client/core'
import { Injectable } from '@nestjs/common'
import { HasuraClient, LoggerService } from '@whosaid/common'
import {
  GetChannelById,
  GetChannelNewestMessage,
  GetChannelOldestMessage,
  GetGuildById,
  GetMemberById,
  InsertChannel,
  InsertGuild,
  InsertMember,
  InsertMessages,
  InsertUser,
} from './operations'
import {
  GetChannelByIdQuery,
  GetChannelByIdQueryVariables,
  GetChannelNewestMessageQuery,
  GetChannelNewestMessageQueryVariables,
  GetChannelOldestMessageQuery,
  GetChannelOldestMessageQueryVariables,
  GetChannelsQuery,
  GetChannelsQueryVariables,
  GetGuildByIdQuery,
  GetGuildByIdQueryVariables,
  GetLastPresenceOfUserQuery,
  GetLastPresenceOfUserQueryVariables,
  GetMemberByIdQuery,
  GetMemberByIdQueryVariables,
  GetUserByIdQuery,
  GetUserByIdQueryVariables,
  InsertChannelMutation,
  InsertChannelMutationVariables,
  InsertGuildMutation,
  InsertGuildMutationVariables,
  InsertMemberMutation,
  InsertMemberMutationVariables,
  InsertMessagesMutation,
  InsertMessagesMutationVariables,
  InsertUserMutation,
  InsertUserMutationVariables,
  InsertUserPresenceMutation,
  InsertUserPresenceMutationVariables,
} from '../generated/graphql'
import { GetUserById } from './operations/GetUserById'
import { GetChannels } from './operations/GetChannels'
import { InsertUserPresence } from './operations/InsertUserPresence'
import { GetLastPresenceOfUser } from './operations/GetLastPresenceOfUser'
import { schema } from '../schema'

@Injectable()
export class ScrapperDto {
  private readonly client: ApolloClient<NormalizedCacheObject>
  constructor(
    private readonly hasuraClient: HasuraClient,
    private readonly loggerService: LoggerService,
  ) {
    this.client = this.hasuraClient.getClient({
      clientName: 'scrapper',
      schema,
    })
  }

  public async insertGuild(
    input: InsertGuildMutationVariables['guild_insert_input'],
  ): Promise<InsertGuildMutation['insert_guild_one']> {
    this.loggerService.debug(
      `ScrapperDto - insertGuild - input ${JSON.stringify(input)}`,
    )

    const result = await this.client.mutate<
      InsertGuildMutation,
      InsertGuildMutationVariables
    >({
      mutation: InsertGuild,
      variables: {
        guild_insert_input: input,
      },
    })

    this.loggerService.debug(
      `ScrapperDto - insertGuild - return ${JSON.stringify(
        result.data?.insert_guild_one,
      )}`,
    )

    return result.data?.insert_guild_one
  }

  public async getGuildById(
    id: string,
  ): Promise<GetGuildByIdQuery['guild_by_pk']> {
    this.loggerService.debug(`ScrapperDto - getGuildById ${id}`)
    const result = await this.client.query<
      GetGuildByIdQuery,
      GetGuildByIdQueryVariables
    >({
      query: GetGuildById,
      variables: { id },
    })

    this.loggerService.debug(
      `ScrapperDto - getUserById - return ${JSON.stringify(
        result.data.guild_by_pk,
      )}`,
    )
    return result.data.guild_by_pk
  }

  public async getUserById(
    id: string,
  ): Promise<GetUserByIdQuery['user_by_pk']> {
    this.loggerService.debug(`ScrapperDto - getUserById ${id}`)
    const result = await this.client.query<
      GetUserByIdQuery,
      GetUserByIdQueryVariables
    >({
      query: GetUserById,
      variables: { id },
    })

    this.loggerService.debug(
      `ScrapperDto - getUserById - return ${JSON.stringify(
        result.data.user_by_pk,
      )}`,
    )
    return result.data.user_by_pk
  }

  public async insertUser(
    input: InsertUserMutationVariables['user_insert_input'],
  ): Promise<InsertUserMutation['insert_user_one']> {
    this.loggerService.debug(
      `ScrapperDto - insertUser - input ${JSON.stringify(input)}`,
    )

    const result = await this.client.mutate<
      InsertUserMutation,
      InsertUserMutationVariables
    >({
      mutation: InsertUser,
      variables: {
        user_insert_input: input,
      },
    })

    this.loggerService.debug(
      `ScrapperDto - insertUser - return ${JSON.stringify(
        result.data?.insert_user_one,
      )}`,
    )

    return result.data?.insert_user_one
  }

  public async insertUserPresence(
    input: InsertUserPresenceMutationVariables['user_presence_input'],
  ): Promise<InsertUserPresenceMutation['insert_user_presence_one']> {
    this.loggerService.debug(
      `ScrapperDto - insertUserPresence - input ${JSON.stringify(input)}`,
    )

    const result = await this.client.mutate<
      InsertUserPresenceMutation,
      InsertUserPresenceMutationVariables
    >({
      mutation: InsertUserPresence,
      variables: {
        user_presence_input: input,
      },
    })

    this.loggerService.debug(
      `ScrapperDto - insertUserPresence - return ${JSON.stringify(
        result.data?.insert_user_presence_one,
      )}`,
    )

    return result.data?.insert_user_presence_one
  }

  public async getLastPresenceOfUser(
    userId: string,
  ): Promise<GetLastPresenceOfUserQuery['user_presence'][0]> {
    this.loggerService.debug(`ScrapperDto - getLastPresenceOfUser ${userId}`)
    const result = await this.client.query<
      GetLastPresenceOfUserQuery,
      GetLastPresenceOfUserQueryVariables
    >({
      query: GetLastPresenceOfUser,
      variables: { user_id: userId },
    })

    this.loggerService.debug(
      `ScrapperDto - getLastPresenceOfUser - return ${JSON.stringify(
        result.data.user_presence,
      )}`,
    )
    return result.data.user_presence[0]
  }

  public async getChannelById(
    id: string,
  ): Promise<GetChannelByIdQuery['channel_by_pk']> {
    this.loggerService.debug(`ScrapperDto - getChannelById ${id}`)
    const result = await this.client.query<
      GetChannelByIdQuery,
      GetChannelByIdQueryVariables
    >({
      query: GetChannelById,
      variables: { id },
    })

    this.loggerService.debug(
      `ScrapperDto - getChannelById - return ${JSON.stringify(
        result.data.channel_by_pk,
      )}`,
    )
    return result.data.channel_by_pk
  }

  public async getChannels(): Promise<GetChannelsQuery['channel']> {
    this.loggerService.debug(`ScrapperDto - getChannels`)
    const result = await this.client.query<
      GetChannelsQuery,
      GetChannelsQueryVariables
    >({
      query: GetChannels,
    })

    this.loggerService.debug(
      `ScrapperDto - getChannelById - return ${JSON.stringify(
        result.data.channel,
      )}`,
    )
    return result.data.channel
  }

  public async insertChannel(
    input: InsertChannelMutationVariables['channel_insert_input'],
  ): Promise<InsertChannelMutation['insert_channel_one']> {
    this.loggerService.debug(
      `ScrapperDto - insertChannel - input ${JSON.stringify(input)}`,
    )

    const result = await this.client.mutate<
      InsertChannelMutation,
      InsertChannelMutationVariables
    >({
      mutation: InsertChannel,
      variables: {
        channel_insert_input: input,
      },
    })

    this.loggerService.debug(
      `ScrapperDto - insertChannel - return ${JSON.stringify(
        result.data?.insert_channel_one,
      )}`,
    )

    return result.data?.insert_channel_one
  }

  public async getMemberById(
    guildId: string,
    userId: string,
  ): Promise<GetMemberByIdQuery['member_by_pk']> {
    this.loggerService.debug(
      `ScrapperDto - getMemberById - guild: ${guildId}, user: ${userId}`,
    )
    const result = await this.client.query<
      GetMemberByIdQuery,
      GetMemberByIdQueryVariables
    >({
      query: GetMemberById,
      variables: {
        guild_id: guildId,
        user_id: userId,
      },
    })

    this.loggerService.debug(
      `ScrapperDto - getMemberById - return ${JSON.stringify(
        result.data.member_by_pk,
      )}`,
    )
    return result.data.member_by_pk
  }

  public async insertMember(
    input: InsertMemberMutationVariables['member_insert_input'],
  ): Promise<InsertMemberMutation['insert_member_one']> {
    this.loggerService.debug(
      `ScrapperDto - insertMember - input ${JSON.stringify(input)}`,
    )

    const result = await this.client.mutate<
      InsertMemberMutation,
      InsertMemberMutationVariables
    >({
      mutation: InsertMember,
      variables: {
        member_insert_input: input,
      },
    })

    this.loggerService.debug(
      `ScrapperDto - insertMember - return ${JSON.stringify(
        result.data?.insert_member_one,
      )}`,
    )

    return result.data?.insert_member_one
  }

  public async getOldestMessageForChannel(
    channelId: string,
  ): Promise<GetChannelOldestMessageQuery['message'][0] | null> {
    this.loggerService.debug(
      `ScrapperDto - getOldestMessageIdForChannel ${channelId}`,
    )

    const result = await this.client.query<
      GetChannelOldestMessageQuery,
      GetChannelOldestMessageQueryVariables
    >({
      query: GetChannelOldestMessage,
      variables: {
        channel_id: channelId,
      },
    })

    this.loggerService.debug(
      `ScrapperDto - getOldestMessageIdForChannel - return ${JSON.stringify(
        result.data.message[0] || null,
      )}`,
    )
    return result.data.message[0] || null
  }

  public async getNewestMessageForChannel(
    channelId: string,
  ): Promise<GetChannelNewestMessageQuery['message'][0] | null> {
    this.loggerService.debug(
      `ScrapperDto - getNewestMessageIdForChannel ${channelId}`,
    )

    const result = await this.client.query<
      GetChannelNewestMessageQuery,
      GetChannelNewestMessageQueryVariables
    >({
      query: GetChannelNewestMessage,
      variables: {
        channel_id: channelId,
      },
    })

    this.loggerService.debug(
      `ScrapperDto - getNewestMessageIdForChannel - return ${JSON.stringify(
        result.data.message[0] || null,
      )}`,
    )
    return result.data.message[0] || null
  }

  public async insertMessages(
    messages: InsertMessagesMutationVariables['messages'],
  ): Promise<InsertMessagesMutation['insert_message']> {
    this.loggerService.debug(`ScrapperDto - insertMessages`)

    const result = await this.client.mutate<
      InsertMessagesMutation,
      InsertMessagesMutationVariables
    >({
      mutation: InsertMessages,
      variables: {
        messages,
      },
    })

    this.loggerService.debug(
      `ScrapperDto - insertChannel - return ${JSON.stringify(
        result.data?.insert_message,
      )}`,
    )

    return result.data?.insert_message
  }
}
