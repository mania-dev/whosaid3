import { Module } from '@nestjs/common'
import { ScrapperModule } from './scrapper/scrapper.module'
import { TestController } from './test/test.controller'
import { LoggerModule } from '@whosaid/common'

@Module({
  imports: [ScrapperModule, LoggerModule],
  controllers: [TestController],
  providers: [],
})
export class AppModule {}
