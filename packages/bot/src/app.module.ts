import { Module } from '@nestjs/common'
import { BotModule } from './bot/bot.module'
import { TestController } from './test/test.controller'
import { LoggerModule } from '@whosaid/common'
import { GameModule } from './game/game.module'

@Module({
  imports: [BotModule, LoggerModule, GameModule],
  controllers: [TestController],
  providers: [],
})
export class AppModule {}
