import { Inject, Injectable, OnModuleInit } from '@nestjs/common'
import {
  Client,
  Message,
  MessageReaction,
  PartialUser,
  TextChannel,
  User,
} from 'discord.js'
import { LoggerService } from '@whosaid/common'
import { GameDto } from './game.dto'
import { GameService } from './game.service'
import minimist from 'minimist'

@Injectable()
export class GameInputsService implements OnModuleInit {
  constructor(
    private readonly logger: LoggerService,
    private readonly gameDto: GameDto,
    private readonly gameService: GameService,
    @Inject('DISCORD_CLIENT') private readonly client: Client,
  ) {}

  public onModuleInit(): void {
    const guildCount = this.client.guilds.cache.size
    this.logger.log(`Connected to ${guildCount} guilds`)
    this.client.on('message', async (message) => this.handleMessage(message))
    this.client.on('messageReactionAdd', async (reaction, user) =>
      this.handleReactionAdd(reaction, user),
    )
  }

  private async handleReactionAdd(
    reaction: MessageReaction,
    user: User | PartialUser,
  ): Promise<void> {
    if (reaction.me) {
      // ignore bot reactions
      return
    }

    if (!(reaction.message.channel instanceof TextChannel)) {
      this.logger.error(
        'GameInputsService - Channel is not a text channel, ignoring reaction',
      )
      return
    }

    this.logger.log(
      `GameInputsService - ${
        user.username
      } reacted with emoji ${reaction.emoji.toString()} to message ${
        reaction.message.id
      }`,
    )

    await this.gameService.addAnswer(
      reaction.message.channel.id,
      reaction.message.id,
      user.id,
      reaction.emoji.toString(),
      new Date(),
    )
  }

  private async handleMessage(message: Message): Promise<void> {
    const channel = message.channel

    if (!(channel instanceof TextChannel)) {
      this.logger.debug(
        `GameInputsService - Message ${message.id} was sent in channel ${channel.id} which is not a TextChannel, ignoring`,
      )
      return
    }

    if (message.author.bot) {
      this.logger.debug(`Message ${message.id} was sent by a bot, ignoring`)
      return
    }

    this.logger.log(
      `GameInputsService - Received message in channel ${channel.name} of guild ${channel.guild.name}: ${message.content}`,
    )

    if (message.content.startsWith('!w start')) {
      const command = message.content.replace('!w start', '')
      const args = minimist(command.split(' '), {
        default: {
          n: 5,
          'min-length': 20,
        },
      })

      this.logger.log(JSON.stringify(args))

      this.logger.log(`GameInputsService - Starting game`)
      await this.gameService.startGame(
        channel.guild.id,
        channel.id,
        args['min-length'],
        args.n,
      )
      message.reply('Starting game')
    }

    if (message.content.startsWith('!w emoji')) {
      const emoji = message.content.replace('!w emoji', '').trim()
      await this.gameService.setPlayer(
        channel.guild.id,
        message.author.id,
        emoji,
      )
      message.reply(`your emoji will be: ${emoji}`)
    }

    if (message.content === '!w') {
      message.channel.send(`Whosaid bot usage:
      
\`!w emoji\`
  Set your emoji
  Example: \`!w emoji :lj:\`

\`!w start\`
  Start a game
  Example: \`!w start -n 10 --min-length 100\`
      `)
    }
  }
}
