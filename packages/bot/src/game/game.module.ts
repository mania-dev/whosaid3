import { Module } from '@nestjs/common'
import { GameService } from './game.service'
import { HasuraModule, DiscordModule } from '@whosaid/common'
import { GameDto } from './game.dto'
import { GameInputsService } from './game.inputs.service'
import { GameOutputsService } from './game.outputs.service'

@Module({
  imports: [DiscordModule, HasuraModule],
  providers: [GameService, GameInputsService, GameOutputsService, GameDto],
  exports: [GameService],
})
export class GameModule {}
