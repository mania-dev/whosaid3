import { Inject, Injectable } from '@nestjs/common'
import { Client, Message, TextChannel } from 'discord.js'
import { LoggerService } from '@whosaid/common'
import { GameDto } from './game.dto'
import { flatMap } from 'rxjs/internal/operators'

@Injectable()
export class GameOutputsService {
  constructor(
    private readonly logger: LoggerService,
    private readonly gameDto: GameDto,
    @Inject('DISCORD_CLIENT') private readonly client: Client,
  ) {}

  public async sendGameFinished(
    channelId: string,
    scores: Record<string, number>,
  ): Promise<Message | undefined> {
    return await this.sendToChannel(
      channelId,
      `Game finished. Scores:
${Object.entries(scores)
  .sort(([, s1], [, s2]) => s2 - s1)
  .map(([user, score]) => `<@${user}>: ${score}\n`)}
    `,
    )
  }

  public async askQuestion(
    channelId: string,
    content: string,
  ): Promise<Message | undefined> {
    return await this.sendToChannel(channelId, `Question: ${content}`)
  }

  public async sendQuestionWon(
    channelId: string,
    winnerId: string,
    response: string | undefined,
  ): Promise<Message | undefined> {
    return await this.sendToChannel(
      channelId,
      `Won by <@${winnerId}>, response was: ${response}`,
    )
  }

  public async sendQuestionTimeout(
    channelId: string,
    response: string | undefined,
  ): Promise<Message | undefined> {
    return await this.sendToChannel(
      channelId,
      `Question has timeout, response was: ${response}`,
    )
  }

  public async sendToChannel(
    channelId: string,
    text: string,
  ): Promise<Message | undefined> {
    const channel = await this.client.channels.fetch(channelId)

    if (!channel) {
      this.logger.error('GameOutputsService - Game channel does not exist :/')
      return
    }

    if (!(channel instanceof TextChannel)) {
      this.logger.error('GameOutputsService - Channel is not a text channel')
      return
    }
    return channel.send(text)
  }
}
