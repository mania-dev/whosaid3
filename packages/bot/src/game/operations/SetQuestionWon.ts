import { gql } from 'graphql-tag'

export const SetQuestionWon = gql`
  mutation SetQuestionWon($id: uuid!) {
    update_game_question(
      where: { id: { _eq: $id }, won: { _eq: false } }
      _set: { won: true }
    ) {
      affected_rows
    }
  }
`
