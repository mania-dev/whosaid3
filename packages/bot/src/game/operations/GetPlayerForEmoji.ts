import { gql } from 'graphql-tag'

export const GetPlayerForEmoji = gql`
  query GetPlayerForEmoji($emoji: String!, $channel_id: String!) {
    game_player(
      where: {
        emoji: { _eq: $emoji }
        guild: { channels: { id: { _eq: $channel_id } } }
      }
    ) {
      id
      user_id
    }
  }
`
