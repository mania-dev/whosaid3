import { gql } from 'graphql-tag'

export const InsertGame = gql`
  mutation InsertGame($game: game_insert_input!) {
    insert_game_one(object: $game) {
      id
    }
  }
`
