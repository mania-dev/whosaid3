import { gql } from 'graphql-tag'

export const GetPlayersEmojis = gql`
  query GetPlayersEmojis($channel_id: String!) {
    game_player(where: { guild: { channels: { id: { _eq: $channel_id } } } }) {
      id
      user_id
      emoji
    }
  }
`
