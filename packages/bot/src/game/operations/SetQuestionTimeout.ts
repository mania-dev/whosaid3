import { gql } from 'graphql-tag'

export const SetQuestionTimeout = gql`
  mutation SetQuestionTimeout($id: uuid!) {
    update_game_question(
      where: { id: { _eq: $id }, timeout: { _eq: false } }
      _set: { timeout: true }
    ) {
      affected_rows
    }
  }
`
