import { gql } from 'graphql-tag'

export const SetPlayerEmoji = gql`
  mutation SetPlayerEmoji($object: game_player_insert_input!) {
    insert_game_player_one(
      object: $object
      on_conflict: {
        constraint: game_player_user_id_guild_id_key
        update_columns: emoji
      }
    ) {
      id
    }
  }
`
