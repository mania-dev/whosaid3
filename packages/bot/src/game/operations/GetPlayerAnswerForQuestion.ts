import { gql } from 'graphql-tag'

export const GetPlayerAnswerForQuestion = gql`
  query GetPlayerAnswerForQuestion($question_id: uuid!, $user_id: String!) {
    game_answer(
      where: { question_id: { _eq: $question_id }, user_id: { _eq: $user_id } }
    ) {
      id
      question_id
    }
  }
`
