import { gql } from 'graphql-tag'

export const SetQuestionAsked = gql`
  mutation SetQuestionAsked($asked_at: timestamptz!, $id: uuid!) {
    update_game_question(
      where: { _and: { id: { _eq: $id }, asked_at: { _is_null: true } } }
      _set: { asked_at: $asked_at }
    ) {
      affected_rows
    }
  }
`
