import { gql } from 'graphql-tag'

export const SetGameFinished = gql`
  mutation SetGameFinished($id: uuid!) {
    update_game(
      where: { id: { _eq: $id }, finished: { _eq: false } }
      _set: { finished: true }
    ) {
      affected_rows
    }
  }
`
