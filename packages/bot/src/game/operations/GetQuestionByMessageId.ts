import { gql } from 'graphql-tag'

export const GetQuestionByMessageId = gql`
  query GetQuestion($question_message_id: String!) {
    game_question(
      where: { question_message_id: { _eq: $question_message_id } }
    ) {
      id
    }
  }
`
