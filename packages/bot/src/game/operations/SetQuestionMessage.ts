import { gql } from 'graphql-tag'

export const SetQuestionMessageId = gql`
  mutation SetQuestionMessageId($id: uuid!, $message_id: String!) {
    update_game_question_by_pk(
      pk_columns: { id: $id }
      _set: { question_message_id: $message_id }
    ) {
      id
    }
  }
`
