import { gql } from 'graphql-tag'

export const AddAnswer = gql`
  mutation AddAnswer($answer: game_answer_insert_input!) {
    insert_game_answer_one(object: $answer) {
      id
    }
  }
`
