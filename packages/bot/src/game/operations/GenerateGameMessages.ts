import { gql } from 'graphql-tag'

export const GenerateGameMessages = gql`
  query GenerateGameMessages(
    $guild: String!
    $min_length: Int!
    $n_questions: Int!
  ) {
    generate_game_messages(
      args: {
        guild: $guild
        min_length: $min_length
        n_questions: $n_questions
      }
    ) {
      id
    }
  }
`
