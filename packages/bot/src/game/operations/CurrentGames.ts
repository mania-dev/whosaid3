import { gql } from 'graphql-tag'

export const CurrentGames = gql`
  subscription CurrentGames {
    game(where: { finished: { _eq: false } }) {
      id
      channel_id
      started_at
      questions {
        id
        won
        timeout
        asked_at
        message {
          content
          user {
            username
            id
          }
        }
        answers {
          id
          answered_at
          answer_user_id
          user {
            username
            id
          }
        }
      }
    }
  }
`
