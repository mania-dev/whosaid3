import { Injectable, OnModuleDestroy, OnModuleInit } from '@nestjs/common'
import { LoggerService } from '@whosaid/common'
import { GameDto } from './game.dto'
import { CurrentGamesSubscription } from '../generated/graphql'
import { Observable, ObservableSubscription } from '@apollo/client/utilities'
import { combineLatest } from 'zen-observable/extras'
import { GameOutputsService } from './game.outputs.service'

type CurrentQuestion = CurrentGamesSubscription['game'][0]['questions'][0] & {
  asked_at: Date
  timeout: true
}

type UnaskedQuestion = CurrentGamesSubscription['game'][0]['questions'][0] & {
  asked_at: null | undefined
  timeout: false
}

@Injectable()
export class GameService implements OnModuleInit, OnModuleDestroy {
  private gameSubscription: ObservableSubscription

  constructor(
    private readonly logger: LoggerService,
    private readonly gameDto: GameDto,
    private readonly gameOutputsService: GameOutputsService,
  ) {}

  public onModuleInit(): void {
    // Use an interval to run the game loop even if there is no game state update
    // The main reason is to detect question timeout, which does not produce a new value in the observable
    const interval = new Observable((observer) => {
      const _interval = setInterval(() => {
        observer.next(null)
      }, 1000)

      // On unsubscription, cancel the timer
      return () => {
        clearInterval(_interval)
      }
    })

    this.gameSubscription = combineLatest(
      this.gameDto.getCurrentGames(),
      interval,
    ).subscribe(
      ([games]: [CurrentGamesSubscription['game'] | undefined, unknown]) =>
        this.handleGames(games),
    )
  }

  public onModuleDestroy(): void {
    this.gameSubscription?.unsubscribe()
  }

  public async startGame(
    guildId: string,
    channelId: string,
    min_length: number,
    n: number,
  ): Promise<void> {
    const messages = await this.gameDto.generateGameMessages({
      guild: guildId,
      min_length,
      n_questions: n,
    })

    const messagesIds = messages.map((msg) => msg.id)

    this.logger.debug(
      `GameService - Game message generated ids: ${JSON.stringify(
        messagesIds,
      )}`,
    )

    const game = await this.gameDto.insertGame({
      game: {
        channel_id: channelId,
        questions: {
          data: messagesIds.map((id) => ({ message_id: id })),
        },
      },
    })

    this.logger.debug(`GameService - Generated game: ${JSON.stringify(game)}`)
  }

  public async addAnswer(
    channelId: string,
    messageId: string,
    userId: string,
    emoji: string,
    answeredAt: Date,
  ): Promise<void> {
    const questionResult = await this.gameDto.getQuestionByMessageId(messageId)

    if (!questionResult || !questionResult[0]) {
      this.logger.warn('GameService - No question for reaction')
      return
    }

    const question = questionResult[0]

    const playerEmojiResponse = await this.gameDto.getPlayerForEmoji(
      emoji,
      channelId,
    )

    if (!playerEmojiResponse || !playerEmojiResponse[0]) {
      this.logger.warn(
        `GameService - No player found that matched reaction ${emoji}`,
      )
      return
    }
    const playerEmoji = playerEmojiResponse[0]

    const playerAnswersForQuestion = await this.gameDto.getPlayerAnswerForQuestion(
      userId,
      question.id,
    )

    if (playerAnswersForQuestion.length > 0) {
      this.logger.warn(
        'GameService - Player have already put an answer for this question',
      )
      return
    }

    await this.gameDto.addAnswer({
      answered_at: answeredAt,
      question_id: question.id,
      user_id: userId,
      answer_user_id: playerEmoji.user_id,
    })
  }

  public async setPlayer(
    guildId: string,
    userId: string,
    emoji: string,
  ): Promise<void> {
    const player = await this.gameDto.setPlayerEmoji({
      guild_id: guildId,
      user_id: userId,
      emoji: emoji,
    })

    this.logger.debug(
      `GameService - Generated player: ${JSON.stringify(player)}`,
    )
  }

  private async handleGames(
    games: CurrentGamesSubscription['game'] | undefined,
  ): Promise<void> {
    if (!games) {
      this.logger.error(
        `GameService - Games object not defined, it should not happen `,
      )
      return
    }

    await Promise.all(games.map((game) => this.handleGame(game)))
  }

  private async handleGame(
    game: CurrentGamesSubscription['game'][0],
  ): Promise<void> {
    this.logger.log(`Handling game ${game.id}`)

    if (game.questions.length === 0) {
      this.logger.error('GameService - Game have no questions, set as finished')
      const result = await this.gameDto.setGameFinished(game.id)
      if (!result || result?.affected_rows === 0) {
        this.logger.log('GameService - Game already finished')
        return
      }
      return
    }

    const currentQuestion = this.getCurrentQuestion(game.questions)
    if (!currentQuestion) {
      await this.handleNoCurrentQuestion(game)
      return
    }

    const questionWinner = this.getQuestionWinner(currentQuestion)
    if (questionWinner) {
      await this.handleQuestionHasWinner(
        game.channel_id,
        currentQuestion,
        questionWinner,
      )
      return
    }

    const hasQuestionTimeout = this.hasQuestionTimeout(currentQuestion)
    if (hasQuestionTimeout) {
      await this.handleQuestionTimeout(game.channel_id, currentQuestion)
      return
    }
  }

  private async handleNoCurrentQuestion(
    game: CurrentGamesSubscription['game'][0],
  ): Promise<void> {
    const nextQuestions = game.questions.filter(
      (question): question is UnaskedQuestion =>
        !question.asked_at && !question.timeout && !question.won,
    )

    if (nextQuestions.length === 0) {
      await this.handleGameEnding(game)
      return
    }

    this.logger.log(`${nextQuestions.length} questions remaining`)
    await this.askQuestion(game.channel_id, nextQuestions[0])
  }

  private async handleQuestionHasWinner(
    channelId: string,
    currentQuestion: CurrentQuestion,
    winnerId: string,
  ): Promise<void> {
    this.logger.log(
      `GameService - Unused variable: currentQuestion ${JSON.stringify(
        currentQuestion,
      )}`,
    )

    const result = await this.gameDto.setQuestionWon(currentQuestion.id)
    if (!result || result?.affected_rows === 0) {
      this.logger.log('GameService - Response already won')
      return
    }

    await this.gameOutputsService.sendQuestionWon(
      channelId,
      winnerId,
      currentQuestion.message.user?.username,
    )
  }

  private async handleGameEnding(
    game: CurrentGamesSubscription['game'][0],
  ): Promise<void> {
    this.logger.log(`GameService - End of the game ${game.id}`)
    const result = await this.gameDto.setGameFinished(game.id)
    if (!result || result?.affected_rows === 0) {
      this.logger.log('GameService - Game already finished')
      return
    }
    const scores = {}

    for (const question of game.questions) {
      const winner = this.getQuestionWinner(question)
      if (!winner) {
        continue
      }
      scores[winner] = scores[winner] ? scores[winner] + 1 : 1
    }

    await this.gameOutputsService.sendGameFinished(game.channel_id, scores)
    return
  }

  private async handleQuestionTimeout(
    channelId: string,
    currentQuestion: CurrentQuestion,
  ): Promise<void> {
    const result = await this.gameDto.setQuestionTimeout(currentQuestion.id)
    if (!result || result?.affected_rows === 0) {
      this.logger.log('GameService - Response already timeout')
      return
    }

    await this.gameOutputsService.sendQuestionTimeout(
      channelId,
      currentQuestion.message.user?.username,
    )

    this.logger.log('GameService - Current question has timeout')
  }

  private getCurrentQuestion(
    questions: CurrentGamesSubscription['game'][0]['questions'],
  ): CurrentQuestion | null {
    const sortedAskedQuestions = questions
      .filter(
        (question): question is CurrentQuestion =>
          !!question.asked_at && !question.timeout && !question.won,
      )
      .sort((q1, q2) => q2.asked_at.getTime() - q1.asked_at.getTime())

    return sortedAskedQuestions[0] || null
  }

  private hasQuestionTimeout(currentQuestion: CurrentQuestion): boolean {
    const timeSinceQuestion =
      (Date.now() - currentQuestion.asked_at.getTime()) / 1000

    this.logger.log(
      `GameService - Time since question: ${timeSinceQuestion} seconds`,
    )
    return timeSinceQuestion > 20 // TODO: parameter
  }

  private getQuestionWinner(
    currentQuestion: CurrentGamesSubscription['game'][0]['questions'][0],
  ): string | null {
    this.logger.log(
      `GameService - current question answers: ${JSON.stringify(
        currentQuestion.answers,
      )}`,
    )

    const correctAnswers = currentQuestion.answers.filter(
      (answer) => answer.answer_user_id === currentQuestion.message.user?.id,
    )

    this.logger.log(
      `GameService - current question answers (correct only): ${JSON.stringify(
        correctAnswers,
      )}`,
    )

    const sortedCorrectAnswers = correctAnswers.sort(
      (a1, a2) => a1.answered_at.getTime() - a2.answered_at.getTime(),
    )

    this.logger.log(
      `GameService - current question answers (correct only, sorted): ${JSON.stringify(
        sortedCorrectAnswers,
      )}`,
    )

    if (sortedCorrectAnswers.length === 0) {
      return null
    }

    this.logger.log(
      `GameService - fastest current answer: ${sortedCorrectAnswers[0]}`,
    )

    return sortedCorrectAnswers[0].user.id
  }

  private async askQuestion(
    channelId: string,
    question: UnaskedQuestion,
  ): Promise<void> {
    const value = await this.gameDto.setQuestionAsked(question.id)
    if (!value || value?.affected_rows === 0) {
      this.logger.log('GameService - Question already asked')
      return
    }

    await new Promise((resolve) => setTimeout(resolve, 2000))

    const message = await this.gameOutputsService.askQuestion(
      channelId,
      question.message.content,
    )

    if (!message) {
      return
    }

    await this.gameDto.setQuestionMessageId(question.id, message.id)

    const playersEmojis = await this.gameDto.getPlayersEmojis(channelId)

    await Promise.all(
      playersEmojis.map((playerEmoji) => message.react(playerEmoji.emoji)),
    )
  }
}
