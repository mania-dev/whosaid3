import { ApolloClient, NormalizedCacheObject } from '@apollo/client/core'
import { Injectable } from '@nestjs/common'
import { HasuraClient, LoggerService } from '@whosaid/common'
import {
  AddAnswerMutation,
  AddAnswerMutationVariables,
  CurrentGamesSubscription,
  CurrentGamesSubscriptionVariables,
  GenerateGameMessagesQuery,
  GenerateGameMessagesQueryVariables,
  GetPlayerAnswerForQuestionQuery,
  GetPlayerAnswerForQuestionQueryVariables,
  GetPlayerForEmojiQuery,
  GetPlayerForEmojiQueryVariables,
  GetPlayersEmojisQuery,
  GetPlayersEmojisQueryVariables,
  GetQuestionQuery,
  GetQuestionQueryVariables,
  InsertGameMutation,
  InsertGameMutationVariables,
  SetGameFinishedMutation,
  SetGameFinishedMutationVariables,
  SetPlayerEmojiMutation,
  SetPlayerEmojiMutationVariables,
  SetQuestionAskedMutation,
  SetQuestionAskedMutationVariables,
  SetQuestionMessageIdMutation,
  SetQuestionMessageIdMutationVariables,
  SetQuestionTimeoutMutation,
  SetQuestionTimeoutMutationVariables,
  SetQuestionWonMutation,
  SetQuestionWonMutationVariables,
} from '../generated/graphql'
import { GenerateGameMessages } from './operations/GenerateGameMessages'
import { InsertGame } from './operations/InsertGame'
import { CurrentGames } from './operations/CurrentGames'
import { Observable } from '@apollo/client/utilities'
import { SetPlayerEmoji } from './operations/SetPlayerEmoji'
import { SetQuestionAsked } from './operations/SetQuestionAsked'
import { SetQuestionTimeout } from './operations/SetQuestionTimeout'
import { SetGameFinished } from './operations/SetGameFinished'
import { GetPlayersEmojis } from './operations/GetPlayersEmojis'
import { SetQuestionMessageId } from './operations/SetQuestionMessage'
import { GetQuestionByMessageId } from './operations/GetQuestionByMessageId'
import { AddAnswer } from './operations/AddAnswer'
import { SetQuestionWon } from './operations/SetQuestionWon'
import { GetPlayerForEmoji } from './operations/GetPlayerForEmoji'
import { GetPlayerAnswerForQuestion } from './operations/GetPlayerAnswerForQuestion'

@Injectable()
export class GameDto {
  private readonly client: ApolloClient<NormalizedCacheObject>
  constructor(
    private readonly hasuraClient: HasuraClient,
    private readonly loggerService: LoggerService,
  ) {
    this.client = hasuraClient.client
  }

  public async generateGameMessages(
    variables: GenerateGameMessagesQueryVariables,
  ): Promise<GenerateGameMessagesQuery['generate_game_messages']> {
    this.loggerService.verbose(
      `GameDto - generateGameMessages - variables: ${JSON.stringify(
        variables,
      )}`,
    )
    const result = await this.client.query<
      GenerateGameMessagesQuery,
      GenerateGameMessagesQueryVariables
    >({
      query: GenerateGameMessages,
      variables,
    })

    this.loggerService.verbose(
      `GameDto - generateGameMessages - return ${JSON.stringify(
        result.data?.generate_game_messages,
      )}`,
    )
    return result.data?.generate_game_messages || []
  }

  public async insertGame(
    variables: InsertGameMutationVariables,
  ): Promise<InsertGameMutation['insert_game_one'] | undefined> {
    this.loggerService.verbose(
      `GameDto - insertGame - variables: ${JSON.stringify(variables)}`,
    )
    const result = await this.client.mutate<
      InsertGameMutation,
      InsertGameMutationVariables
    >({
      mutation: InsertGame,
      variables,
    })

    this.loggerService.verbose(
      `GameDto - insertGame - return ${JSON.stringify(
        result.data?.insert_game_one,
      )}`,
    )
    return result.data?.insert_game_one
  }

  public getCurrentGames(): Observable<
    CurrentGamesSubscription['game'] | undefined
  > {
    this.loggerService.verbose(`GameDto - getCurrentGames`)

    const result = this.client.subscribe<
      CurrentGamesSubscription,
      CurrentGamesSubscriptionVariables
    >({
      query: CurrentGames,
    })

    return result.map((result) => result.data?.game)
  }

  public async getPlayersEmojis(
    channelId: string,
  ): Promise<GetPlayersEmojisQuery['game_player']> {
    this.loggerService.verbose(`GameDto - getPlayersEmojis`)

    const result = await this.client.query<
      GetPlayersEmojisQuery,
      GetPlayersEmojisQueryVariables
    >({
      query: GetPlayersEmojis,
      variables: {
        channel_id: channelId,
      },
    })

    this.loggerService.verbose(
      `GameDto - getPlayersEmojis - returns ${JSON.stringify(result)}`,
    )

    return result.data.game_player
  }

  public async getPlayerAnswerForQuestion(
    userId: string,
    questionId: string,
  ): Promise<GetPlayerAnswerForQuestionQuery['game_answer']> {
    this.loggerService.verbose(`GameDto - getPlayerAnswerForQuestion`)

    const result = await this.client.query<
      GetPlayerAnswerForQuestionQuery,
      GetPlayerAnswerForQuestionQueryVariables
    >({
      query: GetPlayerAnswerForQuestion,
      variables: {
        user_id: userId,
        question_id: questionId,
      },
    })

    this.loggerService.verbose(
      `GameDto - getPlayerAnswerForQuestion - returns ${JSON.stringify(
        result,
      )}`,
    )

    return result.data.game_answer
  }

  public async getPlayerForEmoji(
    emoji: string,
    channelId: string,
  ): Promise<GetPlayerForEmojiQuery['game_player']> {
    this.loggerService.verbose(
      `GameDto - getPlayerForEmoji - emoji ${emoji} - channelId ${channelId}`,
    )
    const result = await this.client.query<
      GetPlayerForEmojiQuery,
      GetPlayerForEmojiQueryVariables
    >({
      query: GetPlayerForEmoji,
      variables: {
        emoji,
        channel_id: channelId,
      },
    })

    this.loggerService.verbose(
      `GameDto - getPlayerForEmoji - returns ${JSON.stringify(result)}`,
    )

    return result.data.game_player
  }

  public async getQuestionByMessageId(
    messageId: string,
  ): Promise<GetQuestionQuery['game_question']> {
    this.loggerService.verbose(
      `GameDto - getQuestionByMessageId - ${messageId}`,
    )
    const result = await this.client.query<
      GetQuestionQuery,
      GetQuestionQueryVariables
    >({
      query: GetQuestionByMessageId,
      variables: {
        question_message_id: messageId,
      },
    })

    this.loggerService.verbose(
      `GameDto - getQuestionByMessageId - returns ${JSON.stringify(result)}`,
    )

    return result.data.game_question
  }

  public async setPlayerEmoji(
    input: SetPlayerEmojiMutationVariables['object'],
  ): Promise<SetPlayerEmojiMutation['insert_game_player_one'] | undefined> {
    this.loggerService.verbose(
      `GameDto - setPlayerEmoji - input: ${JSON.stringify(input)}`,
    )
    const result = await this.client.mutate<
      SetPlayerEmojiMutation,
      SetPlayerEmojiMutationVariables
    >({
      mutation: SetPlayerEmoji,
      variables: {
        object: input,
      },
    })

    this.loggerService.verbose(
      `GameDto - setPlayerEmoji - returns ${JSON.stringify(
        result.data?.insert_game_player_one,
      )}`,
    )
    return result.data?.insert_game_player_one
  }

  public async addAnswer(
    input: AddAnswerMutationVariables['answer'],
  ): Promise<AddAnswerMutation['insert_game_answer_one'] | undefined> {
    this.loggerService.verbose(
      `GameDto - addAnswer - input: ${JSON.stringify(input)}`,
    )
    const result = await this.client.mutate<
      AddAnswerMutation,
      AddAnswerMutationVariables
    >({
      mutation: AddAnswer,
      variables: {
        answer: input,
      },
    })

    this.loggerService.verbose(
      `GameDto - setPlayerEmoji - returns ${JSON.stringify(
        result.data?.insert_game_answer_one,
      )}`,
    )
    return result.data?.insert_game_answer_one
  }

  public async setQuestionAsked(
    questionId: string,
  ): Promise<SetQuestionAskedMutation['update_game_question'] | undefined> {
    this.loggerService.verbose(
      `GameDto - setQuestionAsked - questionId: ${questionId}`,
    )
    const result = await this.client.mutate<
      SetQuestionAskedMutation,
      SetQuestionAskedMutationVariables
    >({
      mutation: SetQuestionAsked,
      variables: {
        asked_at: new Date(),
        id: questionId,
      },
    })

    this.loggerService.verbose(
      `GameDto - setQuestionAsked - returns ${JSON.stringify(
        result.data?.update_game_question,
      )}`,
    )
    return result.data?.update_game_question
  }

  public async setQuestionMessageId(
    questionId: string,
    messageId: string,
  ): Promise<
    SetQuestionMessageIdMutation['update_game_question_by_pk'] | undefined
  > {
    this.loggerService.verbose(
      `GameDto - setQuestionMessageId - questionId: ${questionId}`,
    )
    const result = await this.client.mutate<
      SetQuestionMessageIdMutation,
      SetQuestionMessageIdMutationVariables
    >({
      mutation: SetQuestionMessageId,
      variables: {
        message_id: messageId,
        id: questionId,
      },
    })

    this.loggerService.verbose(
      `GameDto - setQuestionMessageId - returns ${JSON.stringify(
        result.data?.update_game_question_by_pk,
      )}`,
    )
    return result.data?.update_game_question_by_pk
  }

  public async setQuestionTimeout(
    questionId: string,
  ): Promise<SetQuestionTimeoutMutation['update_game_question'] | undefined> {
    this.loggerService.verbose(
      `GameDto - setQuestionTimeout - questionId: ${questionId}`,
    )
    const result = await this.client.mutate<
      SetQuestionTimeoutMutation,
      SetQuestionTimeoutMutationVariables
    >({
      mutation: SetQuestionTimeout,
      variables: {
        id: questionId,
      },
    })

    this.loggerService.verbose(
      `GameDto - setQuestionTimeout - returns ${JSON.stringify(
        result.data?.update_game_question,
      )}`,
    )
    return result.data?.update_game_question
  }

  public async setQuestionWon(
    questionId: string,
  ): Promise<SetQuestionWonMutation['update_game_question'] | undefined> {
    this.loggerService.verbose(
      `GameDto - setQuestionWon - questionId: ${questionId}`,
    )
    const result = await this.client.mutate<
      SetQuestionWonMutation,
      SetQuestionWonMutationVariables
    >({
      mutation: SetQuestionWon,
      variables: {
        id: questionId,
      },
    })

    this.loggerService.verbose(
      `GameDto - setQuestionWon - return ${JSON.stringify(
        result.data?.update_game_question,
      )}`,
    )
    return result.data?.update_game_question
  }

  public async setGameFinished(
    questionId: string,
  ): Promise<SetGameFinishedMutation['update_game'] | undefined> {
    this.loggerService.verbose(
      `GameDto - setGameFinished - questionId: ${questionId}`,
    )
    const result = await this.client.mutate<
      SetGameFinishedMutation,
      SetGameFinishedMutationVariables
    >({
      mutation: SetGameFinished,
      variables: {
        id: questionId,
      },
    })

    this.loggerService.verbose(
      `GameDto - setGameFinished - returns ${JSON.stringify(
        result.data?.update_game,
      )}`,
    )
    return result.data?.update_game
  }
}
