import { Module } from '@nestjs/common'
import { BotService } from './bot.service'
import { HasuraModule, DiscordModule } from '@whosaid/common'
import { BotDto } from './bot.dto'

@Module({
  imports: [DiscordModule, HasuraModule],
  providers: [BotService, BotDto],
  exports: [BotService],
})
export class BotModule {}
