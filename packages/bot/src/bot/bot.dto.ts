import { ApolloClient, NormalizedCacheObject } from '@apollo/client/core'
import { Injectable } from '@nestjs/common'
import { HasuraClient } from '@whosaid/common'
import { GetGuilds } from './operations/GetGuilds'
import { GetGuildsQuery, GetGuildsQueryVariables } from '../generated/graphql'

@Injectable()
export class BotDto {
  private readonly client: ApolloClient<NormalizedCacheObject>
  constructor(private readonly hasuraClient: HasuraClient) {
    this.client = hasuraClient.client
  }

  public async getGuilds(): Promise<void> {
    await this.client.query<GetGuildsQuery, GetGuildsQueryVariables>({
      query: GetGuilds,
      variables: {
        limit: 10,
      },
    })
  }
}
