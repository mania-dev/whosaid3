import { Inject, Injectable, OnModuleInit } from '@nestjs/common'
import { Client } from 'discord.js'
import { LoggerService } from '@whosaid/common'
import { BotDto } from './bot.dto'

@Injectable()
export class BotService {
  constructor(
    private readonly logger: LoggerService,
    private readonly botDto: BotDto,
    @Inject('DISCORD_CLIENT') private readonly client: Client,
  ) {}
}
