import { gql } from 'graphql-tag'

export const GetGuilds = gql`
  query GetGuilds($limit: Int) {
    guild(limit: $limit) {
      id
      joined_at
      name
      owner_id
    }
  }
`
