import { Module } from '@nestjs/common'
import { ConfigModule, LoggerModule } from '@whosaid/common'
import { AuthModule } from './auth/auth.module'
import { AppConfig } from './app.config'

@Module({
  imports: [AuthModule, LoggerModule, ConfigModule.forConfig(AppConfig)],
  controllers: [],
  providers: [],
})
export class AppModule {}
