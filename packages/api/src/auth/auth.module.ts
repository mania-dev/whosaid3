import { Module } from '@nestjs/common'
import { ConfigModule, LoggerModule } from '@whosaid/common'
import { AuthController } from './auth.controller'
import { AuthService } from './auth.service'
import { AuthConfig } from './auth.config'

@Module({
  imports: [LoggerModule, ConfigModule.forConfig(AuthConfig)],
  controllers: [AuthController],
  providers: [AuthService],
  exports: [],
})
export class AuthModule {}
