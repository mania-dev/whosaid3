import { Property } from 'ts-convict'

export class AuthConfig {
  @Property({
    format: String,
    default: null,
    env: 'DISCORD_CLIENT_ID',
  })
  clientId: string

  @Property({
    format: String,
    default: null,
    env: 'DISCORD_CLIENT_SECRET',
  })
  clientSecret: string

  @Property({
    format: String,
    default: null,
    env: 'DISCORD_ENDPOINT',
  })
  discordEndpoint: string

  @Property({
    format: String,
    default: null,
    env: 'REDIRECT_URI',
  })
  redirectUri: string

  @Property({
    format: String,
    default: null,
    env: 'DISCORD_SCOPE',
  })
  scope: string
}
