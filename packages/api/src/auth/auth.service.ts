import { Injectable, UnauthorizedException } from '@nestjs/common'
import { LoggerService } from '@whosaid/common'
import { AuthConfig } from './auth.config'
import { oauth } from './discord.auth'

export enum HasuraRole {
  User = 'user',
}

export interface HasuraAuth {
  'X-Hasura-User-Id': string
  'X-Hasura-Role': HasuraRole
  'X-Hasura-Is-Owner': 'true' | 'false'
}

@Injectable()
export class AuthService {
  constructor(
    private readonly logger: LoggerService,
    private readonly authConfig: AuthConfig,
  ) {}

  public async login(
    code: string,
  ): Promise<ReturnType<typeof oauth.tokenRequest>> {
    this.logger.verbose(`AuthService - login - code ${code}`)

    try {
      const response = await oauth.tokenRequest({
        clientId: this.authConfig.clientId,
        clientSecret: this.authConfig.clientSecret,
        code,
        grantType: 'authorization_code',
        redirectUri: this.authConfig.redirectUri,
        scope: this.authConfig.scope,
      })

      this.logger.verbose(`Response: ${JSON.stringify(response)}`)

      return response
    } catch (e) {
      this.logger.error(JSON.stringify(e))
      throw e
    }
  }

  public async getHasuraAuth(accessToken: string): Promise<HasuraAuth> {
    try {
      const user = await oauth.getUser(accessToken)

      return {
        'X-Hasura-User-Id': user.id,
        'X-Hasura-Is-Owner': 'false',
        'X-Hasura-Role': HasuraRole.User,
      }
    } catch (e) {
      this.logger.error(e.toString())
      throw new UnauthorizedException()
    }
  }
}
