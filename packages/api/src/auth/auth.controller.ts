import {Body, Controller, Get, Headers, Post} from '@nestjs/common'
import {LoggerService} from '@whosaid/common'
import {AuthService, HasuraAuth} from './auth.service'
import {oauth} from './discord.auth'

@Controller()
export class AuthController {
  constructor(
    private readonly logger: LoggerService,
    private readonly authService: AuthService,
  ) {}

  @Post('/login')
  async login(
    @Body('code')
    code: string,
  ): Promise<ReturnType<typeof oauth.tokenRequest>> {
    this.logger.verbose(`AuthController - login - code ${code}`)

    return this.authService.login(code)
  }

  @Get('/hasura/webhook')
  async hasuraWebhook(
    @Headers('Authorization') token?: string,
  ): Promise<HasuraAuth> {
    this.logger.verbose(`AuthController - hasuraWebhook - token ${token}`)

    return this.authService.getHasuraAuth(token?.replace('Bearer ', '') ?? '')
  }
}
