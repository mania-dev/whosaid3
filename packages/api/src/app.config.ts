import { Property } from 'ts-convict'

export class AppConfig {
  @Property({
    format: String,
    default: null,
    env: 'CORS_ORIGIN',
  })
  corsOrigin: string
}
