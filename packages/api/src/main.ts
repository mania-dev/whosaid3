import { NestApplication, NestFactory } from '@nestjs/core'
import { AppModule } from './app.module'
import { LoggerService } from '@whosaid/common'
import { AppConfig } from './app.config'

async function bootstrap(): Promise<void> {
  const app = await NestFactory.create<NestApplication>(AppModule, {})

  const logger = app.get(LoggerService)
  logger.log('Using correct logger')
  app.useLogger(logger)

  const appConfig = app.get(AppConfig)

  app.enableCors({
    origin: appConfig.corsOrigin,
  })

  await app.listen(3000)
}

console.log('Bootstraping')
bootstrap()
