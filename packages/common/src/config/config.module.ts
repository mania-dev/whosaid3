import { DynamicModule, Module } from '@nestjs/common'
import { TSConvict } from 'ts-convict'

type ClassType<T> = { new (): T }

@Module({})
export class ConfigModule {
  public static forConfig<T>(
    configClass: ClassType<T> | ClassType<T>[],
  ): DynamicModule {
    const configClasses = Array.isArray(configClass)
      ? configClass
      : [configClass]

    return {
      providers: configClasses.map(configClass => ({
        useFactory: (): T => {
          const loader = new TSConvict(configClass)
          return loader.load()
        },
        provide: configClass,
      })),
      exports: configClasses,
      module: ConfigModule,
    }
  }
}
