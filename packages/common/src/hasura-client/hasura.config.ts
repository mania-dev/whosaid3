import { Property } from 'ts-convict'

export class HasuraConfig {
  @Property({
    format: String,
    default: null,
    env: 'HASURA_HOST',
  })
  host: string

  @Property({
    format: String,
    default: 'http',
    env: 'HASURA_HTTP_PROTOCOL',
  })
  httpProtocol: string

  @Property({
    format: String,
    default: 'ws',
    env: 'HASURA_WS_PROTOCOL',
  })
  wsProtocol: string

  @Property({
    format: Number,
    default: 80,
    env: 'HASURA_PORT',
  })
  port: number

  @Property({
    format: String,
    default: '/',
    env: 'HASURA_PATH',
  })
  path: string

  @Property({
    format: String,
    default: '',
    env: 'HASURA_ADMIN_SECRET',
  })
  adminSecret: string
}
