import { Global, Module } from '@nestjs/common'
import { HasuraClient } from './hasura.client'
import { ConfigModule } from '../config'
import { HasuraConfig } from './hasura.config'

@Global()
@Module({
  imports: [ConfigModule.forConfig(HasuraConfig)],
  providers: [HasuraClient],
  exports: [HasuraClient],
})
export class HasuraModule {}
