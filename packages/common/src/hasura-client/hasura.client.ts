import { Injectable, OnModuleDestroy } from '@nestjs/common'
import { HasuraConfig } from './hasura.config'
import { LoggerService } from '../logger'
import { SubscriptionClient } from 'subscriptions-transport-ws'
import {
  InMemoryCache,
  HttpLink,
  split,
  ApolloClient,
  NormalizedCacheObject,
  ApolloLink,
} from '@apollo/client/core'
import url from 'url'
import ws from 'ws'

import { WebSocketLink } from '@apollo/client/link/ws'
import { getMainDefinition } from '@apollo/client/utilities'
import fetch from 'isomorphic-fetch'
import { RetryLink } from '@apollo/client/link/retry'
import { withScalars } from 'apollo-link-scalars'
import { typesMap } from './hasura.scalars'
import { schema } from './hasura.schema'
import { GraphQLSchema } from 'graphql'

@Injectable()
export class HasuraClient implements OnModuleDestroy {
  private readonly clientName = 'hasura-client'
  private subscriptionClients: SubscriptionClient[] = []
  private defaultApolloClient: ApolloClient<NormalizedCacheObject>
  private apolloClients: ApolloClient<NormalizedCacheObject>[] = []

  constructor(
    private readonly hasuraConfig: HasuraConfig,
    private readonly loggerService: LoggerService,
  ) {
    this.defaultApolloClient = this.getClient({
      schema,
      clientName: this.clientName,
    })
  }

  public onModuleDestroy(): void {
    this.apolloClients.forEach((client) => {
      client.stop()
    })

    this.subscriptionClients.forEach((client) => {
      client.close()
    })
  }

  public get client(): ApolloClient<NormalizedCacheObject> {
    return this.defaultApolloClient
  }

  public getClient(params: {
    schema: GraphQLSchema
    clientName: string
  }): ApolloClient<NormalizedCacheObject> {
    const cache = new InMemoryCache()
    const httpLink = new HttpLink({
      uri: url.format({
        protocol: this.hasuraConfig.httpProtocol,
        hostname: this.hasuraConfig.host,
        pathname: this.hasuraConfig.path,
        port: this.hasuraConfig.port,
      }),
      headers: {
        'x-hasura-admin-secret': this.hasuraConfig.adminSecret,
      },
      fetch,
    })

    const subscriptionClient = new SubscriptionClient(
      url.format({
        protocol: this.hasuraConfig.wsProtocol,
        hostname: this.hasuraConfig.host,
        pathname: this.hasuraConfig.path,
        port: this.hasuraConfig.port,
      }),
      {
        reconnect: true,
        connectionParams: {
          clientName: this.clientName,
          headers: {
            'x-hasura-admin-secret': this.hasuraConfig.adminSecret,
          },
        },
      },
      ws,
    )

    const wsLink = new WebSocketLink(subscriptionClient)

    const splitLink = split(
      // split based on operation type
      ({ query }) => {
        const definition = getMainDefinition(query)
        return (
          definition.kind === 'OperationDefinition' &&
          definition.operation === 'subscription'
        )
      },
      wsLink,
      httpLink,
    )

    const retryLink = new RetryLink({
      attempts: { max: 10 },
      delay: (count, operation, error) => {
        const delay = count * 1000 * Math.random()

        this.loggerService.error(
          `Error in operation ${operation.operationName}: ${JSON.stringify(
            error,
          )}. Retrying in ${delay} seconds`,
          '',
        )
        return delay
      },
    })

    const scalarsLink = (withScalars({
      schema: params.schema,
      typesMap,
    }) as unknown) as ApolloLink

    const apolloClient = new ApolloClient({
      cache: cache,
      link: ApolloLink.from([scalarsLink, retryLink, splitLink]),
      name: params.clientName,
      queryDeduplication: false,
      defaultOptions: {
        watchQuery: {
          fetchPolicy: 'no-cache',
        },
        query: {
          fetchPolicy: 'no-cache',
        },
      },
    })

    this.subscriptionClients.push(subscriptionClient)
    this.apolloClients.push(apolloClient)

    return apolloClient
  }
}
