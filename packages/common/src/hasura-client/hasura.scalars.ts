import { GraphQLScalarType, Kind } from 'graphql'

export const timestamptz = new GraphQLScalarType({
  name: 'timestamptz',
  description: 'Date custom scalar type',
  parseValue(value) {
    return new Date(value) // value from the client
  },
  serialize(value: Date) {
    return value.toISOString() // value sent to the client
  },
  parseLiteral(ast) {
    if (ast.kind === Kind.STRING) {
      console.log(`parseLiteral ${ast.value}`)
      return ast.value // ast value is always in string format
    }
    return null
  },
})

export const typesMap = {
  timestamptz,
}
