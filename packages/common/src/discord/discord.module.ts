import { ClientOptions, Client } from 'discord.js'
import { DiscordConfig } from './discord.config'
import { ConfigModule } from '../config'
import { Module } from '@nestjs/common'

export type DiscordClientOptions = ClientOptions & { token: string }

const providers = [
  {
    provide: 'DISCORD_CLIENT_OPTIONS',
    useFactory: (discordConfig: DiscordConfig): DiscordClientOptions => ({
      token: discordConfig.token,
      partials: ['MESSAGE', 'CHANNEL', 'REACTION', 'USER', 'GUILD_MEMBER'],
    }),
    inject: [DiscordConfig],
  },
  {
    provide: 'DISCORD_CLIENT',
    useFactory: async (
      discordClientOptions: DiscordClientOptions,
    ): Promise<Client> => {
      const { token, ...clientOptions } = discordClientOptions
      const client = new Client(clientOptions)
      await client.login(token)
      return client
    },
    inject: ['DISCORD_CLIENT_OPTIONS'],
  },
]

@Module({
  imports: [ConfigModule.forConfig(DiscordConfig)],
  providers: [...providers],
  exports: [...providers],
})
export class DiscordModule {}
