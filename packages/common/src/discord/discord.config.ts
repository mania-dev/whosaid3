import { Property } from 'ts-convict'

export class DiscordConfig {
  @Property({
    format: String,
    default: null,
    env: 'DISCORD_TOKEN',
  })
  token: string
}
