import { Global, Module } from '@nestjs/common'
import { LoggerService } from './logger.service'
import { ConfigModule } from '../config/config.module'
import { LoggerConfig } from './logger.config'

@Global()
@Module({
  imports: [ConfigModule.forConfig(LoggerConfig)],
  providers: [LoggerService],
  exports: [LoggerService],
})
export class LoggerModule {}
