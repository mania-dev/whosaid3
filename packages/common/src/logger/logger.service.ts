import { Injectable, LoggerService as LS } from '@nestjs/common'
import { createLogger, format, Logger, transports } from 'winston'
import { LoggerConfig } from './logger.config'

@Injectable()
export class LoggerService implements LS {
  private logger: Logger

  constructor(private loggerConfig: LoggerConfig) {
    const colorizer = format.colorize()
    this.logger = createLogger({
      level: loggerConfig.logLevel,
      format: format.combine(
        format.timestamp({ format: 'YYYY-MM-DD HH:mm:ss' }),
        format.printf((info) =>
          colorizer.colorize(
            info.level,
            `${info.timestamp} :: ${info.level} :: ${info.message}`,
          ),
        ),
      ),
      transports: [new transports.Console()],
    })
  }

  log(message: string): void {
    this.logger.info(message)
  }

  error(message: string, trace?: string): void {
    this.logger.error(message, trace)
  }

  warn(message: string): void {
    this.logger.warn(message)
  }

  debug(message: string): void {
    this.logger.debug(message)
  }

  verbose(message: string): void {
    this.logger.verbose(message)
  }
}
