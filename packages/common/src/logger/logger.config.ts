import { Property } from 'ts-convict'

export class LoggerConfig {
  @Property({
    format: ['log', 'error', 'info', 'warn', 'debug', 'verbose'],
    default: 'info',
    env: 'LOG_LEVEL',
  })
  logLevel: string
}
