export interface Config {
  apiUrl: string
  graphQLUrl: string
  clientId: string
}

export const config: Config = {
  apiUrl: (window as any)._env_.API_URL,
  graphQLUrl: (window as any)._env_.GRAPHQL_URL,
  clientId: (window as any)._env_.CLIENT_ID,
}
