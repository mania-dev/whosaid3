import { useAuthDispatch, useAuthState } from './context/auth'
import axios, { AxiosInstance } from 'axios'
import { config } from '../config'

export function useGraphQLAxios(): AxiosInstance {
  const authState = useAuthState()
  const authDispatch = useAuthDispatch()

  const axiosInstance = axios.create({
    baseURL: config.graphQLUrl,
    headers: authState.accessToken,
  })

  axiosInstance.interceptors.response.use(
    (response) => response,
    async (error) => {
      if (error.response.status === 401) {
        authDispatch({
          type: 'LOGIN_ERROR',
          payload: {
            error: 'Your session has ended, please log in again.',
          },
        })
      }
    },
  )

  return axiosInstance
}
