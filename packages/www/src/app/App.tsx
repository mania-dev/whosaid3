import React from 'react'
import './App.css'
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom'
import { Auth } from './pages/Auth/Auth'

import { QueryParamProvider } from 'use-query-params'
import { AuthProvider } from './context/auth'
import { ProtectedRoute } from './components/ProtectedRoute/ProtectedRoute'
import { client } from './client'
import { ApolloProvider } from '@apollo/client'
import { HomePage } from './pages/Home/HomePage'
import { ActivityPage } from './pages/Activity/Activity'

function App(): JSX.Element {
  return (
    <div className="dark">
      <div className="flex h-screen antialiased text-gray-900 bg-hero-hexagons bg-dark">
        <div className="container w-full mx-auto pt-20">
          <AuthProvider>
            <ApolloProvider client={client}>
              <Router>
                <QueryParamProvider ReactRouterRoute={Route}>
                  <Switch>
                    <Route exact path="/">
                      <HomePage />
                    </Route>
                    <ProtectedRoute exact path="/activity">
                      <ActivityPage />
                    </ProtectedRoute>
                    <Route exact path="/auth/discord">
                      <Auth />
                    </Route>
                    <Route path="*">404</Route>
                  </Switch>
                </QueryParamProvider>
              </Router>
            </ApolloProvider>
          </AuthProvider>
        </div>
      </div>
    </div>
  )
}

export default App
