import { PresenceType } from './types'

export function getLastNDays(nDays: number): Date[] {
  const lastWeek = new Date()
  lastWeek.setDate(lastWeek.getDate() - nDays)

  return [...Array(nDays)].map((_, index) => {
    const day = new Date(lastWeek)
    day.setDate(day.getDate() + index + 1)
    day.setHours(0, 0, 0, 0)
    return day
  })
}

export function filterPresenceBetween(
  allPresences: PresenceType[],
  start: Date,
  end: Date,
): PresenceType[] {
  return allPresences.filter(
    (p) =>
      new Date(p.timestamp).getTime() > start.getTime() &&
      new Date(p.timestamp).getTime() <= end.getTime(),
  )
}

export function isToday(someDate: Date): boolean {
  const today = new Date()
  return (
    someDate.getDate() == today.getDate() &&
    someDate.getMonth() == today.getMonth() &&
    someDate.getFullYear() == today.getFullYear()
  )
}

export function extendLastPresenceToEndOfDay(
  dailyPresence: PresenceType[],
): PresenceType[] {
  const lastPresence = dailyPresence[dailyPresence.length - 1]

  if (!lastPresence) {
    return dailyPresence
  }

  let extendedDate: Date
  if (isToday(new Date(lastPresence.timestamp))) {
    extendedDate = new Date()
  } else {
    extendedDate = new Date(lastPresence.timestamp)
    extendedDate.setHours(23, 59, 59, 999)
  }

  const lastPresenceExtended: PresenceType = {
    ...dailyPresence[dailyPresence.length - 1],
    timestamp: extendedDate.toISOString(),
  }
  return [...dailyPresence, lastPresenceExtended]
}

export function addLastPresenceOfYesterdayToStartOfDay(
  dailyPresence: PresenceType[],
  allPresences: PresenceType[] | undefined,
): PresenceType[] {
  if (!allPresences) {
    return dailyPresence
  }
  const firstPresenceOfDay = dailyPresence[0]

  if (!firstPresenceOfDay) {
    return dailyPresence
  }

  const indexOfFirstPresenceOfDay = allPresences.indexOf(firstPresenceOfDay)

  if (indexOfFirstPresenceOfDay === 0) {
    return dailyPresence
  }
  const lastPresenceOfYesterday = allPresences[indexOfFirstPresenceOfDay - 1]

  const firstTimeOfDay = new Date(firstPresenceOfDay.timestamp)
  firstTimeOfDay.setHours(0, 0, 0, 0)

  const artificialFirstPresenceOfDay: PresenceType = {
    ...lastPresenceOfYesterday,
    timestamp: firstTimeOfDay.toISOString(),
  }

  return [artificialFirstPresenceOfDay, ...dailyPresence]
}
