import { useGetActivityQuery } from '../../../generated/graphql'
import { Loader } from '../../components/Loader/Loader'
import { Daily } from './Daily'
import { DailyInfos } from './types'
import {
  addLastPresenceOfYesterdayToStartOfDay,
  extendLastPresenceToEndOfDay,
  filterPresenceBetween,
  getLastNDays,
} from './utils'
import { useMemo } from 'react'
import { scaleTime } from '@visx/scale'
import { Axis, Orientation } from '@visx/axis'
import { GridColumns } from '@visx/grid'
import { Line } from '@visx/shape'

const dailyHeight = 60
const dailyWidth = 1200
const marginX = 60
const marginY = 60

export function ActivityPage(): JSX.Element {
  const { loading, data } = useGetActivityQuery()

  const activities = data?.user_presence
    ?.map((value) => value.activities)
    .flat()

  console.log(activities)

  const weekPresences: DailyInfos[] = useMemo(() => {
    const week = getLastNDays(7)

    return week.map((startOfDay) => {
      const endOfDay = new Date(startOfDay)
      endOfDay.setHours(23, 59, 59, 999)

      let dailyPresence = data?.user_presence
        ? filterPresenceBetween(data.user_presence, startOfDay, endOfDay)
        : []

      dailyPresence = addLastPresenceOfYesterdayToStartOfDay(
        dailyPresence,
        data?.user_presence,
      )
      dailyPresence = extendLastPresenceToEndOfDay(dailyPresence)

      return {
        start: startOfDay,
        end: endOfDay,
        presences: dailyPresence,
      }
    })
  }, [data])

  const xScale = scaleTime<number>({
    domain: [Date.UTC(2000, 0, 0, 0, 0, 0), Date.UTC(2000, 0, 0, 23, 59, 59)],
    range: [0, dailyWidth],
  })

  return (
    <main className="mt-48 mx-auto max-w-7xl px-4 sm:mt-12 sm:px-6 md:mt-16 lg:mt-20 lg:px-8 xl:mt-28">
      {loading ? (
        <div className={'flex flex-col items-center justify-center '}>
          <Loader />
        </div>
      ) : null}
      {!loading && weekPresences ? (
        <svg
          width={dailyWidth + 2 * marginX}
          height={dailyHeight * weekPresences.length + 2 * marginY}
        >
          <rect
            width={dailyWidth + 2 * marginX}
            height={dailyHeight * weekPresences.length + 2 * marginY}
            fill="#ecfeff"
            rx={5}
            ry={5}
          />
          <rect
            width={dailyWidth}
            height={dailyHeight * weekPresences.length}
            fill="#cffafe"
            x={marginX}
            y={marginY}
          />
          {weekPresences.map((dailyPresence, index) => (
            <>
              <Daily
                dailyInfo={dailyPresence}
                width={dailyWidth}
                height={dailyHeight}
                top={index * dailyHeight + marginY}
                left={marginX}
              />
              <Line
                from={{ x: marginX, y: index * dailyHeight + marginY }}
                to={{
                  x: marginX + dailyWidth,
                  y: index * dailyHeight + marginY,
                }}
                stroke={'#ecfeff'}
              />
            </>
          ))}
          <Axis
            scale={xScale}
            orientation={Orientation.bottom}
            top={dailyHeight * weekPresences.length + marginY}
            left={marginX}
            numTicks={23}
            tickFormat={(value, index, values) =>
              value instanceof Date ? `${value.getUTCHours()}h` : ''
            }
          />
          <GridColumns
            scale={xScale}
            stroke={'#6e0fca'}
            height={60 * weekPresences.length}
            numTicks={24}
            strokeDasharray="1,3"
            strokeOpacity={0.4}
            left={marginX}
            top={marginY}
          />
        </svg>
      ) : null}
    </main>
  )
}
