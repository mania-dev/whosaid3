import React from 'react'
import { max, min } from 'd3-array'
import { curveStepAfter } from '@visx/curve'
import { Group } from '@visx/group'
import { AreaClosed, Line } from '@visx/shape'
import { scaleLinear, scaleTime } from '@visx/scale'
import { DailyInfos, PresenceType } from './types'

import { isToday } from './utils'
import { AreaClosedProps } from '@visx/shape/lib/shapes/AreaClosed'
import { AddSVGProps } from '@visx/shape/lib/types'

export type CurveProps = {
  dailyInfo: DailyInfos
  width: number
  height: number
  top: number
  left: number
  showControls?: boolean
}

export type CurveParams = Omit<
  AddSVGProps<AreaClosedProps<PresenceType>, SVGPathElement>,
  'yScale'
> & {
  presenceFn: (d: PresenceType) => boolean
}

const curves: CurveParams[] = [
  {
    presenceFn: (d) => d.activities.some((value) => value.type === 'PLAYING'),
    fill: '#8b5cf6',
    fillOpacity: 0.4,
  },
  {
    presenceFn: (d) => d.client_status_desktop === 'online',
    fill: '#22c55e',
    fillOpacity: 0.4,
  },
  {
    presenceFn: (d) => d.client_status_desktop === 'idle',
    fill: '#22c55e',
    fillOpacity: 0.2,
  },
  {
    presenceFn: (d) => d.client_status_mobile === 'online',
    fill: '#155e75',
    fillOpacity: 0.4,
  },
]

export function Daily({
  dailyInfo,
  width,
  height,
  top,
  left,
}: CurveProps): JSX.Element {
  const lineHeight = height
  const { presences } = dailyInfo

  // data accessors
  const getX = (d: PresenceType): Date => new Date(d.timestamp)

  const yScale = scaleLinear<number>({
    domain: [0, 1],
    range: [lineHeight, 0],
  })
  // scales
  const minX = min(presences, getX)
  const maxX = max(presences, getX)

  minX?.setHours(0, 0, 0)
  maxX?.setHours(23, 59, 59)

  const xScale = scaleTime<number>({
    domain: minX && maxX ? [minX, maxX] : undefined,
    range: [0, width],
  })

  return (
    <>
      <Group key={`area`} top={top} left={left}>
        {curves.map(({ presenceFn, ...restProps }, index) => {
          const realPresenceFn = (d: PresenceType): number =>
            curves.slice(0, index).some((curve) => curve.presenceFn(d))
              ? 0
              : presenceFn(d)
              ? 1
              : 0

          return (
            <AreaClosed<PresenceType>
              curve={curveStepAfter}
              data={presences}
              x={(d: PresenceType): number => xScale(getX(d)) ?? 0}
              y={(d: PresenceType): number => yScale(realPresenceFn(d)) ?? 0}
              yScale={yScale}
              {...restProps}
            />
          )
        })}
      </Group>
      {minX && isToday(minX) ? (
        <Line
          from={{ x: left + xScale(new Date()), y: top }}
          to={{ x: left + xScale(new Date()), y: top + yScale(0) }}
          stroke={'#F00'}
        />
      ) : null}
    </>
  )
}
