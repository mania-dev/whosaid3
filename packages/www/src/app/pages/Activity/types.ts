import {GetActivityQuery} from '../../../generated/graphql'

export type PresenceType = GetActivityQuery['user_presence'][0]

export type DailyInfos = {
  start: Date
  end: Date
  presences: PresenceType[]
}
