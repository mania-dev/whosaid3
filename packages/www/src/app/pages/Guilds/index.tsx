import { gql, useQuery } from '@apollo/client'
import { useGetGuildsQuery } from '../../../generated/graphql'

const PROFILE_QUERY = gql`
  query MyQuery {
    guild {
      id
    }
  }
`

export function LastMessage() {
  const { loading, data } = useGetGuildsQuery()

  if (loading) {
    return <p className="navbar-text navbar-right">Loading...</p>
  }

  if (data) {
    return (
      <span>
        <p className="navbar-text navbar-right">
          {JSON.stringify(data.guild)}
          &nbsp;
        </p>
      </span>
    )
  }

  return <div>Last message: coucous</div>
}
