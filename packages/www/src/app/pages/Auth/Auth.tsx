import { useQueryParam, StringParam } from 'use-query-params'
import { useEffect } from 'react'
import { config } from '../../../config'
import { useAuthDispatch, useAuthState } from '../../context/auth'
import { useHistory, useLocation } from 'react-router-dom'
import { Loader } from '../../components/Loader/Loader'
import axios from 'axios'

export function Auth(): JSX.Element {
  const [code] = useQueryParam('code', StringParam)
  const dispatch = useAuthDispatch()
  const auth = useAuthState()
  const history = useHistory<any>()
  const location = useLocation<any>()

  const { from } = location.state || { from: { pathname: '/' } }

  console.log(auth)

  useEffect(() => {
    if (!code) {
      return
    }

    const login = async (code: string): Promise<void> => {
      try {
        const response = await axios
          .create({
            baseURL: config.apiUrl,
          })
          .post('/login', { code })

        dispatch({
          type: 'LOGIN_SUCCESS',
          payload: {
            accessToken: response.data.access_token,
            refreshToken: response.data.refresh_token,
          },
        })

        history.replace(from)
      } catch (e) {
        if (axios.isAxiosError(e)) {
          dispatch({
            type: 'LOGIN_ERROR',
            payload: {
              error: e.message,
            },
          })
        } else {
          console.error(e)
          throw e
        }
      }
    }

    login(code)
  }, [code])

  return (
    <div>
      <Loader />
    </div>
  )
}
