import { LoginButton } from '../../components/LoginButton/LoginButton'
import { useAuthState } from '../../context/auth'
import { LogoutButton } from '../../components/LogoutButton/LogoutButton'
import { Link } from 'react-router-dom'

export function HomePage(): JSX.Element {
  const authState = useAuthState()

  return (
    <main className="mt-48 mx-auto max-w-7xl px-4 sm:mt-12 sm:px-6 md:mt-16 lg:mt-20 lg:px-8 xl:mt-28">
      <div className="text-center">
        <h1 className="text-9xl">Whosaid</h1>

        {authState.accessToken ? (
          <h3 className="text-2xl">
            <Link
              to={{
                pathname: '/activity',
              }}
            >
              See activity
            </Link>
          </h3>
        ) : null}

        <div className="mt-96">
          {authState.accessToken ? <LogoutButton /> : <LoginButton />}
        </div>
      </div>
    </main>
  )
}
