import { createContext, Dispatch, useContext, useReducer } from 'react'
import { AuthAction, AuthReducer, AuthState, initialState } from './reducer'

const AuthStateContext = createContext<AuthState>(initialState)

AuthStateContext.displayName = 'AuthStateContext'

const AuthDispatchContext = createContext<Dispatch<AuthAction>>(() => {
  // noop
})
AuthDispatchContext.displayName = 'AuthDispatchContext'

export function useAuthState() {
  const context = useContext(AuthStateContext)
  if (context === undefined) {
    throw new Error('useAuthState must be used within a AuthProvider')
  }

  return context
}

export function useAuthDispatch() {
  const context = useContext(AuthDispatchContext)
  if (context === undefined) {
    throw new Error('useAuthDispatch must be used within a AuthProvider')
  }

  return context
}

export const AuthProvider = ({ children }: { children: any }) => {
  const [user, dispatch] = useReducer(AuthReducer, initialState)

  return (
    <AuthStateContext.Provider value={user}>
      <AuthDispatchContext.Provider value={dispatch}>
        {children}
      </AuthDispatchContext.Provider>
    </AuthStateContext.Provider>
  )
}
