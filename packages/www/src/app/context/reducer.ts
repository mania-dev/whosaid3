const accessTokenFromStorage = localStorage.getItem('accessToken') ?? undefined
const refreshTokenFromStorage =
  localStorage.getItem('refreshToken') ?? undefined

export interface AuthState {
  accessToken: string | undefined
  refreshToken: string | undefined
  loading: boolean
  errorMessage: string | undefined
}

export type RequestLoginAction = {
  type: 'REQUEST_LOGIN'
}

export type LoginAction = {
  type: 'LOGIN_SUCCESS'
  payload: {
    accessToken: string
    refreshToken: string
  }
}

export type LogoutAction = {
  type: 'LOGOUT'
}

export type LoginErrorAction = {
  type: 'LOGIN_ERROR'
  payload: {
    error: string
  }
}

export type AuthAction =
  | RequestLoginAction
  | LoginAction
  | LogoutAction
  | LoginErrorAction

export const initialState: AuthState = {
  accessToken: accessTokenFromStorage,
  refreshToken: refreshTokenFromStorage,
  loading: false,
  errorMessage: undefined,
}

export const AuthReducer = (
  initialState: AuthState,
  action: AuthAction,
): AuthState => {
  switch (action.type) {
    case 'REQUEST_LOGIN':
      return {
        ...initialState,
        loading: true,
      }
    case 'LOGIN_SUCCESS':
      localStorage.setItem('accessToken', action.payload.accessToken)
      localStorage.setItem('refreshToken', action.payload.refreshToken)
      return {
        ...initialState,
        accessToken: action.payload.accessToken,
        refreshToken: action.payload.refreshToken,
        loading: false,
      }
    case 'LOGOUT':
      localStorage.removeItem('accessToken')
      localStorage.removeItem('refreshToken')
      return {
        ...initialState,
        accessToken: undefined,
        refreshToken: undefined,
        loading: false,
      }

    case 'LOGIN_ERROR':
      console.log('LOGIN_ERROR', action)
      localStorage.removeItem('accessToken')
      localStorage.removeItem('refreshToken')
      return {
        ...initialState,
        loading: false,
        accessToken: undefined,
        refreshToken: undefined,
        errorMessage: action.payload.error,
      }
  }
}
