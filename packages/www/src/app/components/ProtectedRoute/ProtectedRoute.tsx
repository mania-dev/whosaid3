import React from 'react'
import { Redirect, Route, RouteProps, useLocation } from 'react-router-dom'
import { useAuthState } from '../../context/auth'

type Props = RouteProps

export function ProtectedRoute(props: Props): JSX.Element {
  const auth = useAuthState()

  const location = useLocation()

  return auth.accessToken !== undefined ? (
    <Route {...props} />
  ) : (
    <Redirect to={{ pathname: '/', state: { from: location } }} />
  )
}
