import 'react-loader-spinner/dist/loader/css/react-spinner-loader.css'
import ReactLoader from 'react-loader-spinner'

export function Loader(): JSX.Element {
  return <ReactLoader type="TailSpin" color="white" height={100} width={100} />
}
