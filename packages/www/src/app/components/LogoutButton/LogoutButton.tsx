import { useAuthDispatch } from '../../context/auth'

export function LogoutButton(): JSX.Element {
  const authDispatch = useAuthDispatch()

  const handleClick = () => {
    authDispatch({
      type: 'LOGOUT',
    })
  }

  return (
    <div className="mx-10 my-4">
      <button
        className="px-10 py-4 text-sm text-white rounded-md bg-primary-darker hover:bg-primary-dark focus:outline-none focus:ring focus:ring-primary focus:ring-offset-1 focus:ring-offset-white dark:focus:ring-offset-dark"
        onClick={handleClick}
      >
        Logout
      </button>
    </div>
  )
}
