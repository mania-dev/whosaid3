import { config } from '../../../config'

export function LoginButton(): JSX.Element {
  const callbackUrl = encodeURIComponent(
    `${window.location.origin}/auth/discord`,
  )
  const responseType = 'code'
  const scope = 'identify'
  const link = `https://discord.com/oauth2/authorize?client_id=${config.clientId}&redirect_uri=${callbackUrl}&response_type=${responseType}&scope=${scope}`

  return (
    <div className="mx-10 my-4">
      <a
        className="px-10 py-4 text-sm text-white rounded-md bg-primary-darker hover:bg-primary-dark focus:outline-none focus:ring focus:ring-primary focus:ring-offset-1 focus:ring-offset-white dark:focus:ring-offset-dark"
        href={link}
      >
        Login with Discord
      </a>
    </div>
  )
}
