import { gql } from '@apollo/client';
import * as Apollo from '@apollo/client';
export type Maybe<T> = T | null;
export type Exact<T extends { [key: string]: unknown }> = { [K in keyof T]: T[K] };
export type MakeOptional<T, K extends keyof T> = Omit<T, K> & { [SubKey in K]?: Maybe<T[SubKey]> };
export type MakeMaybe<T, K extends keyof T> = Omit<T, K> & { [SubKey in K]: Maybe<T[SubKey]> };
/** All built-in and custom scalars, mapped to their actual values */
export type Scalars = {
  ID: string;
  String: string;
  Boolean: boolean;
  Int: number;
  Float: number;
  timestamptz: any;
  uuid: any;
};

/** expression to compare columns of type Boolean. All fields are combined with logical 'AND'. */
export type Boolean_Comparison_Exp = {
  _eq?: Maybe<Scalars['Boolean']>;
  _gt?: Maybe<Scalars['Boolean']>;
  _gte?: Maybe<Scalars['Boolean']>;
  _in?: Maybe<Array<Scalars['Boolean']>>;
  _is_null?: Maybe<Scalars['Boolean']>;
  _lt?: Maybe<Scalars['Boolean']>;
  _lte?: Maybe<Scalars['Boolean']>;
  _neq?: Maybe<Scalars['Boolean']>;
  _nin?: Maybe<Array<Scalars['Boolean']>>;
};

/** expression to compare columns of type Int. All fields are combined with logical 'AND'. */
export type Int_Comparison_Exp = {
  _eq?: Maybe<Scalars['Int']>;
  _gt?: Maybe<Scalars['Int']>;
  _gte?: Maybe<Scalars['Int']>;
  _in?: Maybe<Array<Scalars['Int']>>;
  _is_null?: Maybe<Scalars['Boolean']>;
  _lt?: Maybe<Scalars['Int']>;
  _lte?: Maybe<Scalars['Int']>;
  _neq?: Maybe<Scalars['Int']>;
  _nin?: Maybe<Array<Scalars['Int']>>;
};

/** expression to compare columns of type String. All fields are combined with logical 'AND'. */
export type String_Comparison_Exp = {
  _eq?: Maybe<Scalars['String']>;
  _gt?: Maybe<Scalars['String']>;
  _gte?: Maybe<Scalars['String']>;
  _ilike?: Maybe<Scalars['String']>;
  _in?: Maybe<Array<Scalars['String']>>;
  _is_null?: Maybe<Scalars['Boolean']>;
  _like?: Maybe<Scalars['String']>;
  _lt?: Maybe<Scalars['String']>;
  _lte?: Maybe<Scalars['String']>;
  _neq?: Maybe<Scalars['String']>;
  _nilike?: Maybe<Scalars['String']>;
  _nin?: Maybe<Array<Scalars['String']>>;
  _nlike?: Maybe<Scalars['String']>;
  _nsimilar?: Maybe<Scalars['String']>;
  _similar?: Maybe<Scalars['String']>;
};

/** columns and relationships of "channel" */
export type Channel = {
  channel_created_at: Scalars['timestamptz'];
  /** An object relationship */
  guild: Guild;
  guild_id: Scalars['String'];
  id: Scalars['String'];
  /** An array relationship */
  messages: Array<Message>;
  /** An aggregated array relationship */
  messages_aggregate: Message_Aggregate;
  name: Scalars['String'];
  nsfw: Scalars['Boolean'];
  topic?: Maybe<Scalars['String']>;
  type: Scalars['String'];
};


/** columns and relationships of "channel" */
export type ChannelMessagesArgs = {
  distinct_on?: Maybe<Array<Message_Select_Column>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<Message_Order_By>>;
  where?: Maybe<Message_Bool_Exp>;
};


/** columns and relationships of "channel" */
export type ChannelMessages_AggregateArgs = {
  distinct_on?: Maybe<Array<Message_Select_Column>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<Message_Order_By>>;
  where?: Maybe<Message_Bool_Exp>;
};

/** aggregated selection of "channel" */
export type Channel_Aggregate = {
  aggregate?: Maybe<Channel_Aggregate_Fields>;
  nodes: Array<Channel>;
};

/** aggregate fields of "channel" */
export type Channel_Aggregate_Fields = {
  count?: Maybe<Scalars['Int']>;
  max?: Maybe<Channel_Max_Fields>;
  min?: Maybe<Channel_Min_Fields>;
};


/** aggregate fields of "channel" */
export type Channel_Aggregate_FieldsCountArgs = {
  columns?: Maybe<Array<Channel_Select_Column>>;
  distinct?: Maybe<Scalars['Boolean']>;
};

/** order by aggregate values of table "channel" */
export type Channel_Aggregate_Order_By = {
  count?: Maybe<Order_By>;
  max?: Maybe<Channel_Max_Order_By>;
  min?: Maybe<Channel_Min_Order_By>;
};

/** input type for inserting array relation for remote table "channel" */
export type Channel_Arr_Rel_Insert_Input = {
  data: Array<Channel_Insert_Input>;
  on_conflict?: Maybe<Channel_On_Conflict>;
};

/** Boolean expression to filter rows from the table "channel". All fields are combined with a logical 'AND'. */
export type Channel_Bool_Exp = {
  _and?: Maybe<Array<Maybe<Channel_Bool_Exp>>>;
  _not?: Maybe<Channel_Bool_Exp>;
  _or?: Maybe<Array<Maybe<Channel_Bool_Exp>>>;
  channel_created_at?: Maybe<Timestamptz_Comparison_Exp>;
  guild?: Maybe<Guild_Bool_Exp>;
  guild_id?: Maybe<String_Comparison_Exp>;
  id?: Maybe<String_Comparison_Exp>;
  messages?: Maybe<Message_Bool_Exp>;
  name?: Maybe<String_Comparison_Exp>;
  nsfw?: Maybe<Boolean_Comparison_Exp>;
  topic?: Maybe<String_Comparison_Exp>;
  type?: Maybe<String_Comparison_Exp>;
};

/** unique or primary key constraints on table "channel" */
export enum Channel_Constraint {
  /** unique or primary key constraint */
  ChannelPkey = 'channel_pkey'
}

/** input type for inserting data into table "channel" */
export type Channel_Insert_Input = {
  channel_created_at?: Maybe<Scalars['timestamptz']>;
  guild?: Maybe<Guild_Obj_Rel_Insert_Input>;
  guild_id?: Maybe<Scalars['String']>;
  id?: Maybe<Scalars['String']>;
  messages?: Maybe<Message_Arr_Rel_Insert_Input>;
  name?: Maybe<Scalars['String']>;
  nsfw?: Maybe<Scalars['Boolean']>;
  topic?: Maybe<Scalars['String']>;
  type?: Maybe<Scalars['String']>;
};

/** aggregate max on columns */
export type Channel_Max_Fields = {
  channel_created_at?: Maybe<Scalars['timestamptz']>;
  guild_id?: Maybe<Scalars['String']>;
  id?: Maybe<Scalars['String']>;
  name?: Maybe<Scalars['String']>;
  topic?: Maybe<Scalars['String']>;
  type?: Maybe<Scalars['String']>;
};

/** order by max() on columns of table "channel" */
export type Channel_Max_Order_By = {
  channel_created_at?: Maybe<Order_By>;
  guild_id?: Maybe<Order_By>;
  id?: Maybe<Order_By>;
  name?: Maybe<Order_By>;
  topic?: Maybe<Order_By>;
  type?: Maybe<Order_By>;
};

/** aggregate min on columns */
export type Channel_Min_Fields = {
  channel_created_at?: Maybe<Scalars['timestamptz']>;
  guild_id?: Maybe<Scalars['String']>;
  id?: Maybe<Scalars['String']>;
  name?: Maybe<Scalars['String']>;
  topic?: Maybe<Scalars['String']>;
  type?: Maybe<Scalars['String']>;
};

/** order by min() on columns of table "channel" */
export type Channel_Min_Order_By = {
  channel_created_at?: Maybe<Order_By>;
  guild_id?: Maybe<Order_By>;
  id?: Maybe<Order_By>;
  name?: Maybe<Order_By>;
  topic?: Maybe<Order_By>;
  type?: Maybe<Order_By>;
};

/** response of any mutation on the table "channel" */
export type Channel_Mutation_Response = {
  /** number of affected rows by the mutation */
  affected_rows: Scalars['Int'];
  /** data of the affected rows by the mutation */
  returning: Array<Channel>;
};

/** input type for inserting object relation for remote table "channel" */
export type Channel_Obj_Rel_Insert_Input = {
  data: Channel_Insert_Input;
  on_conflict?: Maybe<Channel_On_Conflict>;
};

/** on conflict condition type for table "channel" */
export type Channel_On_Conflict = {
  constraint: Channel_Constraint;
  update_columns: Array<Channel_Update_Column>;
  where?: Maybe<Channel_Bool_Exp>;
};

/** ordering options when selecting data from "channel" */
export type Channel_Order_By = {
  channel_created_at?: Maybe<Order_By>;
  guild?: Maybe<Guild_Order_By>;
  guild_id?: Maybe<Order_By>;
  id?: Maybe<Order_By>;
  messages_aggregate?: Maybe<Message_Aggregate_Order_By>;
  name?: Maybe<Order_By>;
  nsfw?: Maybe<Order_By>;
  topic?: Maybe<Order_By>;
  type?: Maybe<Order_By>;
};

/** primary key columns input for table: "channel" */
export type Channel_Pk_Columns_Input = {
  id: Scalars['String'];
};

/** select columns of table "channel" */
export enum Channel_Select_Column {
  /** column name */
  ChannelCreatedAt = 'channel_created_at',
  /** column name */
  GuildId = 'guild_id',
  /** column name */
  Id = 'id',
  /** column name */
  Name = 'name',
  /** column name */
  Nsfw = 'nsfw',
  /** column name */
  Topic = 'topic',
  /** column name */
  Type = 'type'
}

/** input type for updating data in table "channel" */
export type Channel_Set_Input = {
  channel_created_at?: Maybe<Scalars['timestamptz']>;
  guild_id?: Maybe<Scalars['String']>;
  id?: Maybe<Scalars['String']>;
  name?: Maybe<Scalars['String']>;
  nsfw?: Maybe<Scalars['Boolean']>;
  topic?: Maybe<Scalars['String']>;
  type?: Maybe<Scalars['String']>;
};

/** update columns of table "channel" */
export enum Channel_Update_Column {
  /** column name */
  ChannelCreatedAt = 'channel_created_at',
  /** column name */
  GuildId = 'guild_id',
  /** column name */
  Id = 'id',
  /** column name */
  Name = 'name',
  /** column name */
  Nsfw = 'nsfw',
  /** column name */
  Topic = 'topic',
  /** column name */
  Type = 'type'
}

/** columns and relationships of "game" */
export type Game = {
  /** An object relationship */
  channel: Channel;
  channel_id: Scalars['String'];
  finished: Scalars['Boolean'];
  id: Scalars['uuid'];
  /** An array relationship */
  questions: Array<Game_Question>;
  /** An aggregated array relationship */
  questions_aggregate: Game_Question_Aggregate;
  started_at: Scalars['timestamptz'];
};


/** columns and relationships of "game" */
export type GameQuestionsArgs = {
  distinct_on?: Maybe<Array<Game_Question_Select_Column>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<Game_Question_Order_By>>;
  where?: Maybe<Game_Question_Bool_Exp>;
};


/** columns and relationships of "game" */
export type GameQuestions_AggregateArgs = {
  distinct_on?: Maybe<Array<Game_Question_Select_Column>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<Game_Question_Order_By>>;
  where?: Maybe<Game_Question_Bool_Exp>;
};

/** aggregated selection of "game" */
export type Game_Aggregate = {
  aggregate?: Maybe<Game_Aggregate_Fields>;
  nodes: Array<Game>;
};

/** aggregate fields of "game" */
export type Game_Aggregate_Fields = {
  count?: Maybe<Scalars['Int']>;
  max?: Maybe<Game_Max_Fields>;
  min?: Maybe<Game_Min_Fields>;
};


/** aggregate fields of "game" */
export type Game_Aggregate_FieldsCountArgs = {
  columns?: Maybe<Array<Game_Select_Column>>;
  distinct?: Maybe<Scalars['Boolean']>;
};

/** order by aggregate values of table "game" */
export type Game_Aggregate_Order_By = {
  count?: Maybe<Order_By>;
  max?: Maybe<Game_Max_Order_By>;
  min?: Maybe<Game_Min_Order_By>;
};

/** columns and relationships of "game_answer" */
export type Game_Answer = {
  answer_user_id: Scalars['String'];
  answered_at: Scalars['timestamptz'];
  id: Scalars['uuid'];
  /** An object relationship */
  question: Game_Question;
  question_id: Scalars['uuid'];
  /** An object relationship */
  user: User;
  user_id: Scalars['String'];
};

/** aggregated selection of "game_answer" */
export type Game_Answer_Aggregate = {
  aggregate?: Maybe<Game_Answer_Aggregate_Fields>;
  nodes: Array<Game_Answer>;
};

/** aggregate fields of "game_answer" */
export type Game_Answer_Aggregate_Fields = {
  count?: Maybe<Scalars['Int']>;
  max?: Maybe<Game_Answer_Max_Fields>;
  min?: Maybe<Game_Answer_Min_Fields>;
};


/** aggregate fields of "game_answer" */
export type Game_Answer_Aggregate_FieldsCountArgs = {
  columns?: Maybe<Array<Game_Answer_Select_Column>>;
  distinct?: Maybe<Scalars['Boolean']>;
};

/** order by aggregate values of table "game_answer" */
export type Game_Answer_Aggregate_Order_By = {
  count?: Maybe<Order_By>;
  max?: Maybe<Game_Answer_Max_Order_By>;
  min?: Maybe<Game_Answer_Min_Order_By>;
};

/** input type for inserting array relation for remote table "game_answer" */
export type Game_Answer_Arr_Rel_Insert_Input = {
  data: Array<Game_Answer_Insert_Input>;
  on_conflict?: Maybe<Game_Answer_On_Conflict>;
};

/** Boolean expression to filter rows from the table "game_answer". All fields are combined with a logical 'AND'. */
export type Game_Answer_Bool_Exp = {
  _and?: Maybe<Array<Maybe<Game_Answer_Bool_Exp>>>;
  _not?: Maybe<Game_Answer_Bool_Exp>;
  _or?: Maybe<Array<Maybe<Game_Answer_Bool_Exp>>>;
  answer_user_id?: Maybe<String_Comparison_Exp>;
  answered_at?: Maybe<Timestamptz_Comparison_Exp>;
  id?: Maybe<Uuid_Comparison_Exp>;
  question?: Maybe<Game_Question_Bool_Exp>;
  question_id?: Maybe<Uuid_Comparison_Exp>;
  user?: Maybe<User_Bool_Exp>;
  user_id?: Maybe<String_Comparison_Exp>;
};

/** unique or primary key constraints on table "game_answer" */
export enum Game_Answer_Constraint {
  /** unique or primary key constraint */
  GameAnswerPkey = 'game_answer_pkey'
}

/** input type for inserting data into table "game_answer" */
export type Game_Answer_Insert_Input = {
  answer_user_id?: Maybe<Scalars['String']>;
  answered_at?: Maybe<Scalars['timestamptz']>;
  id?: Maybe<Scalars['uuid']>;
  question?: Maybe<Game_Question_Obj_Rel_Insert_Input>;
  question_id?: Maybe<Scalars['uuid']>;
  user?: Maybe<User_Obj_Rel_Insert_Input>;
  user_id?: Maybe<Scalars['String']>;
};

/** aggregate max on columns */
export type Game_Answer_Max_Fields = {
  answer_user_id?: Maybe<Scalars['String']>;
  answered_at?: Maybe<Scalars['timestamptz']>;
  id?: Maybe<Scalars['uuid']>;
  question_id?: Maybe<Scalars['uuid']>;
  user_id?: Maybe<Scalars['String']>;
};

/** order by max() on columns of table "game_answer" */
export type Game_Answer_Max_Order_By = {
  answer_user_id?: Maybe<Order_By>;
  answered_at?: Maybe<Order_By>;
  id?: Maybe<Order_By>;
  question_id?: Maybe<Order_By>;
  user_id?: Maybe<Order_By>;
};

/** aggregate min on columns */
export type Game_Answer_Min_Fields = {
  answer_user_id?: Maybe<Scalars['String']>;
  answered_at?: Maybe<Scalars['timestamptz']>;
  id?: Maybe<Scalars['uuid']>;
  question_id?: Maybe<Scalars['uuid']>;
  user_id?: Maybe<Scalars['String']>;
};

/** order by min() on columns of table "game_answer" */
export type Game_Answer_Min_Order_By = {
  answer_user_id?: Maybe<Order_By>;
  answered_at?: Maybe<Order_By>;
  id?: Maybe<Order_By>;
  question_id?: Maybe<Order_By>;
  user_id?: Maybe<Order_By>;
};

/** response of any mutation on the table "game_answer" */
export type Game_Answer_Mutation_Response = {
  /** number of affected rows by the mutation */
  affected_rows: Scalars['Int'];
  /** data of the affected rows by the mutation */
  returning: Array<Game_Answer>;
};

/** input type for inserting object relation for remote table "game_answer" */
export type Game_Answer_Obj_Rel_Insert_Input = {
  data: Game_Answer_Insert_Input;
  on_conflict?: Maybe<Game_Answer_On_Conflict>;
};

/** on conflict condition type for table "game_answer" */
export type Game_Answer_On_Conflict = {
  constraint: Game_Answer_Constraint;
  update_columns: Array<Game_Answer_Update_Column>;
  where?: Maybe<Game_Answer_Bool_Exp>;
};

/** ordering options when selecting data from "game_answer" */
export type Game_Answer_Order_By = {
  answer_user_id?: Maybe<Order_By>;
  answered_at?: Maybe<Order_By>;
  id?: Maybe<Order_By>;
  question?: Maybe<Game_Question_Order_By>;
  question_id?: Maybe<Order_By>;
  user?: Maybe<User_Order_By>;
  user_id?: Maybe<Order_By>;
};

/** primary key columns input for table: "game_answer" */
export type Game_Answer_Pk_Columns_Input = {
  id: Scalars['uuid'];
};

/** select columns of table "game_answer" */
export enum Game_Answer_Select_Column {
  /** column name */
  AnswerUserId = 'answer_user_id',
  /** column name */
  AnsweredAt = 'answered_at',
  /** column name */
  Id = 'id',
  /** column name */
  QuestionId = 'question_id',
  /** column name */
  UserId = 'user_id'
}

/** input type for updating data in table "game_answer" */
export type Game_Answer_Set_Input = {
  answer_user_id?: Maybe<Scalars['String']>;
  answered_at?: Maybe<Scalars['timestamptz']>;
  id?: Maybe<Scalars['uuid']>;
  question_id?: Maybe<Scalars['uuid']>;
  user_id?: Maybe<Scalars['String']>;
};

/** update columns of table "game_answer" */
export enum Game_Answer_Update_Column {
  /** column name */
  AnswerUserId = 'answer_user_id',
  /** column name */
  AnsweredAt = 'answered_at',
  /** column name */
  Id = 'id',
  /** column name */
  QuestionId = 'question_id',
  /** column name */
  UserId = 'user_id'
}

/** input type for inserting array relation for remote table "game" */
export type Game_Arr_Rel_Insert_Input = {
  data: Array<Game_Insert_Input>;
  on_conflict?: Maybe<Game_On_Conflict>;
};

/** Boolean expression to filter rows from the table "game". All fields are combined with a logical 'AND'. */
export type Game_Bool_Exp = {
  _and?: Maybe<Array<Maybe<Game_Bool_Exp>>>;
  _not?: Maybe<Game_Bool_Exp>;
  _or?: Maybe<Array<Maybe<Game_Bool_Exp>>>;
  channel?: Maybe<Channel_Bool_Exp>;
  channel_id?: Maybe<String_Comparison_Exp>;
  finished?: Maybe<Boolean_Comparison_Exp>;
  id?: Maybe<Uuid_Comparison_Exp>;
  questions?: Maybe<Game_Question_Bool_Exp>;
  started_at?: Maybe<Timestamptz_Comparison_Exp>;
};

/** unique or primary key constraints on table "game" */
export enum Game_Constraint {
  /** unique or primary key constraint */
  GamePkey = 'game_pkey'
}

/** input type for inserting data into table "game" */
export type Game_Insert_Input = {
  channel?: Maybe<Channel_Obj_Rel_Insert_Input>;
  channel_id?: Maybe<Scalars['String']>;
  finished?: Maybe<Scalars['Boolean']>;
  id?: Maybe<Scalars['uuid']>;
  questions?: Maybe<Game_Question_Arr_Rel_Insert_Input>;
  started_at?: Maybe<Scalars['timestamptz']>;
};

/** aggregate max on columns */
export type Game_Max_Fields = {
  channel_id?: Maybe<Scalars['String']>;
  id?: Maybe<Scalars['uuid']>;
  started_at?: Maybe<Scalars['timestamptz']>;
};

/** order by max() on columns of table "game" */
export type Game_Max_Order_By = {
  channel_id?: Maybe<Order_By>;
  id?: Maybe<Order_By>;
  started_at?: Maybe<Order_By>;
};

/** aggregate min on columns */
export type Game_Min_Fields = {
  channel_id?: Maybe<Scalars['String']>;
  id?: Maybe<Scalars['uuid']>;
  started_at?: Maybe<Scalars['timestamptz']>;
};

/** order by min() on columns of table "game" */
export type Game_Min_Order_By = {
  channel_id?: Maybe<Order_By>;
  id?: Maybe<Order_By>;
  started_at?: Maybe<Order_By>;
};

/** response of any mutation on the table "game" */
export type Game_Mutation_Response = {
  /** number of affected rows by the mutation */
  affected_rows: Scalars['Int'];
  /** data of the affected rows by the mutation */
  returning: Array<Game>;
};

/** input type for inserting object relation for remote table "game" */
export type Game_Obj_Rel_Insert_Input = {
  data: Game_Insert_Input;
  on_conflict?: Maybe<Game_On_Conflict>;
};

/** on conflict condition type for table "game" */
export type Game_On_Conflict = {
  constraint: Game_Constraint;
  update_columns: Array<Game_Update_Column>;
  where?: Maybe<Game_Bool_Exp>;
};

/** ordering options when selecting data from "game" */
export type Game_Order_By = {
  channel?: Maybe<Channel_Order_By>;
  channel_id?: Maybe<Order_By>;
  finished?: Maybe<Order_By>;
  id?: Maybe<Order_By>;
  questions_aggregate?: Maybe<Game_Question_Aggregate_Order_By>;
  started_at?: Maybe<Order_By>;
};

/** primary key columns input for table: "game" */
export type Game_Pk_Columns_Input = {
  id: Scalars['uuid'];
};

/** columns and relationships of "game_player" */
export type Game_Player = {
  emoji: Scalars['String'];
  /** An object relationship */
  guild: Guild;
  guild_id: Scalars['String'];
  id: Scalars['uuid'];
  /** An object relationship */
  user: User;
  user_id: Scalars['String'];
};

/** aggregated selection of "game_player" */
export type Game_Player_Aggregate = {
  aggregate?: Maybe<Game_Player_Aggregate_Fields>;
  nodes: Array<Game_Player>;
};

/** aggregate fields of "game_player" */
export type Game_Player_Aggregate_Fields = {
  count?: Maybe<Scalars['Int']>;
  max?: Maybe<Game_Player_Max_Fields>;
  min?: Maybe<Game_Player_Min_Fields>;
};


/** aggregate fields of "game_player" */
export type Game_Player_Aggregate_FieldsCountArgs = {
  columns?: Maybe<Array<Game_Player_Select_Column>>;
  distinct?: Maybe<Scalars['Boolean']>;
};

/** order by aggregate values of table "game_player" */
export type Game_Player_Aggregate_Order_By = {
  count?: Maybe<Order_By>;
  max?: Maybe<Game_Player_Max_Order_By>;
  min?: Maybe<Game_Player_Min_Order_By>;
};

/** input type for inserting array relation for remote table "game_player" */
export type Game_Player_Arr_Rel_Insert_Input = {
  data: Array<Game_Player_Insert_Input>;
  on_conflict?: Maybe<Game_Player_On_Conflict>;
};

/** Boolean expression to filter rows from the table "game_player". All fields are combined with a logical 'AND'. */
export type Game_Player_Bool_Exp = {
  _and?: Maybe<Array<Maybe<Game_Player_Bool_Exp>>>;
  _not?: Maybe<Game_Player_Bool_Exp>;
  _or?: Maybe<Array<Maybe<Game_Player_Bool_Exp>>>;
  emoji?: Maybe<String_Comparison_Exp>;
  guild?: Maybe<Guild_Bool_Exp>;
  guild_id?: Maybe<String_Comparison_Exp>;
  id?: Maybe<Uuid_Comparison_Exp>;
  user?: Maybe<User_Bool_Exp>;
  user_id?: Maybe<String_Comparison_Exp>;
};

/** unique or primary key constraints on table "game_player" */
export enum Game_Player_Constraint {
  /** unique or primary key constraint */
  GamePlayerPkey = 'game_player_pkey',
  /** unique or primary key constraint */
  GamePlayerUserIdGuildIdKey = 'game_player_user_id_guild_id_key'
}

/** input type for inserting data into table "game_player" */
export type Game_Player_Insert_Input = {
  emoji?: Maybe<Scalars['String']>;
  guild?: Maybe<Guild_Obj_Rel_Insert_Input>;
  guild_id?: Maybe<Scalars['String']>;
  id?: Maybe<Scalars['uuid']>;
  user?: Maybe<User_Obj_Rel_Insert_Input>;
  user_id?: Maybe<Scalars['String']>;
};

/** aggregate max on columns */
export type Game_Player_Max_Fields = {
  emoji?: Maybe<Scalars['String']>;
  guild_id?: Maybe<Scalars['String']>;
  id?: Maybe<Scalars['uuid']>;
  user_id?: Maybe<Scalars['String']>;
};

/** order by max() on columns of table "game_player" */
export type Game_Player_Max_Order_By = {
  emoji?: Maybe<Order_By>;
  guild_id?: Maybe<Order_By>;
  id?: Maybe<Order_By>;
  user_id?: Maybe<Order_By>;
};

/** aggregate min on columns */
export type Game_Player_Min_Fields = {
  emoji?: Maybe<Scalars['String']>;
  guild_id?: Maybe<Scalars['String']>;
  id?: Maybe<Scalars['uuid']>;
  user_id?: Maybe<Scalars['String']>;
};

/** order by min() on columns of table "game_player" */
export type Game_Player_Min_Order_By = {
  emoji?: Maybe<Order_By>;
  guild_id?: Maybe<Order_By>;
  id?: Maybe<Order_By>;
  user_id?: Maybe<Order_By>;
};

/** response of any mutation on the table "game_player" */
export type Game_Player_Mutation_Response = {
  /** number of affected rows by the mutation */
  affected_rows: Scalars['Int'];
  /** data of the affected rows by the mutation */
  returning: Array<Game_Player>;
};

/** input type for inserting object relation for remote table "game_player" */
export type Game_Player_Obj_Rel_Insert_Input = {
  data: Game_Player_Insert_Input;
  on_conflict?: Maybe<Game_Player_On_Conflict>;
};

/** on conflict condition type for table "game_player" */
export type Game_Player_On_Conflict = {
  constraint: Game_Player_Constraint;
  update_columns: Array<Game_Player_Update_Column>;
  where?: Maybe<Game_Player_Bool_Exp>;
};

/** ordering options when selecting data from "game_player" */
export type Game_Player_Order_By = {
  emoji?: Maybe<Order_By>;
  guild?: Maybe<Guild_Order_By>;
  guild_id?: Maybe<Order_By>;
  id?: Maybe<Order_By>;
  user?: Maybe<User_Order_By>;
  user_id?: Maybe<Order_By>;
};

/** primary key columns input for table: "game_player" */
export type Game_Player_Pk_Columns_Input = {
  id: Scalars['uuid'];
};

/** select columns of table "game_player" */
export enum Game_Player_Select_Column {
  /** column name */
  Emoji = 'emoji',
  /** column name */
  GuildId = 'guild_id',
  /** column name */
  Id = 'id',
  /** column name */
  UserId = 'user_id'
}

/** input type for updating data in table "game_player" */
export type Game_Player_Set_Input = {
  emoji?: Maybe<Scalars['String']>;
  guild_id?: Maybe<Scalars['String']>;
  id?: Maybe<Scalars['uuid']>;
  user_id?: Maybe<Scalars['String']>;
};

/** update columns of table "game_player" */
export enum Game_Player_Update_Column {
  /** column name */
  Emoji = 'emoji',
  /** column name */
  GuildId = 'guild_id',
  /** column name */
  Id = 'id',
  /** column name */
  UserId = 'user_id'
}

/** columns and relationships of "game_question" */
export type Game_Question = {
  /** An array relationship */
  answers: Array<Game_Answer>;
  /** An aggregated array relationship */
  answers_aggregate: Game_Answer_Aggregate;
  asked_at?: Maybe<Scalars['timestamptz']>;
  /** An object relationship */
  game: Game;
  game_id: Scalars['uuid'];
  id: Scalars['uuid'];
  /** An object relationship */
  message: Message;
  message_id: Scalars['String'];
  question_message_id?: Maybe<Scalars['String']>;
  timeout: Scalars['Boolean'];
  won: Scalars['Boolean'];
};


/** columns and relationships of "game_question" */
export type Game_QuestionAnswersArgs = {
  distinct_on?: Maybe<Array<Game_Answer_Select_Column>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<Game_Answer_Order_By>>;
  where?: Maybe<Game_Answer_Bool_Exp>;
};


/** columns and relationships of "game_question" */
export type Game_QuestionAnswers_AggregateArgs = {
  distinct_on?: Maybe<Array<Game_Answer_Select_Column>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<Game_Answer_Order_By>>;
  where?: Maybe<Game_Answer_Bool_Exp>;
};

/** aggregated selection of "game_question" */
export type Game_Question_Aggregate = {
  aggregate?: Maybe<Game_Question_Aggregate_Fields>;
  nodes: Array<Game_Question>;
};

/** aggregate fields of "game_question" */
export type Game_Question_Aggregate_Fields = {
  count?: Maybe<Scalars['Int']>;
  max?: Maybe<Game_Question_Max_Fields>;
  min?: Maybe<Game_Question_Min_Fields>;
};


/** aggregate fields of "game_question" */
export type Game_Question_Aggregate_FieldsCountArgs = {
  columns?: Maybe<Array<Game_Question_Select_Column>>;
  distinct?: Maybe<Scalars['Boolean']>;
};

/** order by aggregate values of table "game_question" */
export type Game_Question_Aggregate_Order_By = {
  count?: Maybe<Order_By>;
  max?: Maybe<Game_Question_Max_Order_By>;
  min?: Maybe<Game_Question_Min_Order_By>;
};

/** input type for inserting array relation for remote table "game_question" */
export type Game_Question_Arr_Rel_Insert_Input = {
  data: Array<Game_Question_Insert_Input>;
  on_conflict?: Maybe<Game_Question_On_Conflict>;
};

/** Boolean expression to filter rows from the table "game_question". All fields are combined with a logical 'AND'. */
export type Game_Question_Bool_Exp = {
  _and?: Maybe<Array<Maybe<Game_Question_Bool_Exp>>>;
  _not?: Maybe<Game_Question_Bool_Exp>;
  _or?: Maybe<Array<Maybe<Game_Question_Bool_Exp>>>;
  answers?: Maybe<Game_Answer_Bool_Exp>;
  asked_at?: Maybe<Timestamptz_Comparison_Exp>;
  game?: Maybe<Game_Bool_Exp>;
  game_id?: Maybe<Uuid_Comparison_Exp>;
  id?: Maybe<Uuid_Comparison_Exp>;
  message?: Maybe<Message_Bool_Exp>;
  message_id?: Maybe<String_Comparison_Exp>;
  question_message_id?: Maybe<String_Comparison_Exp>;
  timeout?: Maybe<Boolean_Comparison_Exp>;
  won?: Maybe<Boolean_Comparison_Exp>;
};

/** unique or primary key constraints on table "game_question" */
export enum Game_Question_Constraint {
  /** unique or primary key constraint */
  GameQuestionPkey = 'game_question_pkey',
  /** unique or primary key constraint */
  GameQuestionQuestionMessageIdKey = 'game_question_question_message_id_key'
}

/** input type for inserting data into table "game_question" */
export type Game_Question_Insert_Input = {
  answers?: Maybe<Game_Answer_Arr_Rel_Insert_Input>;
  asked_at?: Maybe<Scalars['timestamptz']>;
  game?: Maybe<Game_Obj_Rel_Insert_Input>;
  game_id?: Maybe<Scalars['uuid']>;
  id?: Maybe<Scalars['uuid']>;
  message?: Maybe<Message_Obj_Rel_Insert_Input>;
  message_id?: Maybe<Scalars['String']>;
  question_message_id?: Maybe<Scalars['String']>;
  timeout?: Maybe<Scalars['Boolean']>;
  won?: Maybe<Scalars['Boolean']>;
};

/** aggregate max on columns */
export type Game_Question_Max_Fields = {
  asked_at?: Maybe<Scalars['timestamptz']>;
  game_id?: Maybe<Scalars['uuid']>;
  id?: Maybe<Scalars['uuid']>;
  message_id?: Maybe<Scalars['String']>;
  question_message_id?: Maybe<Scalars['String']>;
};

/** order by max() on columns of table "game_question" */
export type Game_Question_Max_Order_By = {
  asked_at?: Maybe<Order_By>;
  game_id?: Maybe<Order_By>;
  id?: Maybe<Order_By>;
  message_id?: Maybe<Order_By>;
  question_message_id?: Maybe<Order_By>;
};

/** aggregate min on columns */
export type Game_Question_Min_Fields = {
  asked_at?: Maybe<Scalars['timestamptz']>;
  game_id?: Maybe<Scalars['uuid']>;
  id?: Maybe<Scalars['uuid']>;
  message_id?: Maybe<Scalars['String']>;
  question_message_id?: Maybe<Scalars['String']>;
};

/** order by min() on columns of table "game_question" */
export type Game_Question_Min_Order_By = {
  asked_at?: Maybe<Order_By>;
  game_id?: Maybe<Order_By>;
  id?: Maybe<Order_By>;
  message_id?: Maybe<Order_By>;
  question_message_id?: Maybe<Order_By>;
};

/** response of any mutation on the table "game_question" */
export type Game_Question_Mutation_Response = {
  /** number of affected rows by the mutation */
  affected_rows: Scalars['Int'];
  /** data of the affected rows by the mutation */
  returning: Array<Game_Question>;
};

/** input type for inserting object relation for remote table "game_question" */
export type Game_Question_Obj_Rel_Insert_Input = {
  data: Game_Question_Insert_Input;
  on_conflict?: Maybe<Game_Question_On_Conflict>;
};

/** on conflict condition type for table "game_question" */
export type Game_Question_On_Conflict = {
  constraint: Game_Question_Constraint;
  update_columns: Array<Game_Question_Update_Column>;
  where?: Maybe<Game_Question_Bool_Exp>;
};

/** ordering options when selecting data from "game_question" */
export type Game_Question_Order_By = {
  answers_aggregate?: Maybe<Game_Answer_Aggregate_Order_By>;
  asked_at?: Maybe<Order_By>;
  game?: Maybe<Game_Order_By>;
  game_id?: Maybe<Order_By>;
  id?: Maybe<Order_By>;
  message?: Maybe<Message_Order_By>;
  message_id?: Maybe<Order_By>;
  question_message_id?: Maybe<Order_By>;
  timeout?: Maybe<Order_By>;
  won?: Maybe<Order_By>;
};

/** primary key columns input for table: "game_question" */
export type Game_Question_Pk_Columns_Input = {
  id: Scalars['uuid'];
};

/** select columns of table "game_question" */
export enum Game_Question_Select_Column {
  /** column name */
  AskedAt = 'asked_at',
  /** column name */
  GameId = 'game_id',
  /** column name */
  Id = 'id',
  /** column name */
  MessageId = 'message_id',
  /** column name */
  QuestionMessageId = 'question_message_id',
  /** column name */
  Timeout = 'timeout',
  /** column name */
  Won = 'won'
}

/** input type for updating data in table "game_question" */
export type Game_Question_Set_Input = {
  asked_at?: Maybe<Scalars['timestamptz']>;
  game_id?: Maybe<Scalars['uuid']>;
  id?: Maybe<Scalars['uuid']>;
  message_id?: Maybe<Scalars['String']>;
  question_message_id?: Maybe<Scalars['String']>;
  timeout?: Maybe<Scalars['Boolean']>;
  won?: Maybe<Scalars['Boolean']>;
};

/** update columns of table "game_question" */
export enum Game_Question_Update_Column {
  /** column name */
  AskedAt = 'asked_at',
  /** column name */
  GameId = 'game_id',
  /** column name */
  Id = 'id',
  /** column name */
  MessageId = 'message_id',
  /** column name */
  QuestionMessageId = 'question_message_id',
  /** column name */
  Timeout = 'timeout',
  /** column name */
  Won = 'won'
}

/** select columns of table "game" */
export enum Game_Select_Column {
  /** column name */
  ChannelId = 'channel_id',
  /** column name */
  Finished = 'finished',
  /** column name */
  Id = 'id',
  /** column name */
  StartedAt = 'started_at'
}

/** input type for updating data in table "game" */
export type Game_Set_Input = {
  channel_id?: Maybe<Scalars['String']>;
  finished?: Maybe<Scalars['Boolean']>;
  id?: Maybe<Scalars['uuid']>;
  started_at?: Maybe<Scalars['timestamptz']>;
};

/** update columns of table "game" */
export enum Game_Update_Column {
  /** column name */
  ChannelId = 'channel_id',
  /** column name */
  Finished = 'finished',
  /** column name */
  Id = 'id',
  /** column name */
  StartedAt = 'started_at'
}

export type Generate_Game_Messages_Args = {
  guild?: Maybe<Scalars['String']>;
  min_length?: Maybe<Scalars['Int']>;
  n_questions?: Maybe<Scalars['Int']>;
};

/** columns and relationships of "guild" */
export type Guild = {
  /** An array relationship */
  channels: Array<Channel>;
  /** An aggregated array relationship */
  channels_aggregate: Channel_Aggregate;
  guild_created_at: Scalars['timestamptz'];
  id: Scalars['String'];
  joined_at: Scalars['timestamptz'];
  /** An array relationship */
  members: Array<Member>;
  /** An aggregated array relationship */
  members_aggregate: Member_Aggregate;
  name: Scalars['String'];
  /** An object relationship */
  owner: User;
  owner_id: Scalars['String'];
  region: Scalars['String'];
};


/** columns and relationships of "guild" */
export type GuildChannelsArgs = {
  distinct_on?: Maybe<Array<Channel_Select_Column>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<Channel_Order_By>>;
  where?: Maybe<Channel_Bool_Exp>;
};


/** columns and relationships of "guild" */
export type GuildChannels_AggregateArgs = {
  distinct_on?: Maybe<Array<Channel_Select_Column>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<Channel_Order_By>>;
  where?: Maybe<Channel_Bool_Exp>;
};


/** columns and relationships of "guild" */
export type GuildMembersArgs = {
  distinct_on?: Maybe<Array<Member_Select_Column>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<Member_Order_By>>;
  where?: Maybe<Member_Bool_Exp>;
};


/** columns and relationships of "guild" */
export type GuildMembers_AggregateArgs = {
  distinct_on?: Maybe<Array<Member_Select_Column>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<Member_Order_By>>;
  where?: Maybe<Member_Bool_Exp>;
};

/** aggregated selection of "guild" */
export type Guild_Aggregate = {
  aggregate?: Maybe<Guild_Aggregate_Fields>;
  nodes: Array<Guild>;
};

/** aggregate fields of "guild" */
export type Guild_Aggregate_Fields = {
  count?: Maybe<Scalars['Int']>;
  max?: Maybe<Guild_Max_Fields>;
  min?: Maybe<Guild_Min_Fields>;
};


/** aggregate fields of "guild" */
export type Guild_Aggregate_FieldsCountArgs = {
  columns?: Maybe<Array<Guild_Select_Column>>;
  distinct?: Maybe<Scalars['Boolean']>;
};

/** order by aggregate values of table "guild" */
export type Guild_Aggregate_Order_By = {
  count?: Maybe<Order_By>;
  max?: Maybe<Guild_Max_Order_By>;
  min?: Maybe<Guild_Min_Order_By>;
};

/** input type for inserting array relation for remote table "guild" */
export type Guild_Arr_Rel_Insert_Input = {
  data: Array<Guild_Insert_Input>;
  on_conflict?: Maybe<Guild_On_Conflict>;
};

/** Boolean expression to filter rows from the table "guild". All fields are combined with a logical 'AND'. */
export type Guild_Bool_Exp = {
  _and?: Maybe<Array<Maybe<Guild_Bool_Exp>>>;
  _not?: Maybe<Guild_Bool_Exp>;
  _or?: Maybe<Array<Maybe<Guild_Bool_Exp>>>;
  channels?: Maybe<Channel_Bool_Exp>;
  guild_created_at?: Maybe<Timestamptz_Comparison_Exp>;
  id?: Maybe<String_Comparison_Exp>;
  joined_at?: Maybe<Timestamptz_Comparison_Exp>;
  members?: Maybe<Member_Bool_Exp>;
  name?: Maybe<String_Comparison_Exp>;
  owner?: Maybe<User_Bool_Exp>;
  owner_id?: Maybe<String_Comparison_Exp>;
  region?: Maybe<String_Comparison_Exp>;
};

/** unique or primary key constraints on table "guild" */
export enum Guild_Constraint {
  /** unique or primary key constraint */
  GuildPkey = 'guild_pkey'
}

/** input type for inserting data into table "guild" */
export type Guild_Insert_Input = {
  channels?: Maybe<Channel_Arr_Rel_Insert_Input>;
  guild_created_at?: Maybe<Scalars['timestamptz']>;
  id?: Maybe<Scalars['String']>;
  joined_at?: Maybe<Scalars['timestamptz']>;
  members?: Maybe<Member_Arr_Rel_Insert_Input>;
  name?: Maybe<Scalars['String']>;
  owner?: Maybe<User_Obj_Rel_Insert_Input>;
  owner_id?: Maybe<Scalars['String']>;
  region?: Maybe<Scalars['String']>;
};

/** aggregate max on columns */
export type Guild_Max_Fields = {
  guild_created_at?: Maybe<Scalars['timestamptz']>;
  id?: Maybe<Scalars['String']>;
  joined_at?: Maybe<Scalars['timestamptz']>;
  name?: Maybe<Scalars['String']>;
  owner_id?: Maybe<Scalars['String']>;
  region?: Maybe<Scalars['String']>;
};

/** order by max() on columns of table "guild" */
export type Guild_Max_Order_By = {
  guild_created_at?: Maybe<Order_By>;
  id?: Maybe<Order_By>;
  joined_at?: Maybe<Order_By>;
  name?: Maybe<Order_By>;
  owner_id?: Maybe<Order_By>;
  region?: Maybe<Order_By>;
};

/** aggregate min on columns */
export type Guild_Min_Fields = {
  guild_created_at?: Maybe<Scalars['timestamptz']>;
  id?: Maybe<Scalars['String']>;
  joined_at?: Maybe<Scalars['timestamptz']>;
  name?: Maybe<Scalars['String']>;
  owner_id?: Maybe<Scalars['String']>;
  region?: Maybe<Scalars['String']>;
};

/** order by min() on columns of table "guild" */
export type Guild_Min_Order_By = {
  guild_created_at?: Maybe<Order_By>;
  id?: Maybe<Order_By>;
  joined_at?: Maybe<Order_By>;
  name?: Maybe<Order_By>;
  owner_id?: Maybe<Order_By>;
  region?: Maybe<Order_By>;
};

/** response of any mutation on the table "guild" */
export type Guild_Mutation_Response = {
  /** number of affected rows by the mutation */
  affected_rows: Scalars['Int'];
  /** data of the affected rows by the mutation */
  returning: Array<Guild>;
};

/** input type for inserting object relation for remote table "guild" */
export type Guild_Obj_Rel_Insert_Input = {
  data: Guild_Insert_Input;
  on_conflict?: Maybe<Guild_On_Conflict>;
};

/** on conflict condition type for table "guild" */
export type Guild_On_Conflict = {
  constraint: Guild_Constraint;
  update_columns: Array<Guild_Update_Column>;
  where?: Maybe<Guild_Bool_Exp>;
};

/** ordering options when selecting data from "guild" */
export type Guild_Order_By = {
  channels_aggregate?: Maybe<Channel_Aggregate_Order_By>;
  guild_created_at?: Maybe<Order_By>;
  id?: Maybe<Order_By>;
  joined_at?: Maybe<Order_By>;
  members_aggregate?: Maybe<Member_Aggregate_Order_By>;
  name?: Maybe<Order_By>;
  owner?: Maybe<User_Order_By>;
  owner_id?: Maybe<Order_By>;
  region?: Maybe<Order_By>;
};

/** primary key columns input for table: "guild" */
export type Guild_Pk_Columns_Input = {
  id: Scalars['String'];
};

/** select columns of table "guild" */
export enum Guild_Select_Column {
  /** column name */
  GuildCreatedAt = 'guild_created_at',
  /** column name */
  Id = 'id',
  /** column name */
  JoinedAt = 'joined_at',
  /** column name */
  Name = 'name',
  /** column name */
  OwnerId = 'owner_id',
  /** column name */
  Region = 'region'
}

/** input type for updating data in table "guild" */
export type Guild_Set_Input = {
  guild_created_at?: Maybe<Scalars['timestamptz']>;
  id?: Maybe<Scalars['String']>;
  joined_at?: Maybe<Scalars['timestamptz']>;
  name?: Maybe<Scalars['String']>;
  owner_id?: Maybe<Scalars['String']>;
  region?: Maybe<Scalars['String']>;
};

/** update columns of table "guild" */
export enum Guild_Update_Column {
  /** column name */
  GuildCreatedAt = 'guild_created_at',
  /** column name */
  Id = 'id',
  /** column name */
  JoinedAt = 'joined_at',
  /** column name */
  Name = 'name',
  /** column name */
  OwnerId = 'owner_id',
  /** column name */
  Region = 'region'
}

/** columns and relationships of "member" */
export type Member = {
  display_name?: Maybe<Scalars['String']>;
  /** An object relationship */
  guild: Guild;
  guild_id: Scalars['String'];
  member_joined_at: Scalars['timestamptz'];
  nickname?: Maybe<Scalars['String']>;
  /** An object relationship */
  user: User;
  user_id: Scalars['String'];
};

/** aggregated selection of "member" */
export type Member_Aggregate = {
  aggregate?: Maybe<Member_Aggregate_Fields>;
  nodes: Array<Member>;
};

/** aggregate fields of "member" */
export type Member_Aggregate_Fields = {
  count?: Maybe<Scalars['Int']>;
  max?: Maybe<Member_Max_Fields>;
  min?: Maybe<Member_Min_Fields>;
};


/** aggregate fields of "member" */
export type Member_Aggregate_FieldsCountArgs = {
  columns?: Maybe<Array<Member_Select_Column>>;
  distinct?: Maybe<Scalars['Boolean']>;
};

/** order by aggregate values of table "member" */
export type Member_Aggregate_Order_By = {
  count?: Maybe<Order_By>;
  max?: Maybe<Member_Max_Order_By>;
  min?: Maybe<Member_Min_Order_By>;
};

/** input type for inserting array relation for remote table "member" */
export type Member_Arr_Rel_Insert_Input = {
  data: Array<Member_Insert_Input>;
  on_conflict?: Maybe<Member_On_Conflict>;
};

/** Boolean expression to filter rows from the table "member". All fields are combined with a logical 'AND'. */
export type Member_Bool_Exp = {
  _and?: Maybe<Array<Maybe<Member_Bool_Exp>>>;
  _not?: Maybe<Member_Bool_Exp>;
  _or?: Maybe<Array<Maybe<Member_Bool_Exp>>>;
  display_name?: Maybe<String_Comparison_Exp>;
  guild?: Maybe<Guild_Bool_Exp>;
  guild_id?: Maybe<String_Comparison_Exp>;
  member_joined_at?: Maybe<Timestamptz_Comparison_Exp>;
  nickname?: Maybe<String_Comparison_Exp>;
  user?: Maybe<User_Bool_Exp>;
  user_id?: Maybe<String_Comparison_Exp>;
};

/** unique or primary key constraints on table "member" */
export enum Member_Constraint {
  /** unique or primary key constraint */
  MemberPkey = 'member_pkey'
}

/** input type for inserting data into table "member" */
export type Member_Insert_Input = {
  display_name?: Maybe<Scalars['String']>;
  guild?: Maybe<Guild_Obj_Rel_Insert_Input>;
  guild_id?: Maybe<Scalars['String']>;
  member_joined_at?: Maybe<Scalars['timestamptz']>;
  nickname?: Maybe<Scalars['String']>;
  user?: Maybe<User_Obj_Rel_Insert_Input>;
  user_id?: Maybe<Scalars['String']>;
};

/** aggregate max on columns */
export type Member_Max_Fields = {
  display_name?: Maybe<Scalars['String']>;
  guild_id?: Maybe<Scalars['String']>;
  member_joined_at?: Maybe<Scalars['timestamptz']>;
  nickname?: Maybe<Scalars['String']>;
  user_id?: Maybe<Scalars['String']>;
};

/** order by max() on columns of table "member" */
export type Member_Max_Order_By = {
  display_name?: Maybe<Order_By>;
  guild_id?: Maybe<Order_By>;
  member_joined_at?: Maybe<Order_By>;
  nickname?: Maybe<Order_By>;
  user_id?: Maybe<Order_By>;
};

/** aggregate min on columns */
export type Member_Min_Fields = {
  display_name?: Maybe<Scalars['String']>;
  guild_id?: Maybe<Scalars['String']>;
  member_joined_at?: Maybe<Scalars['timestamptz']>;
  nickname?: Maybe<Scalars['String']>;
  user_id?: Maybe<Scalars['String']>;
};

/** order by min() on columns of table "member" */
export type Member_Min_Order_By = {
  display_name?: Maybe<Order_By>;
  guild_id?: Maybe<Order_By>;
  member_joined_at?: Maybe<Order_By>;
  nickname?: Maybe<Order_By>;
  user_id?: Maybe<Order_By>;
};

/** response of any mutation on the table "member" */
export type Member_Mutation_Response = {
  /** number of affected rows by the mutation */
  affected_rows: Scalars['Int'];
  /** data of the affected rows by the mutation */
  returning: Array<Member>;
};

/** input type for inserting object relation for remote table "member" */
export type Member_Obj_Rel_Insert_Input = {
  data: Member_Insert_Input;
  on_conflict?: Maybe<Member_On_Conflict>;
};

/** on conflict condition type for table "member" */
export type Member_On_Conflict = {
  constraint: Member_Constraint;
  update_columns: Array<Member_Update_Column>;
  where?: Maybe<Member_Bool_Exp>;
};

/** ordering options when selecting data from "member" */
export type Member_Order_By = {
  display_name?: Maybe<Order_By>;
  guild?: Maybe<Guild_Order_By>;
  guild_id?: Maybe<Order_By>;
  member_joined_at?: Maybe<Order_By>;
  nickname?: Maybe<Order_By>;
  user?: Maybe<User_Order_By>;
  user_id?: Maybe<Order_By>;
};

/** primary key columns input for table: "member" */
export type Member_Pk_Columns_Input = {
  guild_id: Scalars['String'];
  user_id: Scalars['String'];
};

/** select columns of table "member" */
export enum Member_Select_Column {
  /** column name */
  DisplayName = 'display_name',
  /** column name */
  GuildId = 'guild_id',
  /** column name */
  MemberJoinedAt = 'member_joined_at',
  /** column name */
  Nickname = 'nickname',
  /** column name */
  UserId = 'user_id'
}

/** input type for updating data in table "member" */
export type Member_Set_Input = {
  display_name?: Maybe<Scalars['String']>;
  guild_id?: Maybe<Scalars['String']>;
  member_joined_at?: Maybe<Scalars['timestamptz']>;
  nickname?: Maybe<Scalars['String']>;
  user_id?: Maybe<Scalars['String']>;
};

/** update columns of table "member" */
export enum Member_Update_Column {
  /** column name */
  DisplayName = 'display_name',
  /** column name */
  GuildId = 'guild_id',
  /** column name */
  MemberJoinedAt = 'member_joined_at',
  /** column name */
  Nickname = 'nickname',
  /** column name */
  UserId = 'user_id'
}

/** columns and relationships of "message" */
export type Message = {
  /** An object relationship */
  channel: Channel;
  channel_id: Scalars['String'];
  content: Scalars['String'];
  id: Scalars['String'];
  message_created_at: Scalars['timestamptz'];
  /** An object relationship */
  user?: Maybe<User>;
  user_id?: Maybe<Scalars['String']>;
};

/** aggregated selection of "message" */
export type Message_Aggregate = {
  aggregate?: Maybe<Message_Aggregate_Fields>;
  nodes: Array<Message>;
};

/** aggregate fields of "message" */
export type Message_Aggregate_Fields = {
  count?: Maybe<Scalars['Int']>;
  max?: Maybe<Message_Max_Fields>;
  min?: Maybe<Message_Min_Fields>;
};


/** aggregate fields of "message" */
export type Message_Aggregate_FieldsCountArgs = {
  columns?: Maybe<Array<Message_Select_Column>>;
  distinct?: Maybe<Scalars['Boolean']>;
};

/** order by aggregate values of table "message" */
export type Message_Aggregate_Order_By = {
  count?: Maybe<Order_By>;
  max?: Maybe<Message_Max_Order_By>;
  min?: Maybe<Message_Min_Order_By>;
};

/** input type for inserting array relation for remote table "message" */
export type Message_Arr_Rel_Insert_Input = {
  data: Array<Message_Insert_Input>;
  on_conflict?: Maybe<Message_On_Conflict>;
};

/** Boolean expression to filter rows from the table "message". All fields are combined with a logical 'AND'. */
export type Message_Bool_Exp = {
  _and?: Maybe<Array<Maybe<Message_Bool_Exp>>>;
  _not?: Maybe<Message_Bool_Exp>;
  _or?: Maybe<Array<Maybe<Message_Bool_Exp>>>;
  channel?: Maybe<Channel_Bool_Exp>;
  channel_id?: Maybe<String_Comparison_Exp>;
  content?: Maybe<String_Comparison_Exp>;
  id?: Maybe<String_Comparison_Exp>;
  message_created_at?: Maybe<Timestamptz_Comparison_Exp>;
  user?: Maybe<User_Bool_Exp>;
  user_id?: Maybe<String_Comparison_Exp>;
};

/** unique or primary key constraints on table "message" */
export enum Message_Constraint {
  /** unique or primary key constraint */
  MessagePkey = 'message_pkey'
}

/** input type for inserting data into table "message" */
export type Message_Insert_Input = {
  channel?: Maybe<Channel_Obj_Rel_Insert_Input>;
  channel_id?: Maybe<Scalars['String']>;
  content?: Maybe<Scalars['String']>;
  id?: Maybe<Scalars['String']>;
  message_created_at?: Maybe<Scalars['timestamptz']>;
  user?: Maybe<User_Obj_Rel_Insert_Input>;
  user_id?: Maybe<Scalars['String']>;
};

/** aggregate max on columns */
export type Message_Max_Fields = {
  channel_id?: Maybe<Scalars['String']>;
  content?: Maybe<Scalars['String']>;
  id?: Maybe<Scalars['String']>;
  message_created_at?: Maybe<Scalars['timestamptz']>;
  user_id?: Maybe<Scalars['String']>;
};

/** order by max() on columns of table "message" */
export type Message_Max_Order_By = {
  channel_id?: Maybe<Order_By>;
  content?: Maybe<Order_By>;
  id?: Maybe<Order_By>;
  message_created_at?: Maybe<Order_By>;
  user_id?: Maybe<Order_By>;
};

/** aggregate min on columns */
export type Message_Min_Fields = {
  channel_id?: Maybe<Scalars['String']>;
  content?: Maybe<Scalars['String']>;
  id?: Maybe<Scalars['String']>;
  message_created_at?: Maybe<Scalars['timestamptz']>;
  user_id?: Maybe<Scalars['String']>;
};

/** order by min() on columns of table "message" */
export type Message_Min_Order_By = {
  channel_id?: Maybe<Order_By>;
  content?: Maybe<Order_By>;
  id?: Maybe<Order_By>;
  message_created_at?: Maybe<Order_By>;
  user_id?: Maybe<Order_By>;
};

/** response of any mutation on the table "message" */
export type Message_Mutation_Response = {
  /** number of affected rows by the mutation */
  affected_rows: Scalars['Int'];
  /** data of the affected rows by the mutation */
  returning: Array<Message>;
};

/** input type for inserting object relation for remote table "message" */
export type Message_Obj_Rel_Insert_Input = {
  data: Message_Insert_Input;
  on_conflict?: Maybe<Message_On_Conflict>;
};

/** on conflict condition type for table "message" */
export type Message_On_Conflict = {
  constraint: Message_Constraint;
  update_columns: Array<Message_Update_Column>;
  where?: Maybe<Message_Bool_Exp>;
};

/** ordering options when selecting data from "message" */
export type Message_Order_By = {
  channel?: Maybe<Channel_Order_By>;
  channel_id?: Maybe<Order_By>;
  content?: Maybe<Order_By>;
  id?: Maybe<Order_By>;
  message_created_at?: Maybe<Order_By>;
  user?: Maybe<User_Order_By>;
  user_id?: Maybe<Order_By>;
};

/** primary key columns input for table: "message" */
export type Message_Pk_Columns_Input = {
  id: Scalars['String'];
};

/** select columns of table "message" */
export enum Message_Select_Column {
  /** column name */
  ChannelId = 'channel_id',
  /** column name */
  Content = 'content',
  /** column name */
  Id = 'id',
  /** column name */
  MessageCreatedAt = 'message_created_at',
  /** column name */
  UserId = 'user_id'
}

/** input type for updating data in table "message" */
export type Message_Set_Input = {
  channel_id?: Maybe<Scalars['String']>;
  content?: Maybe<Scalars['String']>;
  id?: Maybe<Scalars['String']>;
  message_created_at?: Maybe<Scalars['timestamptz']>;
  user_id?: Maybe<Scalars['String']>;
};

/** update columns of table "message" */
export enum Message_Update_Column {
  /** column name */
  ChannelId = 'channel_id',
  /** column name */
  Content = 'content',
  /** column name */
  Id = 'id',
  /** column name */
  MessageCreatedAt = 'message_created_at',
  /** column name */
  UserId = 'user_id'
}

/** mutation root */
export type Mutation_Root = {
  /** delete data from the table: "channel" */
  delete_channel?: Maybe<Channel_Mutation_Response>;
  /** delete single row from the table: "channel" */
  delete_channel_by_pk?: Maybe<Channel>;
  /** delete data from the table: "game" */
  delete_game?: Maybe<Game_Mutation_Response>;
  /** delete data from the table: "game_answer" */
  delete_game_answer?: Maybe<Game_Answer_Mutation_Response>;
  /** delete single row from the table: "game_answer" */
  delete_game_answer_by_pk?: Maybe<Game_Answer>;
  /** delete single row from the table: "game" */
  delete_game_by_pk?: Maybe<Game>;
  /** delete data from the table: "game_player" */
  delete_game_player?: Maybe<Game_Player_Mutation_Response>;
  /** delete single row from the table: "game_player" */
  delete_game_player_by_pk?: Maybe<Game_Player>;
  /** delete data from the table: "game_question" */
  delete_game_question?: Maybe<Game_Question_Mutation_Response>;
  /** delete single row from the table: "game_question" */
  delete_game_question_by_pk?: Maybe<Game_Question>;
  /** delete data from the table: "guild" */
  delete_guild?: Maybe<Guild_Mutation_Response>;
  /** delete single row from the table: "guild" */
  delete_guild_by_pk?: Maybe<Guild>;
  /** delete data from the table: "member" */
  delete_member?: Maybe<Member_Mutation_Response>;
  /** delete single row from the table: "member" */
  delete_member_by_pk?: Maybe<Member>;
  /** delete data from the table: "message" */
  delete_message?: Maybe<Message_Mutation_Response>;
  /** delete single row from the table: "message" */
  delete_message_by_pk?: Maybe<Message>;
  /** delete data from the table: "user" */
  delete_user?: Maybe<User_Mutation_Response>;
  /** delete single row from the table: "user" */
  delete_user_by_pk?: Maybe<User>;
  /** delete data from the table: "user_presence" */
  delete_user_presence?: Maybe<User_Presence_Mutation_Response>;
  /** delete data from the table: "user_presence_activity" */
  delete_user_presence_activity?: Maybe<User_Presence_Activity_Mutation_Response>;
  /** delete single row from the table: "user_presence_activity" */
  delete_user_presence_activity_by_pk?: Maybe<User_Presence_Activity>;
  /** delete single row from the table: "user_presence" */
  delete_user_presence_by_pk?: Maybe<User_Presence>;
  /** insert data into the table: "channel" */
  insert_channel?: Maybe<Channel_Mutation_Response>;
  /** insert a single row into the table: "channel" */
  insert_channel_one?: Maybe<Channel>;
  /** insert data into the table: "game" */
  insert_game?: Maybe<Game_Mutation_Response>;
  /** insert data into the table: "game_answer" */
  insert_game_answer?: Maybe<Game_Answer_Mutation_Response>;
  /** insert a single row into the table: "game_answer" */
  insert_game_answer_one?: Maybe<Game_Answer>;
  /** insert a single row into the table: "game" */
  insert_game_one?: Maybe<Game>;
  /** insert data into the table: "game_player" */
  insert_game_player?: Maybe<Game_Player_Mutation_Response>;
  /** insert a single row into the table: "game_player" */
  insert_game_player_one?: Maybe<Game_Player>;
  /** insert data into the table: "game_question" */
  insert_game_question?: Maybe<Game_Question_Mutation_Response>;
  /** insert a single row into the table: "game_question" */
  insert_game_question_one?: Maybe<Game_Question>;
  /** insert data into the table: "guild" */
  insert_guild?: Maybe<Guild_Mutation_Response>;
  /** insert a single row into the table: "guild" */
  insert_guild_one?: Maybe<Guild>;
  /** insert data into the table: "member" */
  insert_member?: Maybe<Member_Mutation_Response>;
  /** insert a single row into the table: "member" */
  insert_member_one?: Maybe<Member>;
  /** insert data into the table: "message" */
  insert_message?: Maybe<Message_Mutation_Response>;
  /** insert a single row into the table: "message" */
  insert_message_one?: Maybe<Message>;
  /** insert data into the table: "user" */
  insert_user?: Maybe<User_Mutation_Response>;
  /** insert a single row into the table: "user" */
  insert_user_one?: Maybe<User>;
  /** insert data into the table: "user_presence" */
  insert_user_presence?: Maybe<User_Presence_Mutation_Response>;
  /** insert data into the table: "user_presence_activity" */
  insert_user_presence_activity?: Maybe<User_Presence_Activity_Mutation_Response>;
  /** insert a single row into the table: "user_presence_activity" */
  insert_user_presence_activity_one?: Maybe<User_Presence_Activity>;
  /** insert a single row into the table: "user_presence" */
  insert_user_presence_one?: Maybe<User_Presence>;
  /** update data of the table: "channel" */
  update_channel?: Maybe<Channel_Mutation_Response>;
  /** update single row of the table: "channel" */
  update_channel_by_pk?: Maybe<Channel>;
  /** update data of the table: "game" */
  update_game?: Maybe<Game_Mutation_Response>;
  /** update data of the table: "game_answer" */
  update_game_answer?: Maybe<Game_Answer_Mutation_Response>;
  /** update single row of the table: "game_answer" */
  update_game_answer_by_pk?: Maybe<Game_Answer>;
  /** update single row of the table: "game" */
  update_game_by_pk?: Maybe<Game>;
  /** update data of the table: "game_player" */
  update_game_player?: Maybe<Game_Player_Mutation_Response>;
  /** update single row of the table: "game_player" */
  update_game_player_by_pk?: Maybe<Game_Player>;
  /** update data of the table: "game_question" */
  update_game_question?: Maybe<Game_Question_Mutation_Response>;
  /** update single row of the table: "game_question" */
  update_game_question_by_pk?: Maybe<Game_Question>;
  /** update data of the table: "guild" */
  update_guild?: Maybe<Guild_Mutation_Response>;
  /** update single row of the table: "guild" */
  update_guild_by_pk?: Maybe<Guild>;
  /** update data of the table: "member" */
  update_member?: Maybe<Member_Mutation_Response>;
  /** update single row of the table: "member" */
  update_member_by_pk?: Maybe<Member>;
  /** update data of the table: "message" */
  update_message?: Maybe<Message_Mutation_Response>;
  /** update single row of the table: "message" */
  update_message_by_pk?: Maybe<Message>;
  /** update data of the table: "user" */
  update_user?: Maybe<User_Mutation_Response>;
  /** update single row of the table: "user" */
  update_user_by_pk?: Maybe<User>;
  /** update data of the table: "user_presence" */
  update_user_presence?: Maybe<User_Presence_Mutation_Response>;
  /** update data of the table: "user_presence_activity" */
  update_user_presence_activity?: Maybe<User_Presence_Activity_Mutation_Response>;
  /** update single row of the table: "user_presence_activity" */
  update_user_presence_activity_by_pk?: Maybe<User_Presence_Activity>;
  /** update single row of the table: "user_presence" */
  update_user_presence_by_pk?: Maybe<User_Presence>;
};


/** mutation root */
export type Mutation_RootDelete_ChannelArgs = {
  where: Channel_Bool_Exp;
};


/** mutation root */
export type Mutation_RootDelete_Channel_By_PkArgs = {
  id: Scalars['String'];
};


/** mutation root */
export type Mutation_RootDelete_GameArgs = {
  where: Game_Bool_Exp;
};


/** mutation root */
export type Mutation_RootDelete_Game_AnswerArgs = {
  where: Game_Answer_Bool_Exp;
};


/** mutation root */
export type Mutation_RootDelete_Game_Answer_By_PkArgs = {
  id: Scalars['uuid'];
};


/** mutation root */
export type Mutation_RootDelete_Game_By_PkArgs = {
  id: Scalars['uuid'];
};


/** mutation root */
export type Mutation_RootDelete_Game_PlayerArgs = {
  where: Game_Player_Bool_Exp;
};


/** mutation root */
export type Mutation_RootDelete_Game_Player_By_PkArgs = {
  id: Scalars['uuid'];
};


/** mutation root */
export type Mutation_RootDelete_Game_QuestionArgs = {
  where: Game_Question_Bool_Exp;
};


/** mutation root */
export type Mutation_RootDelete_Game_Question_By_PkArgs = {
  id: Scalars['uuid'];
};


/** mutation root */
export type Mutation_RootDelete_GuildArgs = {
  where: Guild_Bool_Exp;
};


/** mutation root */
export type Mutation_RootDelete_Guild_By_PkArgs = {
  id: Scalars['String'];
};


/** mutation root */
export type Mutation_RootDelete_MemberArgs = {
  where: Member_Bool_Exp;
};


/** mutation root */
export type Mutation_RootDelete_Member_By_PkArgs = {
  guild_id: Scalars['String'];
  user_id: Scalars['String'];
};


/** mutation root */
export type Mutation_RootDelete_MessageArgs = {
  where: Message_Bool_Exp;
};


/** mutation root */
export type Mutation_RootDelete_Message_By_PkArgs = {
  id: Scalars['String'];
};


/** mutation root */
export type Mutation_RootDelete_UserArgs = {
  where: User_Bool_Exp;
};


/** mutation root */
export type Mutation_RootDelete_User_By_PkArgs = {
  id: Scalars['String'];
};


/** mutation root */
export type Mutation_RootDelete_User_PresenceArgs = {
  where: User_Presence_Bool_Exp;
};


/** mutation root */
export type Mutation_RootDelete_User_Presence_ActivityArgs = {
  where: User_Presence_Activity_Bool_Exp;
};


/** mutation root */
export type Mutation_RootDelete_User_Presence_Activity_By_PkArgs = {
  id: Scalars['uuid'];
};


/** mutation root */
export type Mutation_RootDelete_User_Presence_By_PkArgs = {
  id: Scalars['uuid'];
};


/** mutation root */
export type Mutation_RootInsert_ChannelArgs = {
  objects: Array<Channel_Insert_Input>;
  on_conflict?: Maybe<Channel_On_Conflict>;
};


/** mutation root */
export type Mutation_RootInsert_Channel_OneArgs = {
  object: Channel_Insert_Input;
  on_conflict?: Maybe<Channel_On_Conflict>;
};


/** mutation root */
export type Mutation_RootInsert_GameArgs = {
  objects: Array<Game_Insert_Input>;
  on_conflict?: Maybe<Game_On_Conflict>;
};


/** mutation root */
export type Mutation_RootInsert_Game_AnswerArgs = {
  objects: Array<Game_Answer_Insert_Input>;
  on_conflict?: Maybe<Game_Answer_On_Conflict>;
};


/** mutation root */
export type Mutation_RootInsert_Game_Answer_OneArgs = {
  object: Game_Answer_Insert_Input;
  on_conflict?: Maybe<Game_Answer_On_Conflict>;
};


/** mutation root */
export type Mutation_RootInsert_Game_OneArgs = {
  object: Game_Insert_Input;
  on_conflict?: Maybe<Game_On_Conflict>;
};


/** mutation root */
export type Mutation_RootInsert_Game_PlayerArgs = {
  objects: Array<Game_Player_Insert_Input>;
  on_conflict?: Maybe<Game_Player_On_Conflict>;
};


/** mutation root */
export type Mutation_RootInsert_Game_Player_OneArgs = {
  object: Game_Player_Insert_Input;
  on_conflict?: Maybe<Game_Player_On_Conflict>;
};


/** mutation root */
export type Mutation_RootInsert_Game_QuestionArgs = {
  objects: Array<Game_Question_Insert_Input>;
  on_conflict?: Maybe<Game_Question_On_Conflict>;
};


/** mutation root */
export type Mutation_RootInsert_Game_Question_OneArgs = {
  object: Game_Question_Insert_Input;
  on_conflict?: Maybe<Game_Question_On_Conflict>;
};


/** mutation root */
export type Mutation_RootInsert_GuildArgs = {
  objects: Array<Guild_Insert_Input>;
  on_conflict?: Maybe<Guild_On_Conflict>;
};


/** mutation root */
export type Mutation_RootInsert_Guild_OneArgs = {
  object: Guild_Insert_Input;
  on_conflict?: Maybe<Guild_On_Conflict>;
};


/** mutation root */
export type Mutation_RootInsert_MemberArgs = {
  objects: Array<Member_Insert_Input>;
  on_conflict?: Maybe<Member_On_Conflict>;
};


/** mutation root */
export type Mutation_RootInsert_Member_OneArgs = {
  object: Member_Insert_Input;
  on_conflict?: Maybe<Member_On_Conflict>;
};


/** mutation root */
export type Mutation_RootInsert_MessageArgs = {
  objects: Array<Message_Insert_Input>;
  on_conflict?: Maybe<Message_On_Conflict>;
};


/** mutation root */
export type Mutation_RootInsert_Message_OneArgs = {
  object: Message_Insert_Input;
  on_conflict?: Maybe<Message_On_Conflict>;
};


/** mutation root */
export type Mutation_RootInsert_UserArgs = {
  objects: Array<User_Insert_Input>;
  on_conflict?: Maybe<User_On_Conflict>;
};


/** mutation root */
export type Mutation_RootInsert_User_OneArgs = {
  object: User_Insert_Input;
  on_conflict?: Maybe<User_On_Conflict>;
};


/** mutation root */
export type Mutation_RootInsert_User_PresenceArgs = {
  objects: Array<User_Presence_Insert_Input>;
  on_conflict?: Maybe<User_Presence_On_Conflict>;
};


/** mutation root */
export type Mutation_RootInsert_User_Presence_ActivityArgs = {
  objects: Array<User_Presence_Activity_Insert_Input>;
  on_conflict?: Maybe<User_Presence_Activity_On_Conflict>;
};


/** mutation root */
export type Mutation_RootInsert_User_Presence_Activity_OneArgs = {
  object: User_Presence_Activity_Insert_Input;
  on_conflict?: Maybe<User_Presence_Activity_On_Conflict>;
};


/** mutation root */
export type Mutation_RootInsert_User_Presence_OneArgs = {
  object: User_Presence_Insert_Input;
  on_conflict?: Maybe<User_Presence_On_Conflict>;
};


/** mutation root */
export type Mutation_RootUpdate_ChannelArgs = {
  _set?: Maybe<Channel_Set_Input>;
  where: Channel_Bool_Exp;
};


/** mutation root */
export type Mutation_RootUpdate_Channel_By_PkArgs = {
  _set?: Maybe<Channel_Set_Input>;
  pk_columns: Channel_Pk_Columns_Input;
};


/** mutation root */
export type Mutation_RootUpdate_GameArgs = {
  _set?: Maybe<Game_Set_Input>;
  where: Game_Bool_Exp;
};


/** mutation root */
export type Mutation_RootUpdate_Game_AnswerArgs = {
  _set?: Maybe<Game_Answer_Set_Input>;
  where: Game_Answer_Bool_Exp;
};


/** mutation root */
export type Mutation_RootUpdate_Game_Answer_By_PkArgs = {
  _set?: Maybe<Game_Answer_Set_Input>;
  pk_columns: Game_Answer_Pk_Columns_Input;
};


/** mutation root */
export type Mutation_RootUpdate_Game_By_PkArgs = {
  _set?: Maybe<Game_Set_Input>;
  pk_columns: Game_Pk_Columns_Input;
};


/** mutation root */
export type Mutation_RootUpdate_Game_PlayerArgs = {
  _set?: Maybe<Game_Player_Set_Input>;
  where: Game_Player_Bool_Exp;
};


/** mutation root */
export type Mutation_RootUpdate_Game_Player_By_PkArgs = {
  _set?: Maybe<Game_Player_Set_Input>;
  pk_columns: Game_Player_Pk_Columns_Input;
};


/** mutation root */
export type Mutation_RootUpdate_Game_QuestionArgs = {
  _set?: Maybe<Game_Question_Set_Input>;
  where: Game_Question_Bool_Exp;
};


/** mutation root */
export type Mutation_RootUpdate_Game_Question_By_PkArgs = {
  _set?: Maybe<Game_Question_Set_Input>;
  pk_columns: Game_Question_Pk_Columns_Input;
};


/** mutation root */
export type Mutation_RootUpdate_GuildArgs = {
  _set?: Maybe<Guild_Set_Input>;
  where: Guild_Bool_Exp;
};


/** mutation root */
export type Mutation_RootUpdate_Guild_By_PkArgs = {
  _set?: Maybe<Guild_Set_Input>;
  pk_columns: Guild_Pk_Columns_Input;
};


/** mutation root */
export type Mutation_RootUpdate_MemberArgs = {
  _set?: Maybe<Member_Set_Input>;
  where: Member_Bool_Exp;
};


/** mutation root */
export type Mutation_RootUpdate_Member_By_PkArgs = {
  _set?: Maybe<Member_Set_Input>;
  pk_columns: Member_Pk_Columns_Input;
};


/** mutation root */
export type Mutation_RootUpdate_MessageArgs = {
  _set?: Maybe<Message_Set_Input>;
  where: Message_Bool_Exp;
};


/** mutation root */
export type Mutation_RootUpdate_Message_By_PkArgs = {
  _set?: Maybe<Message_Set_Input>;
  pk_columns: Message_Pk_Columns_Input;
};


/** mutation root */
export type Mutation_RootUpdate_UserArgs = {
  _set?: Maybe<User_Set_Input>;
  where: User_Bool_Exp;
};


/** mutation root */
export type Mutation_RootUpdate_User_By_PkArgs = {
  _set?: Maybe<User_Set_Input>;
  pk_columns: User_Pk_Columns_Input;
};


/** mutation root */
export type Mutation_RootUpdate_User_PresenceArgs = {
  _set?: Maybe<User_Presence_Set_Input>;
  where: User_Presence_Bool_Exp;
};


/** mutation root */
export type Mutation_RootUpdate_User_Presence_ActivityArgs = {
  _inc?: Maybe<User_Presence_Activity_Inc_Input>;
  _set?: Maybe<User_Presence_Activity_Set_Input>;
  where: User_Presence_Activity_Bool_Exp;
};


/** mutation root */
export type Mutation_RootUpdate_User_Presence_Activity_By_PkArgs = {
  _inc?: Maybe<User_Presence_Activity_Inc_Input>;
  _set?: Maybe<User_Presence_Activity_Set_Input>;
  pk_columns: User_Presence_Activity_Pk_Columns_Input;
};


/** mutation root */
export type Mutation_RootUpdate_User_Presence_By_PkArgs = {
  _set?: Maybe<User_Presence_Set_Input>;
  pk_columns: User_Presence_Pk_Columns_Input;
};

/** column ordering options */
export enum Order_By {
  /** in the ascending order, nulls last */
  Asc = 'asc',
  /** in the ascending order, nulls first */
  AscNullsFirst = 'asc_nulls_first',
  /** in the ascending order, nulls last */
  AscNullsLast = 'asc_nulls_last',
  /** in the descending order, nulls first */
  Desc = 'desc',
  /** in the descending order, nulls first */
  DescNullsFirst = 'desc_nulls_first',
  /** in the descending order, nulls last */
  DescNullsLast = 'desc_nulls_last'
}

/** query root */
export type Query_Root = {
  /** fetch data from the table: "channel" */
  channel: Array<Channel>;
  /** fetch aggregated fields from the table: "channel" */
  channel_aggregate: Channel_Aggregate;
  /** fetch data from the table: "channel" using primary key columns */
  channel_by_pk?: Maybe<Channel>;
  /** fetch data from the table: "game" */
  game: Array<Game>;
  /** fetch aggregated fields from the table: "game" */
  game_aggregate: Game_Aggregate;
  /** fetch data from the table: "game_answer" */
  game_answer: Array<Game_Answer>;
  /** fetch aggregated fields from the table: "game_answer" */
  game_answer_aggregate: Game_Answer_Aggregate;
  /** fetch data from the table: "game_answer" using primary key columns */
  game_answer_by_pk?: Maybe<Game_Answer>;
  /** fetch data from the table: "game" using primary key columns */
  game_by_pk?: Maybe<Game>;
  /** fetch data from the table: "game_player" */
  game_player: Array<Game_Player>;
  /** fetch aggregated fields from the table: "game_player" */
  game_player_aggregate: Game_Player_Aggregate;
  /** fetch data from the table: "game_player" using primary key columns */
  game_player_by_pk?: Maybe<Game_Player>;
  /** fetch data from the table: "game_question" */
  game_question: Array<Game_Question>;
  /** fetch aggregated fields from the table: "game_question" */
  game_question_aggregate: Game_Question_Aggregate;
  /** fetch data from the table: "game_question" using primary key columns */
  game_question_by_pk?: Maybe<Game_Question>;
  /** execute function "generate_game_messages" which returns "message" */
  generate_game_messages: Array<Message>;
  /** execute function "generate_game_messages" and query aggregates on result of table type "message" */
  generate_game_messages_aggregate: Message_Aggregate;
  /** fetch data from the table: "guild" */
  guild: Array<Guild>;
  /** fetch aggregated fields from the table: "guild" */
  guild_aggregate: Guild_Aggregate;
  /** fetch data from the table: "guild" using primary key columns */
  guild_by_pk?: Maybe<Guild>;
  /** fetch data from the table: "member" */
  member: Array<Member>;
  /** fetch aggregated fields from the table: "member" */
  member_aggregate: Member_Aggregate;
  /** fetch data from the table: "member" using primary key columns */
  member_by_pk?: Maybe<Member>;
  /** fetch data from the table: "message" */
  message: Array<Message>;
  /** fetch aggregated fields from the table: "message" */
  message_aggregate: Message_Aggregate;
  /** fetch data from the table: "message" using primary key columns */
  message_by_pk?: Maybe<Message>;
  /** fetch data from the table: "user" */
  user: Array<User>;
  /** fetch aggregated fields from the table: "user" */
  user_aggregate: User_Aggregate;
  /** fetch data from the table: "user" using primary key columns */
  user_by_pk?: Maybe<User>;
  /** fetch data from the table: "user_presence" */
  user_presence: Array<User_Presence>;
  /** fetch data from the table: "user_presence_activity" */
  user_presence_activity: Array<User_Presence_Activity>;
  /** fetch aggregated fields from the table: "user_presence_activity" */
  user_presence_activity_aggregate: User_Presence_Activity_Aggregate;
  /** fetch data from the table: "user_presence_activity" using primary key columns */
  user_presence_activity_by_pk?: Maybe<User_Presence_Activity>;
  /** fetch aggregated fields from the table: "user_presence" */
  user_presence_aggregate: User_Presence_Aggregate;
  /** fetch data from the table: "user_presence" using primary key columns */
  user_presence_by_pk?: Maybe<User_Presence>;
};


/** query root */
export type Query_RootChannelArgs = {
  distinct_on?: Maybe<Array<Channel_Select_Column>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<Channel_Order_By>>;
  where?: Maybe<Channel_Bool_Exp>;
};


/** query root */
export type Query_RootChannel_AggregateArgs = {
  distinct_on?: Maybe<Array<Channel_Select_Column>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<Channel_Order_By>>;
  where?: Maybe<Channel_Bool_Exp>;
};


/** query root */
export type Query_RootChannel_By_PkArgs = {
  id: Scalars['String'];
};


/** query root */
export type Query_RootGameArgs = {
  distinct_on?: Maybe<Array<Game_Select_Column>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<Game_Order_By>>;
  where?: Maybe<Game_Bool_Exp>;
};


/** query root */
export type Query_RootGame_AggregateArgs = {
  distinct_on?: Maybe<Array<Game_Select_Column>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<Game_Order_By>>;
  where?: Maybe<Game_Bool_Exp>;
};


/** query root */
export type Query_RootGame_AnswerArgs = {
  distinct_on?: Maybe<Array<Game_Answer_Select_Column>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<Game_Answer_Order_By>>;
  where?: Maybe<Game_Answer_Bool_Exp>;
};


/** query root */
export type Query_RootGame_Answer_AggregateArgs = {
  distinct_on?: Maybe<Array<Game_Answer_Select_Column>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<Game_Answer_Order_By>>;
  where?: Maybe<Game_Answer_Bool_Exp>;
};


/** query root */
export type Query_RootGame_Answer_By_PkArgs = {
  id: Scalars['uuid'];
};


/** query root */
export type Query_RootGame_By_PkArgs = {
  id: Scalars['uuid'];
};


/** query root */
export type Query_RootGame_PlayerArgs = {
  distinct_on?: Maybe<Array<Game_Player_Select_Column>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<Game_Player_Order_By>>;
  where?: Maybe<Game_Player_Bool_Exp>;
};


/** query root */
export type Query_RootGame_Player_AggregateArgs = {
  distinct_on?: Maybe<Array<Game_Player_Select_Column>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<Game_Player_Order_By>>;
  where?: Maybe<Game_Player_Bool_Exp>;
};


/** query root */
export type Query_RootGame_Player_By_PkArgs = {
  id: Scalars['uuid'];
};


/** query root */
export type Query_RootGame_QuestionArgs = {
  distinct_on?: Maybe<Array<Game_Question_Select_Column>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<Game_Question_Order_By>>;
  where?: Maybe<Game_Question_Bool_Exp>;
};


/** query root */
export type Query_RootGame_Question_AggregateArgs = {
  distinct_on?: Maybe<Array<Game_Question_Select_Column>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<Game_Question_Order_By>>;
  where?: Maybe<Game_Question_Bool_Exp>;
};


/** query root */
export type Query_RootGame_Question_By_PkArgs = {
  id: Scalars['uuid'];
};


/** query root */
export type Query_RootGenerate_Game_MessagesArgs = {
  args: Generate_Game_Messages_Args;
  distinct_on?: Maybe<Array<Message_Select_Column>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<Message_Order_By>>;
  where?: Maybe<Message_Bool_Exp>;
};


/** query root */
export type Query_RootGenerate_Game_Messages_AggregateArgs = {
  args: Generate_Game_Messages_Args;
  distinct_on?: Maybe<Array<Message_Select_Column>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<Message_Order_By>>;
  where?: Maybe<Message_Bool_Exp>;
};


/** query root */
export type Query_RootGuildArgs = {
  distinct_on?: Maybe<Array<Guild_Select_Column>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<Guild_Order_By>>;
  where?: Maybe<Guild_Bool_Exp>;
};


/** query root */
export type Query_RootGuild_AggregateArgs = {
  distinct_on?: Maybe<Array<Guild_Select_Column>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<Guild_Order_By>>;
  where?: Maybe<Guild_Bool_Exp>;
};


/** query root */
export type Query_RootGuild_By_PkArgs = {
  id: Scalars['String'];
};


/** query root */
export type Query_RootMemberArgs = {
  distinct_on?: Maybe<Array<Member_Select_Column>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<Member_Order_By>>;
  where?: Maybe<Member_Bool_Exp>;
};


/** query root */
export type Query_RootMember_AggregateArgs = {
  distinct_on?: Maybe<Array<Member_Select_Column>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<Member_Order_By>>;
  where?: Maybe<Member_Bool_Exp>;
};


/** query root */
export type Query_RootMember_By_PkArgs = {
  guild_id: Scalars['String'];
  user_id: Scalars['String'];
};


/** query root */
export type Query_RootMessageArgs = {
  distinct_on?: Maybe<Array<Message_Select_Column>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<Message_Order_By>>;
  where?: Maybe<Message_Bool_Exp>;
};


/** query root */
export type Query_RootMessage_AggregateArgs = {
  distinct_on?: Maybe<Array<Message_Select_Column>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<Message_Order_By>>;
  where?: Maybe<Message_Bool_Exp>;
};


/** query root */
export type Query_RootMessage_By_PkArgs = {
  id: Scalars['String'];
};


/** query root */
export type Query_RootUserArgs = {
  distinct_on?: Maybe<Array<User_Select_Column>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<User_Order_By>>;
  where?: Maybe<User_Bool_Exp>;
};


/** query root */
export type Query_RootUser_AggregateArgs = {
  distinct_on?: Maybe<Array<User_Select_Column>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<User_Order_By>>;
  where?: Maybe<User_Bool_Exp>;
};


/** query root */
export type Query_RootUser_By_PkArgs = {
  id: Scalars['String'];
};


/** query root */
export type Query_RootUser_PresenceArgs = {
  distinct_on?: Maybe<Array<User_Presence_Select_Column>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<User_Presence_Order_By>>;
  where?: Maybe<User_Presence_Bool_Exp>;
};


/** query root */
export type Query_RootUser_Presence_ActivityArgs = {
  distinct_on?: Maybe<Array<User_Presence_Activity_Select_Column>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<User_Presence_Activity_Order_By>>;
  where?: Maybe<User_Presence_Activity_Bool_Exp>;
};


/** query root */
export type Query_RootUser_Presence_Activity_AggregateArgs = {
  distinct_on?: Maybe<Array<User_Presence_Activity_Select_Column>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<User_Presence_Activity_Order_By>>;
  where?: Maybe<User_Presence_Activity_Bool_Exp>;
};


/** query root */
export type Query_RootUser_Presence_Activity_By_PkArgs = {
  id: Scalars['uuid'];
};


/** query root */
export type Query_RootUser_Presence_AggregateArgs = {
  distinct_on?: Maybe<Array<User_Presence_Select_Column>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<User_Presence_Order_By>>;
  where?: Maybe<User_Presence_Bool_Exp>;
};


/** query root */
export type Query_RootUser_Presence_By_PkArgs = {
  id: Scalars['uuid'];
};

/** subscription root */
export type Subscription_Root = {
  /** fetch data from the table: "channel" */
  channel: Array<Channel>;
  /** fetch aggregated fields from the table: "channel" */
  channel_aggregate: Channel_Aggregate;
  /** fetch data from the table: "channel" using primary key columns */
  channel_by_pk?: Maybe<Channel>;
  /** fetch data from the table: "game" */
  game: Array<Game>;
  /** fetch aggregated fields from the table: "game" */
  game_aggregate: Game_Aggregate;
  /** fetch data from the table: "game_answer" */
  game_answer: Array<Game_Answer>;
  /** fetch aggregated fields from the table: "game_answer" */
  game_answer_aggregate: Game_Answer_Aggregate;
  /** fetch data from the table: "game_answer" using primary key columns */
  game_answer_by_pk?: Maybe<Game_Answer>;
  /** fetch data from the table: "game" using primary key columns */
  game_by_pk?: Maybe<Game>;
  /** fetch data from the table: "game_player" */
  game_player: Array<Game_Player>;
  /** fetch aggregated fields from the table: "game_player" */
  game_player_aggregate: Game_Player_Aggregate;
  /** fetch data from the table: "game_player" using primary key columns */
  game_player_by_pk?: Maybe<Game_Player>;
  /** fetch data from the table: "game_question" */
  game_question: Array<Game_Question>;
  /** fetch aggregated fields from the table: "game_question" */
  game_question_aggregate: Game_Question_Aggregate;
  /** fetch data from the table: "game_question" using primary key columns */
  game_question_by_pk?: Maybe<Game_Question>;
  /** execute function "generate_game_messages" which returns "message" */
  generate_game_messages: Array<Message>;
  /** execute function "generate_game_messages" and query aggregates on result of table type "message" */
  generate_game_messages_aggregate: Message_Aggregate;
  /** fetch data from the table: "guild" */
  guild: Array<Guild>;
  /** fetch aggregated fields from the table: "guild" */
  guild_aggregate: Guild_Aggregate;
  /** fetch data from the table: "guild" using primary key columns */
  guild_by_pk?: Maybe<Guild>;
  /** fetch data from the table: "member" */
  member: Array<Member>;
  /** fetch aggregated fields from the table: "member" */
  member_aggregate: Member_Aggregate;
  /** fetch data from the table: "member" using primary key columns */
  member_by_pk?: Maybe<Member>;
  /** fetch data from the table: "message" */
  message: Array<Message>;
  /** fetch aggregated fields from the table: "message" */
  message_aggregate: Message_Aggregate;
  /** fetch data from the table: "message" using primary key columns */
  message_by_pk?: Maybe<Message>;
  /** fetch data from the table: "user" */
  user: Array<User>;
  /** fetch aggregated fields from the table: "user" */
  user_aggregate: User_Aggregate;
  /** fetch data from the table: "user" using primary key columns */
  user_by_pk?: Maybe<User>;
  /** fetch data from the table: "user_presence" */
  user_presence: Array<User_Presence>;
  /** fetch data from the table: "user_presence_activity" */
  user_presence_activity: Array<User_Presence_Activity>;
  /** fetch aggregated fields from the table: "user_presence_activity" */
  user_presence_activity_aggregate: User_Presence_Activity_Aggregate;
  /** fetch data from the table: "user_presence_activity" using primary key columns */
  user_presence_activity_by_pk?: Maybe<User_Presence_Activity>;
  /** fetch aggregated fields from the table: "user_presence" */
  user_presence_aggregate: User_Presence_Aggregate;
  /** fetch data from the table: "user_presence" using primary key columns */
  user_presence_by_pk?: Maybe<User_Presence>;
};


/** subscription root */
export type Subscription_RootChannelArgs = {
  distinct_on?: Maybe<Array<Channel_Select_Column>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<Channel_Order_By>>;
  where?: Maybe<Channel_Bool_Exp>;
};


/** subscription root */
export type Subscription_RootChannel_AggregateArgs = {
  distinct_on?: Maybe<Array<Channel_Select_Column>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<Channel_Order_By>>;
  where?: Maybe<Channel_Bool_Exp>;
};


/** subscription root */
export type Subscription_RootChannel_By_PkArgs = {
  id: Scalars['String'];
};


/** subscription root */
export type Subscription_RootGameArgs = {
  distinct_on?: Maybe<Array<Game_Select_Column>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<Game_Order_By>>;
  where?: Maybe<Game_Bool_Exp>;
};


/** subscription root */
export type Subscription_RootGame_AggregateArgs = {
  distinct_on?: Maybe<Array<Game_Select_Column>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<Game_Order_By>>;
  where?: Maybe<Game_Bool_Exp>;
};


/** subscription root */
export type Subscription_RootGame_AnswerArgs = {
  distinct_on?: Maybe<Array<Game_Answer_Select_Column>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<Game_Answer_Order_By>>;
  where?: Maybe<Game_Answer_Bool_Exp>;
};


/** subscription root */
export type Subscription_RootGame_Answer_AggregateArgs = {
  distinct_on?: Maybe<Array<Game_Answer_Select_Column>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<Game_Answer_Order_By>>;
  where?: Maybe<Game_Answer_Bool_Exp>;
};


/** subscription root */
export type Subscription_RootGame_Answer_By_PkArgs = {
  id: Scalars['uuid'];
};


/** subscription root */
export type Subscription_RootGame_By_PkArgs = {
  id: Scalars['uuid'];
};


/** subscription root */
export type Subscription_RootGame_PlayerArgs = {
  distinct_on?: Maybe<Array<Game_Player_Select_Column>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<Game_Player_Order_By>>;
  where?: Maybe<Game_Player_Bool_Exp>;
};


/** subscription root */
export type Subscription_RootGame_Player_AggregateArgs = {
  distinct_on?: Maybe<Array<Game_Player_Select_Column>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<Game_Player_Order_By>>;
  where?: Maybe<Game_Player_Bool_Exp>;
};


/** subscription root */
export type Subscription_RootGame_Player_By_PkArgs = {
  id: Scalars['uuid'];
};


/** subscription root */
export type Subscription_RootGame_QuestionArgs = {
  distinct_on?: Maybe<Array<Game_Question_Select_Column>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<Game_Question_Order_By>>;
  where?: Maybe<Game_Question_Bool_Exp>;
};


/** subscription root */
export type Subscription_RootGame_Question_AggregateArgs = {
  distinct_on?: Maybe<Array<Game_Question_Select_Column>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<Game_Question_Order_By>>;
  where?: Maybe<Game_Question_Bool_Exp>;
};


/** subscription root */
export type Subscription_RootGame_Question_By_PkArgs = {
  id: Scalars['uuid'];
};


/** subscription root */
export type Subscription_RootGenerate_Game_MessagesArgs = {
  args: Generate_Game_Messages_Args;
  distinct_on?: Maybe<Array<Message_Select_Column>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<Message_Order_By>>;
  where?: Maybe<Message_Bool_Exp>;
};


/** subscription root */
export type Subscription_RootGenerate_Game_Messages_AggregateArgs = {
  args: Generate_Game_Messages_Args;
  distinct_on?: Maybe<Array<Message_Select_Column>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<Message_Order_By>>;
  where?: Maybe<Message_Bool_Exp>;
};


/** subscription root */
export type Subscription_RootGuildArgs = {
  distinct_on?: Maybe<Array<Guild_Select_Column>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<Guild_Order_By>>;
  where?: Maybe<Guild_Bool_Exp>;
};


/** subscription root */
export type Subscription_RootGuild_AggregateArgs = {
  distinct_on?: Maybe<Array<Guild_Select_Column>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<Guild_Order_By>>;
  where?: Maybe<Guild_Bool_Exp>;
};


/** subscription root */
export type Subscription_RootGuild_By_PkArgs = {
  id: Scalars['String'];
};


/** subscription root */
export type Subscription_RootMemberArgs = {
  distinct_on?: Maybe<Array<Member_Select_Column>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<Member_Order_By>>;
  where?: Maybe<Member_Bool_Exp>;
};


/** subscription root */
export type Subscription_RootMember_AggregateArgs = {
  distinct_on?: Maybe<Array<Member_Select_Column>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<Member_Order_By>>;
  where?: Maybe<Member_Bool_Exp>;
};


/** subscription root */
export type Subscription_RootMember_By_PkArgs = {
  guild_id: Scalars['String'];
  user_id: Scalars['String'];
};


/** subscription root */
export type Subscription_RootMessageArgs = {
  distinct_on?: Maybe<Array<Message_Select_Column>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<Message_Order_By>>;
  where?: Maybe<Message_Bool_Exp>;
};


/** subscription root */
export type Subscription_RootMessage_AggregateArgs = {
  distinct_on?: Maybe<Array<Message_Select_Column>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<Message_Order_By>>;
  where?: Maybe<Message_Bool_Exp>;
};


/** subscription root */
export type Subscription_RootMessage_By_PkArgs = {
  id: Scalars['String'];
};


/** subscription root */
export type Subscription_RootUserArgs = {
  distinct_on?: Maybe<Array<User_Select_Column>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<User_Order_By>>;
  where?: Maybe<User_Bool_Exp>;
};


/** subscription root */
export type Subscription_RootUser_AggregateArgs = {
  distinct_on?: Maybe<Array<User_Select_Column>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<User_Order_By>>;
  where?: Maybe<User_Bool_Exp>;
};


/** subscription root */
export type Subscription_RootUser_By_PkArgs = {
  id: Scalars['String'];
};


/** subscription root */
export type Subscription_RootUser_PresenceArgs = {
  distinct_on?: Maybe<Array<User_Presence_Select_Column>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<User_Presence_Order_By>>;
  where?: Maybe<User_Presence_Bool_Exp>;
};


/** subscription root */
export type Subscription_RootUser_Presence_ActivityArgs = {
  distinct_on?: Maybe<Array<User_Presence_Activity_Select_Column>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<User_Presence_Activity_Order_By>>;
  where?: Maybe<User_Presence_Activity_Bool_Exp>;
};


/** subscription root */
export type Subscription_RootUser_Presence_Activity_AggregateArgs = {
  distinct_on?: Maybe<Array<User_Presence_Activity_Select_Column>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<User_Presence_Activity_Order_By>>;
  where?: Maybe<User_Presence_Activity_Bool_Exp>;
};


/** subscription root */
export type Subscription_RootUser_Presence_Activity_By_PkArgs = {
  id: Scalars['uuid'];
};


/** subscription root */
export type Subscription_RootUser_Presence_AggregateArgs = {
  distinct_on?: Maybe<Array<User_Presence_Select_Column>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<User_Presence_Order_By>>;
  where?: Maybe<User_Presence_Bool_Exp>;
};


/** subscription root */
export type Subscription_RootUser_Presence_By_PkArgs = {
  id: Scalars['uuid'];
};


/** expression to compare columns of type timestamptz. All fields are combined with logical 'AND'. */
export type Timestamptz_Comparison_Exp = {
  _eq?: Maybe<Scalars['timestamptz']>;
  _gt?: Maybe<Scalars['timestamptz']>;
  _gte?: Maybe<Scalars['timestamptz']>;
  _in?: Maybe<Array<Scalars['timestamptz']>>;
  _is_null?: Maybe<Scalars['Boolean']>;
  _lt?: Maybe<Scalars['timestamptz']>;
  _lte?: Maybe<Scalars['timestamptz']>;
  _neq?: Maybe<Scalars['timestamptz']>;
  _nin?: Maybe<Array<Scalars['timestamptz']>>;
};

/** columns and relationships of "user" */
export type User = {
  avatar?: Maybe<Scalars['String']>;
  bot: Scalars['Boolean'];
  /** An array relationship */
  guilds: Array<Guild>;
  /** An aggregated array relationship */
  guilds_aggregate: Guild_Aggregate;
  id: Scalars['String'];
  locale?: Maybe<Scalars['String']>;
  /** An array relationship */
  members: Array<Member>;
  /** An aggregated array relationship */
  members_aggregate: Member_Aggregate;
  /** An array relationship */
  messages: Array<Message>;
  /** An aggregated array relationship */
  messages_aggregate: Message_Aggregate;
  /** An array relationship */
  presences: Array<User_Presence>;
  /** An aggregated array relationship */
  presences_aggregate: User_Presence_Aggregate;
  tag: Scalars['String'];
  user_created_at: Scalars['timestamptz'];
  username: Scalars['String'];
};


/** columns and relationships of "user" */
export type UserGuildsArgs = {
  distinct_on?: Maybe<Array<Guild_Select_Column>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<Guild_Order_By>>;
  where?: Maybe<Guild_Bool_Exp>;
};


/** columns and relationships of "user" */
export type UserGuilds_AggregateArgs = {
  distinct_on?: Maybe<Array<Guild_Select_Column>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<Guild_Order_By>>;
  where?: Maybe<Guild_Bool_Exp>;
};


/** columns and relationships of "user" */
export type UserMembersArgs = {
  distinct_on?: Maybe<Array<Member_Select_Column>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<Member_Order_By>>;
  where?: Maybe<Member_Bool_Exp>;
};


/** columns and relationships of "user" */
export type UserMembers_AggregateArgs = {
  distinct_on?: Maybe<Array<Member_Select_Column>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<Member_Order_By>>;
  where?: Maybe<Member_Bool_Exp>;
};


/** columns and relationships of "user" */
export type UserMessagesArgs = {
  distinct_on?: Maybe<Array<Message_Select_Column>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<Message_Order_By>>;
  where?: Maybe<Message_Bool_Exp>;
};


/** columns and relationships of "user" */
export type UserMessages_AggregateArgs = {
  distinct_on?: Maybe<Array<Message_Select_Column>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<Message_Order_By>>;
  where?: Maybe<Message_Bool_Exp>;
};


/** columns and relationships of "user" */
export type UserPresencesArgs = {
  distinct_on?: Maybe<Array<User_Presence_Select_Column>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<User_Presence_Order_By>>;
  where?: Maybe<User_Presence_Bool_Exp>;
};


/** columns and relationships of "user" */
export type UserPresences_AggregateArgs = {
  distinct_on?: Maybe<Array<User_Presence_Select_Column>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<User_Presence_Order_By>>;
  where?: Maybe<User_Presence_Bool_Exp>;
};

/** aggregated selection of "user" */
export type User_Aggregate = {
  aggregate?: Maybe<User_Aggregate_Fields>;
  nodes: Array<User>;
};

/** aggregate fields of "user" */
export type User_Aggregate_Fields = {
  count?: Maybe<Scalars['Int']>;
  max?: Maybe<User_Max_Fields>;
  min?: Maybe<User_Min_Fields>;
};


/** aggregate fields of "user" */
export type User_Aggregate_FieldsCountArgs = {
  columns?: Maybe<Array<User_Select_Column>>;
  distinct?: Maybe<Scalars['Boolean']>;
};

/** order by aggregate values of table "user" */
export type User_Aggregate_Order_By = {
  count?: Maybe<Order_By>;
  max?: Maybe<User_Max_Order_By>;
  min?: Maybe<User_Min_Order_By>;
};

/** input type for inserting array relation for remote table "user" */
export type User_Arr_Rel_Insert_Input = {
  data: Array<User_Insert_Input>;
  on_conflict?: Maybe<User_On_Conflict>;
};

/** Boolean expression to filter rows from the table "user". All fields are combined with a logical 'AND'. */
export type User_Bool_Exp = {
  _and?: Maybe<Array<Maybe<User_Bool_Exp>>>;
  _not?: Maybe<User_Bool_Exp>;
  _or?: Maybe<Array<Maybe<User_Bool_Exp>>>;
  avatar?: Maybe<String_Comparison_Exp>;
  bot?: Maybe<Boolean_Comparison_Exp>;
  guilds?: Maybe<Guild_Bool_Exp>;
  id?: Maybe<String_Comparison_Exp>;
  locale?: Maybe<String_Comparison_Exp>;
  members?: Maybe<Member_Bool_Exp>;
  messages?: Maybe<Message_Bool_Exp>;
  presences?: Maybe<User_Presence_Bool_Exp>;
  tag?: Maybe<String_Comparison_Exp>;
  user_created_at?: Maybe<Timestamptz_Comparison_Exp>;
  username?: Maybe<String_Comparison_Exp>;
};

/** unique or primary key constraints on table "user" */
export enum User_Constraint {
  /** unique or primary key constraint */
  UserPkey = 'user_pkey'
}

/** input type for inserting data into table "user" */
export type User_Insert_Input = {
  avatar?: Maybe<Scalars['String']>;
  bot?: Maybe<Scalars['Boolean']>;
  guilds?: Maybe<Guild_Arr_Rel_Insert_Input>;
  id?: Maybe<Scalars['String']>;
  locale?: Maybe<Scalars['String']>;
  members?: Maybe<Member_Arr_Rel_Insert_Input>;
  messages?: Maybe<Message_Arr_Rel_Insert_Input>;
  presences?: Maybe<User_Presence_Arr_Rel_Insert_Input>;
  tag?: Maybe<Scalars['String']>;
  user_created_at?: Maybe<Scalars['timestamptz']>;
  username?: Maybe<Scalars['String']>;
};

/** aggregate max on columns */
export type User_Max_Fields = {
  avatar?: Maybe<Scalars['String']>;
  id?: Maybe<Scalars['String']>;
  locale?: Maybe<Scalars['String']>;
  tag?: Maybe<Scalars['String']>;
  user_created_at?: Maybe<Scalars['timestamptz']>;
  username?: Maybe<Scalars['String']>;
};

/** order by max() on columns of table "user" */
export type User_Max_Order_By = {
  avatar?: Maybe<Order_By>;
  id?: Maybe<Order_By>;
  locale?: Maybe<Order_By>;
  tag?: Maybe<Order_By>;
  user_created_at?: Maybe<Order_By>;
  username?: Maybe<Order_By>;
};

/** aggregate min on columns */
export type User_Min_Fields = {
  avatar?: Maybe<Scalars['String']>;
  id?: Maybe<Scalars['String']>;
  locale?: Maybe<Scalars['String']>;
  tag?: Maybe<Scalars['String']>;
  user_created_at?: Maybe<Scalars['timestamptz']>;
  username?: Maybe<Scalars['String']>;
};

/** order by min() on columns of table "user" */
export type User_Min_Order_By = {
  avatar?: Maybe<Order_By>;
  id?: Maybe<Order_By>;
  locale?: Maybe<Order_By>;
  tag?: Maybe<Order_By>;
  user_created_at?: Maybe<Order_By>;
  username?: Maybe<Order_By>;
};

/** response of any mutation on the table "user" */
export type User_Mutation_Response = {
  /** number of affected rows by the mutation */
  affected_rows: Scalars['Int'];
  /** data of the affected rows by the mutation */
  returning: Array<User>;
};

/** input type for inserting object relation for remote table "user" */
export type User_Obj_Rel_Insert_Input = {
  data: User_Insert_Input;
  on_conflict?: Maybe<User_On_Conflict>;
};

/** on conflict condition type for table "user" */
export type User_On_Conflict = {
  constraint: User_Constraint;
  update_columns: Array<User_Update_Column>;
  where?: Maybe<User_Bool_Exp>;
};

/** ordering options when selecting data from "user" */
export type User_Order_By = {
  avatar?: Maybe<Order_By>;
  bot?: Maybe<Order_By>;
  guilds_aggregate?: Maybe<Guild_Aggregate_Order_By>;
  id?: Maybe<Order_By>;
  locale?: Maybe<Order_By>;
  members_aggregate?: Maybe<Member_Aggregate_Order_By>;
  messages_aggregate?: Maybe<Message_Aggregate_Order_By>;
  presences_aggregate?: Maybe<User_Presence_Aggregate_Order_By>;
  tag?: Maybe<Order_By>;
  user_created_at?: Maybe<Order_By>;
  username?: Maybe<Order_By>;
};

/** primary key columns input for table: "user" */
export type User_Pk_Columns_Input = {
  id: Scalars['String'];
};

/** columns and relationships of "user_presence" */
export type User_Presence = {
  /** An array relationship */
  activities: Array<User_Presence_Activity>;
  /** An aggregated array relationship */
  activities_aggregate: User_Presence_Activity_Aggregate;
  client_status_desktop?: Maybe<Scalars['String']>;
  client_status_mobile?: Maybe<Scalars['String']>;
  client_status_web?: Maybe<Scalars['String']>;
  guild_id?: Maybe<Scalars['String']>;
  id: Scalars['uuid'];
  status: Scalars['String'];
  timestamp?: Maybe<Scalars['timestamptz']>;
  /** An object relationship */
  user: User;
  user_id: Scalars['String'];
};


/** columns and relationships of "user_presence" */
export type User_PresenceActivitiesArgs = {
  distinct_on?: Maybe<Array<User_Presence_Activity_Select_Column>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<User_Presence_Activity_Order_By>>;
  where?: Maybe<User_Presence_Activity_Bool_Exp>;
};


/** columns and relationships of "user_presence" */
export type User_PresenceActivities_AggregateArgs = {
  distinct_on?: Maybe<Array<User_Presence_Activity_Select_Column>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<User_Presence_Activity_Order_By>>;
  where?: Maybe<User_Presence_Activity_Bool_Exp>;
};

/** columns and relationships of "user_presence_activity" */
export type User_Presence_Activity = {
  application_id?: Maybe<Scalars['String']>;
  created_at: Scalars['timestamptz'];
  details?: Maybe<Scalars['String']>;
  emoji?: Maybe<Scalars['String']>;
  end?: Maybe<Scalars['timestamptz']>;
  id: Scalars['uuid'];
  name: Scalars['String'];
  party_id?: Maybe<Scalars['String']>;
  party_size?: Maybe<Scalars['Int']>;
  /** An object relationship */
  presence: User_Presence;
  start?: Maybe<Scalars['timestamptz']>;
  state?: Maybe<Scalars['String']>;
  type: Scalars['String'];
  user_presence_id: Scalars['uuid'];
};

/** aggregated selection of "user_presence_activity" */
export type User_Presence_Activity_Aggregate = {
  aggregate?: Maybe<User_Presence_Activity_Aggregate_Fields>;
  nodes: Array<User_Presence_Activity>;
};

/** aggregate fields of "user_presence_activity" */
export type User_Presence_Activity_Aggregate_Fields = {
  avg?: Maybe<User_Presence_Activity_Avg_Fields>;
  count?: Maybe<Scalars['Int']>;
  max?: Maybe<User_Presence_Activity_Max_Fields>;
  min?: Maybe<User_Presence_Activity_Min_Fields>;
  stddev?: Maybe<User_Presence_Activity_Stddev_Fields>;
  stddev_pop?: Maybe<User_Presence_Activity_Stddev_Pop_Fields>;
  stddev_samp?: Maybe<User_Presence_Activity_Stddev_Samp_Fields>;
  sum?: Maybe<User_Presence_Activity_Sum_Fields>;
  var_pop?: Maybe<User_Presence_Activity_Var_Pop_Fields>;
  var_samp?: Maybe<User_Presence_Activity_Var_Samp_Fields>;
  variance?: Maybe<User_Presence_Activity_Variance_Fields>;
};


/** aggregate fields of "user_presence_activity" */
export type User_Presence_Activity_Aggregate_FieldsCountArgs = {
  columns?: Maybe<Array<User_Presence_Activity_Select_Column>>;
  distinct?: Maybe<Scalars['Boolean']>;
};

/** order by aggregate values of table "user_presence_activity" */
export type User_Presence_Activity_Aggregate_Order_By = {
  avg?: Maybe<User_Presence_Activity_Avg_Order_By>;
  count?: Maybe<Order_By>;
  max?: Maybe<User_Presence_Activity_Max_Order_By>;
  min?: Maybe<User_Presence_Activity_Min_Order_By>;
  stddev?: Maybe<User_Presence_Activity_Stddev_Order_By>;
  stddev_pop?: Maybe<User_Presence_Activity_Stddev_Pop_Order_By>;
  stddev_samp?: Maybe<User_Presence_Activity_Stddev_Samp_Order_By>;
  sum?: Maybe<User_Presence_Activity_Sum_Order_By>;
  var_pop?: Maybe<User_Presence_Activity_Var_Pop_Order_By>;
  var_samp?: Maybe<User_Presence_Activity_Var_Samp_Order_By>;
  variance?: Maybe<User_Presence_Activity_Variance_Order_By>;
};

/** input type for inserting array relation for remote table "user_presence_activity" */
export type User_Presence_Activity_Arr_Rel_Insert_Input = {
  data: Array<User_Presence_Activity_Insert_Input>;
  on_conflict?: Maybe<User_Presence_Activity_On_Conflict>;
};

/** aggregate avg on columns */
export type User_Presence_Activity_Avg_Fields = {
  party_size?: Maybe<Scalars['Float']>;
};

/** order by avg() on columns of table "user_presence_activity" */
export type User_Presence_Activity_Avg_Order_By = {
  party_size?: Maybe<Order_By>;
};

/** Boolean expression to filter rows from the table "user_presence_activity". All fields are combined with a logical 'AND'. */
export type User_Presence_Activity_Bool_Exp = {
  _and?: Maybe<Array<Maybe<User_Presence_Activity_Bool_Exp>>>;
  _not?: Maybe<User_Presence_Activity_Bool_Exp>;
  _or?: Maybe<Array<Maybe<User_Presence_Activity_Bool_Exp>>>;
  application_id?: Maybe<String_Comparison_Exp>;
  created_at?: Maybe<Timestamptz_Comparison_Exp>;
  details?: Maybe<String_Comparison_Exp>;
  emoji?: Maybe<String_Comparison_Exp>;
  end?: Maybe<Timestamptz_Comparison_Exp>;
  id?: Maybe<Uuid_Comparison_Exp>;
  name?: Maybe<String_Comparison_Exp>;
  party_id?: Maybe<String_Comparison_Exp>;
  party_size?: Maybe<Int_Comparison_Exp>;
  presence?: Maybe<User_Presence_Bool_Exp>;
  start?: Maybe<Timestamptz_Comparison_Exp>;
  state?: Maybe<String_Comparison_Exp>;
  type?: Maybe<String_Comparison_Exp>;
  user_presence_id?: Maybe<Uuid_Comparison_Exp>;
};

/** unique or primary key constraints on table "user_presence_activity" */
export enum User_Presence_Activity_Constraint {
  /** unique or primary key constraint */
  UserPresenceActivityPkey = 'user_presence_activity_pkey'
}

/** input type for incrementing integer column in table "user_presence_activity" */
export type User_Presence_Activity_Inc_Input = {
  party_size?: Maybe<Scalars['Int']>;
};

/** input type for inserting data into table "user_presence_activity" */
export type User_Presence_Activity_Insert_Input = {
  application_id?: Maybe<Scalars['String']>;
  created_at?: Maybe<Scalars['timestamptz']>;
  details?: Maybe<Scalars['String']>;
  emoji?: Maybe<Scalars['String']>;
  end?: Maybe<Scalars['timestamptz']>;
  id?: Maybe<Scalars['uuid']>;
  name?: Maybe<Scalars['String']>;
  party_id?: Maybe<Scalars['String']>;
  party_size?: Maybe<Scalars['Int']>;
  presence?: Maybe<User_Presence_Obj_Rel_Insert_Input>;
  start?: Maybe<Scalars['timestamptz']>;
  state?: Maybe<Scalars['String']>;
  type?: Maybe<Scalars['String']>;
  user_presence_id?: Maybe<Scalars['uuid']>;
};

/** aggregate max on columns */
export type User_Presence_Activity_Max_Fields = {
  application_id?: Maybe<Scalars['String']>;
  created_at?: Maybe<Scalars['timestamptz']>;
  details?: Maybe<Scalars['String']>;
  emoji?: Maybe<Scalars['String']>;
  end?: Maybe<Scalars['timestamptz']>;
  id?: Maybe<Scalars['uuid']>;
  name?: Maybe<Scalars['String']>;
  party_id?: Maybe<Scalars['String']>;
  party_size?: Maybe<Scalars['Int']>;
  start?: Maybe<Scalars['timestamptz']>;
  state?: Maybe<Scalars['String']>;
  type?: Maybe<Scalars['String']>;
  user_presence_id?: Maybe<Scalars['uuid']>;
};

/** order by max() on columns of table "user_presence_activity" */
export type User_Presence_Activity_Max_Order_By = {
  application_id?: Maybe<Order_By>;
  created_at?: Maybe<Order_By>;
  details?: Maybe<Order_By>;
  emoji?: Maybe<Order_By>;
  end?: Maybe<Order_By>;
  id?: Maybe<Order_By>;
  name?: Maybe<Order_By>;
  party_id?: Maybe<Order_By>;
  party_size?: Maybe<Order_By>;
  start?: Maybe<Order_By>;
  state?: Maybe<Order_By>;
  type?: Maybe<Order_By>;
  user_presence_id?: Maybe<Order_By>;
};

/** aggregate min on columns */
export type User_Presence_Activity_Min_Fields = {
  application_id?: Maybe<Scalars['String']>;
  created_at?: Maybe<Scalars['timestamptz']>;
  details?: Maybe<Scalars['String']>;
  emoji?: Maybe<Scalars['String']>;
  end?: Maybe<Scalars['timestamptz']>;
  id?: Maybe<Scalars['uuid']>;
  name?: Maybe<Scalars['String']>;
  party_id?: Maybe<Scalars['String']>;
  party_size?: Maybe<Scalars['Int']>;
  start?: Maybe<Scalars['timestamptz']>;
  state?: Maybe<Scalars['String']>;
  type?: Maybe<Scalars['String']>;
  user_presence_id?: Maybe<Scalars['uuid']>;
};

/** order by min() on columns of table "user_presence_activity" */
export type User_Presence_Activity_Min_Order_By = {
  application_id?: Maybe<Order_By>;
  created_at?: Maybe<Order_By>;
  details?: Maybe<Order_By>;
  emoji?: Maybe<Order_By>;
  end?: Maybe<Order_By>;
  id?: Maybe<Order_By>;
  name?: Maybe<Order_By>;
  party_id?: Maybe<Order_By>;
  party_size?: Maybe<Order_By>;
  start?: Maybe<Order_By>;
  state?: Maybe<Order_By>;
  type?: Maybe<Order_By>;
  user_presence_id?: Maybe<Order_By>;
};

/** response of any mutation on the table "user_presence_activity" */
export type User_Presence_Activity_Mutation_Response = {
  /** number of affected rows by the mutation */
  affected_rows: Scalars['Int'];
  /** data of the affected rows by the mutation */
  returning: Array<User_Presence_Activity>;
};

/** input type for inserting object relation for remote table "user_presence_activity" */
export type User_Presence_Activity_Obj_Rel_Insert_Input = {
  data: User_Presence_Activity_Insert_Input;
  on_conflict?: Maybe<User_Presence_Activity_On_Conflict>;
};

/** on conflict condition type for table "user_presence_activity" */
export type User_Presence_Activity_On_Conflict = {
  constraint: User_Presence_Activity_Constraint;
  update_columns: Array<User_Presence_Activity_Update_Column>;
  where?: Maybe<User_Presence_Activity_Bool_Exp>;
};

/** ordering options when selecting data from "user_presence_activity" */
export type User_Presence_Activity_Order_By = {
  application_id?: Maybe<Order_By>;
  created_at?: Maybe<Order_By>;
  details?: Maybe<Order_By>;
  emoji?: Maybe<Order_By>;
  end?: Maybe<Order_By>;
  id?: Maybe<Order_By>;
  name?: Maybe<Order_By>;
  party_id?: Maybe<Order_By>;
  party_size?: Maybe<Order_By>;
  presence?: Maybe<User_Presence_Order_By>;
  start?: Maybe<Order_By>;
  state?: Maybe<Order_By>;
  type?: Maybe<Order_By>;
  user_presence_id?: Maybe<Order_By>;
};

/** primary key columns input for table: "user_presence_activity" */
export type User_Presence_Activity_Pk_Columns_Input = {
  id: Scalars['uuid'];
};

/** select columns of table "user_presence_activity" */
export enum User_Presence_Activity_Select_Column {
  /** column name */
  ApplicationId = 'application_id',
  /** column name */
  CreatedAt = 'created_at',
  /** column name */
  Details = 'details',
  /** column name */
  Emoji = 'emoji',
  /** column name */
  End = 'end',
  /** column name */
  Id = 'id',
  /** column name */
  Name = 'name',
  /** column name */
  PartyId = 'party_id',
  /** column name */
  PartySize = 'party_size',
  /** column name */
  Start = 'start',
  /** column name */
  State = 'state',
  /** column name */
  Type = 'type',
  /** column name */
  UserPresenceId = 'user_presence_id'
}

/** input type for updating data in table "user_presence_activity" */
export type User_Presence_Activity_Set_Input = {
  application_id?: Maybe<Scalars['String']>;
  created_at?: Maybe<Scalars['timestamptz']>;
  details?: Maybe<Scalars['String']>;
  emoji?: Maybe<Scalars['String']>;
  end?: Maybe<Scalars['timestamptz']>;
  id?: Maybe<Scalars['uuid']>;
  name?: Maybe<Scalars['String']>;
  party_id?: Maybe<Scalars['String']>;
  party_size?: Maybe<Scalars['Int']>;
  start?: Maybe<Scalars['timestamptz']>;
  state?: Maybe<Scalars['String']>;
  type?: Maybe<Scalars['String']>;
  user_presence_id?: Maybe<Scalars['uuid']>;
};

/** aggregate stddev on columns */
export type User_Presence_Activity_Stddev_Fields = {
  party_size?: Maybe<Scalars['Float']>;
};

/** order by stddev() on columns of table "user_presence_activity" */
export type User_Presence_Activity_Stddev_Order_By = {
  party_size?: Maybe<Order_By>;
};

/** aggregate stddev_pop on columns */
export type User_Presence_Activity_Stddev_Pop_Fields = {
  party_size?: Maybe<Scalars['Float']>;
};

/** order by stddev_pop() on columns of table "user_presence_activity" */
export type User_Presence_Activity_Stddev_Pop_Order_By = {
  party_size?: Maybe<Order_By>;
};

/** aggregate stddev_samp on columns */
export type User_Presence_Activity_Stddev_Samp_Fields = {
  party_size?: Maybe<Scalars['Float']>;
};

/** order by stddev_samp() on columns of table "user_presence_activity" */
export type User_Presence_Activity_Stddev_Samp_Order_By = {
  party_size?: Maybe<Order_By>;
};

/** aggregate sum on columns */
export type User_Presence_Activity_Sum_Fields = {
  party_size?: Maybe<Scalars['Int']>;
};

/** order by sum() on columns of table "user_presence_activity" */
export type User_Presence_Activity_Sum_Order_By = {
  party_size?: Maybe<Order_By>;
};

/** update columns of table "user_presence_activity" */
export enum User_Presence_Activity_Update_Column {
  /** column name */
  ApplicationId = 'application_id',
  /** column name */
  CreatedAt = 'created_at',
  /** column name */
  Details = 'details',
  /** column name */
  Emoji = 'emoji',
  /** column name */
  End = 'end',
  /** column name */
  Id = 'id',
  /** column name */
  Name = 'name',
  /** column name */
  PartyId = 'party_id',
  /** column name */
  PartySize = 'party_size',
  /** column name */
  Start = 'start',
  /** column name */
  State = 'state',
  /** column name */
  Type = 'type',
  /** column name */
  UserPresenceId = 'user_presence_id'
}

/** aggregate var_pop on columns */
export type User_Presence_Activity_Var_Pop_Fields = {
  party_size?: Maybe<Scalars['Float']>;
};

/** order by var_pop() on columns of table "user_presence_activity" */
export type User_Presence_Activity_Var_Pop_Order_By = {
  party_size?: Maybe<Order_By>;
};

/** aggregate var_samp on columns */
export type User_Presence_Activity_Var_Samp_Fields = {
  party_size?: Maybe<Scalars['Float']>;
};

/** order by var_samp() on columns of table "user_presence_activity" */
export type User_Presence_Activity_Var_Samp_Order_By = {
  party_size?: Maybe<Order_By>;
};

/** aggregate variance on columns */
export type User_Presence_Activity_Variance_Fields = {
  party_size?: Maybe<Scalars['Float']>;
};

/** order by variance() on columns of table "user_presence_activity" */
export type User_Presence_Activity_Variance_Order_By = {
  party_size?: Maybe<Order_By>;
};

/** aggregated selection of "user_presence" */
export type User_Presence_Aggregate = {
  aggregate?: Maybe<User_Presence_Aggregate_Fields>;
  nodes: Array<User_Presence>;
};

/** aggregate fields of "user_presence" */
export type User_Presence_Aggregate_Fields = {
  count?: Maybe<Scalars['Int']>;
  max?: Maybe<User_Presence_Max_Fields>;
  min?: Maybe<User_Presence_Min_Fields>;
};


/** aggregate fields of "user_presence" */
export type User_Presence_Aggregate_FieldsCountArgs = {
  columns?: Maybe<Array<User_Presence_Select_Column>>;
  distinct?: Maybe<Scalars['Boolean']>;
};

/** order by aggregate values of table "user_presence" */
export type User_Presence_Aggregate_Order_By = {
  count?: Maybe<Order_By>;
  max?: Maybe<User_Presence_Max_Order_By>;
  min?: Maybe<User_Presence_Min_Order_By>;
};

/** input type for inserting array relation for remote table "user_presence" */
export type User_Presence_Arr_Rel_Insert_Input = {
  data: Array<User_Presence_Insert_Input>;
  on_conflict?: Maybe<User_Presence_On_Conflict>;
};

/** Boolean expression to filter rows from the table "user_presence". All fields are combined with a logical 'AND'. */
export type User_Presence_Bool_Exp = {
  _and?: Maybe<Array<Maybe<User_Presence_Bool_Exp>>>;
  _not?: Maybe<User_Presence_Bool_Exp>;
  _or?: Maybe<Array<Maybe<User_Presence_Bool_Exp>>>;
  activities?: Maybe<User_Presence_Activity_Bool_Exp>;
  client_status_desktop?: Maybe<String_Comparison_Exp>;
  client_status_mobile?: Maybe<String_Comparison_Exp>;
  client_status_web?: Maybe<String_Comparison_Exp>;
  guild_id?: Maybe<String_Comparison_Exp>;
  id?: Maybe<Uuid_Comparison_Exp>;
  status?: Maybe<String_Comparison_Exp>;
  timestamp?: Maybe<Timestamptz_Comparison_Exp>;
  user?: Maybe<User_Bool_Exp>;
  user_id?: Maybe<String_Comparison_Exp>;
};

/** unique or primary key constraints on table "user_presence" */
export enum User_Presence_Constraint {
  /** unique or primary key constraint */
  UserPresencePkey = 'user_presence_pkey'
}

/** input type for inserting data into table "user_presence" */
export type User_Presence_Insert_Input = {
  activities?: Maybe<User_Presence_Activity_Arr_Rel_Insert_Input>;
  client_status_desktop?: Maybe<Scalars['String']>;
  client_status_mobile?: Maybe<Scalars['String']>;
  client_status_web?: Maybe<Scalars['String']>;
  guild_id?: Maybe<Scalars['String']>;
  id?: Maybe<Scalars['uuid']>;
  status?: Maybe<Scalars['String']>;
  timestamp?: Maybe<Scalars['timestamptz']>;
  user?: Maybe<User_Obj_Rel_Insert_Input>;
  user_id?: Maybe<Scalars['String']>;
};

/** aggregate max on columns */
export type User_Presence_Max_Fields = {
  client_status_desktop?: Maybe<Scalars['String']>;
  client_status_mobile?: Maybe<Scalars['String']>;
  client_status_web?: Maybe<Scalars['String']>;
  guild_id?: Maybe<Scalars['String']>;
  id?: Maybe<Scalars['uuid']>;
  status?: Maybe<Scalars['String']>;
  timestamp?: Maybe<Scalars['timestamptz']>;
  user_id?: Maybe<Scalars['String']>;
};

/** order by max() on columns of table "user_presence" */
export type User_Presence_Max_Order_By = {
  client_status_desktop?: Maybe<Order_By>;
  client_status_mobile?: Maybe<Order_By>;
  client_status_web?: Maybe<Order_By>;
  guild_id?: Maybe<Order_By>;
  id?: Maybe<Order_By>;
  status?: Maybe<Order_By>;
  timestamp?: Maybe<Order_By>;
  user_id?: Maybe<Order_By>;
};

/** aggregate min on columns */
export type User_Presence_Min_Fields = {
  client_status_desktop?: Maybe<Scalars['String']>;
  client_status_mobile?: Maybe<Scalars['String']>;
  client_status_web?: Maybe<Scalars['String']>;
  guild_id?: Maybe<Scalars['String']>;
  id?: Maybe<Scalars['uuid']>;
  status?: Maybe<Scalars['String']>;
  timestamp?: Maybe<Scalars['timestamptz']>;
  user_id?: Maybe<Scalars['String']>;
};

/** order by min() on columns of table "user_presence" */
export type User_Presence_Min_Order_By = {
  client_status_desktop?: Maybe<Order_By>;
  client_status_mobile?: Maybe<Order_By>;
  client_status_web?: Maybe<Order_By>;
  guild_id?: Maybe<Order_By>;
  id?: Maybe<Order_By>;
  status?: Maybe<Order_By>;
  timestamp?: Maybe<Order_By>;
  user_id?: Maybe<Order_By>;
};

/** response of any mutation on the table "user_presence" */
export type User_Presence_Mutation_Response = {
  /** number of affected rows by the mutation */
  affected_rows: Scalars['Int'];
  /** data of the affected rows by the mutation */
  returning: Array<User_Presence>;
};

/** input type for inserting object relation for remote table "user_presence" */
export type User_Presence_Obj_Rel_Insert_Input = {
  data: User_Presence_Insert_Input;
  on_conflict?: Maybe<User_Presence_On_Conflict>;
};

/** on conflict condition type for table "user_presence" */
export type User_Presence_On_Conflict = {
  constraint: User_Presence_Constraint;
  update_columns: Array<User_Presence_Update_Column>;
  where?: Maybe<User_Presence_Bool_Exp>;
};

/** ordering options when selecting data from "user_presence" */
export type User_Presence_Order_By = {
  activities_aggregate?: Maybe<User_Presence_Activity_Aggregate_Order_By>;
  client_status_desktop?: Maybe<Order_By>;
  client_status_mobile?: Maybe<Order_By>;
  client_status_web?: Maybe<Order_By>;
  guild_id?: Maybe<Order_By>;
  id?: Maybe<Order_By>;
  status?: Maybe<Order_By>;
  timestamp?: Maybe<Order_By>;
  user?: Maybe<User_Order_By>;
  user_id?: Maybe<Order_By>;
};

/** primary key columns input for table: "user_presence" */
export type User_Presence_Pk_Columns_Input = {
  id: Scalars['uuid'];
};

/** select columns of table "user_presence" */
export enum User_Presence_Select_Column {
  /** column name */
  ClientStatusDesktop = 'client_status_desktop',
  /** column name */
  ClientStatusMobile = 'client_status_mobile',
  /** column name */
  ClientStatusWeb = 'client_status_web',
  /** column name */
  GuildId = 'guild_id',
  /** column name */
  Id = 'id',
  /** column name */
  Status = 'status',
  /** column name */
  Timestamp = 'timestamp',
  /** column name */
  UserId = 'user_id'
}

/** input type for updating data in table "user_presence" */
export type User_Presence_Set_Input = {
  client_status_desktop?: Maybe<Scalars['String']>;
  client_status_mobile?: Maybe<Scalars['String']>;
  client_status_web?: Maybe<Scalars['String']>;
  guild_id?: Maybe<Scalars['String']>;
  id?: Maybe<Scalars['uuid']>;
  status?: Maybe<Scalars['String']>;
  timestamp?: Maybe<Scalars['timestamptz']>;
  user_id?: Maybe<Scalars['String']>;
};

/** update columns of table "user_presence" */
export enum User_Presence_Update_Column {
  /** column name */
  ClientStatusDesktop = 'client_status_desktop',
  /** column name */
  ClientStatusMobile = 'client_status_mobile',
  /** column name */
  ClientStatusWeb = 'client_status_web',
  /** column name */
  GuildId = 'guild_id',
  /** column name */
  Id = 'id',
  /** column name */
  Status = 'status',
  /** column name */
  Timestamp = 'timestamp',
  /** column name */
  UserId = 'user_id'
}

/** select columns of table "user" */
export enum User_Select_Column {
  /** column name */
  Avatar = 'avatar',
  /** column name */
  Bot = 'bot',
  /** column name */
  Id = 'id',
  /** column name */
  Locale = 'locale',
  /** column name */
  Tag = 'tag',
  /** column name */
  UserCreatedAt = 'user_created_at',
  /** column name */
  Username = 'username'
}

/** input type for updating data in table "user" */
export type User_Set_Input = {
  avatar?: Maybe<Scalars['String']>;
  bot?: Maybe<Scalars['Boolean']>;
  id?: Maybe<Scalars['String']>;
  locale?: Maybe<Scalars['String']>;
  tag?: Maybe<Scalars['String']>;
  user_created_at?: Maybe<Scalars['timestamptz']>;
  username?: Maybe<Scalars['String']>;
};

/** update columns of table "user" */
export enum User_Update_Column {
  /** column name */
  Avatar = 'avatar',
  /** column name */
  Bot = 'bot',
  /** column name */
  Id = 'id',
  /** column name */
  Locale = 'locale',
  /** column name */
  Tag = 'tag',
  /** column name */
  UserCreatedAt = 'user_created_at',
  /** column name */
  Username = 'username'
}


/** expression to compare columns of type uuid. All fields are combined with logical 'AND'. */
export type Uuid_Comparison_Exp = {
  _eq?: Maybe<Scalars['uuid']>;
  _gt?: Maybe<Scalars['uuid']>;
  _gte?: Maybe<Scalars['uuid']>;
  _in?: Maybe<Array<Scalars['uuid']>>;
  _is_null?: Maybe<Scalars['Boolean']>;
  _lt?: Maybe<Scalars['uuid']>;
  _lte?: Maybe<Scalars['uuid']>;
  _neq?: Maybe<Scalars['uuid']>;
  _nin?: Maybe<Array<Scalars['uuid']>>;
};

export type GetActivityQueryVariables = Exact<{ [key: string]: never; }>;


export type GetActivityQuery = { user_presence: Array<(
    Pick<User_Presence, 'client_status_desktop' | 'client_status_mobile' | 'client_status_web' | 'guild_id' | 'id' | 'status' | 'timestamp' | 'user_id'>
    & { activities: Array<Pick<User_Presence_Activity, 'user_presence_id' | 'type' | 'state' | 'start' | 'party_size' | 'party_id' | 'name' | 'id' | 'end' | 'emoji' | 'details' | 'application_id' | 'created_at'>> }
  )> };

export type GetGuildsQueryVariables = Exact<{ [key: string]: never; }>;


export type GetGuildsQuery = { guild: Array<Pick<Guild, 'id' | 'name'>> };


export const GetActivityDocument = gql`
    query GetActivity {
  user_presence(order_by: {timestamp: asc}) {
    activities {
      user_presence_id
      type
      state
      start
      party_size
      party_id
      name
      id
      end
      emoji
      details
      application_id
      created_at
    }
    client_status_desktop
    client_status_mobile
    client_status_web
    guild_id
    id
    status
    timestamp
    user_id
  }
}
    `;

/**
 * __useGetActivityQuery__
 *
 * To run a query within a React component, call `useGetActivityQuery` and pass it any options that fit your needs.
 * When your component renders, `useGetActivityQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useGetActivityQuery({
 *   variables: {
 *   },
 * });
 */
export function useGetActivityQuery(baseOptions?: Apollo.QueryHookOptions<GetActivityQuery, GetActivityQueryVariables>) {
        return Apollo.useQuery<GetActivityQuery, GetActivityQueryVariables>(GetActivityDocument, baseOptions);
      }
export function useGetActivityLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<GetActivityQuery, GetActivityQueryVariables>) {
          return Apollo.useLazyQuery<GetActivityQuery, GetActivityQueryVariables>(GetActivityDocument, baseOptions);
        }
export type GetActivityQueryHookResult = ReturnType<typeof useGetActivityQuery>;
export type GetActivityLazyQueryHookResult = ReturnType<typeof useGetActivityLazyQuery>;
export type GetActivityQueryResult = Apollo.QueryResult<GetActivityQuery, GetActivityQueryVariables>;
export const GetGuildsDocument = gql`
    query GetGuilds {
  guild {
    id
    name
  }
}
    `;

/**
 * __useGetGuildsQuery__
 *
 * To run a query within a React component, call `useGetGuildsQuery` and pass it any options that fit your needs.
 * When your component renders, `useGetGuildsQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useGetGuildsQuery({
 *   variables: {
 *   },
 * });
 */
export function useGetGuildsQuery(baseOptions?: Apollo.QueryHookOptions<GetGuildsQuery, GetGuildsQueryVariables>) {
        return Apollo.useQuery<GetGuildsQuery, GetGuildsQueryVariables>(GetGuildsDocument, baseOptions);
      }
export function useGetGuildsLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<GetGuildsQuery, GetGuildsQueryVariables>) {
          return Apollo.useLazyQuery<GetGuildsQuery, GetGuildsQueryVariables>(GetGuildsDocument, baseOptions);
        }
export type GetGuildsQueryHookResult = ReturnType<typeof useGetGuildsQuery>;
export type GetGuildsLazyQueryHookResult = ReturnType<typeof useGetGuildsLazyQuery>;
export type GetGuildsQueryResult = Apollo.QueryResult<GetGuildsQuery, GetGuildsQueryVariables>;