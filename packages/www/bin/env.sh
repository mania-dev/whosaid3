#!/bin/bash

BASEDIR="$(dirname $0)"

DEFAULT_SOURCE="$BASEDIR/../.env"
DEFAULT_DESTINATION="$BASEDIR/../public/env-config.js"

SOURCE=${1:-$DEFAULT_SOURCE}
DESTINATION=${2:-$DEFAULT_DESTINATION}

echo source: $SOURCE
echo destination: $DESTINATION

# Recreate config file
rm -rf "$DESTINATION"
touch "$DESTINATION"

# Add assignment
echo "window._env_ = {" >> "$DESTINATION"

# Read each line in .env file
# Each line represents key=value pairs
while read -r line || [[ -n "$line" ]];
do
  # Split env variables by character `=`
  if printf '%s\n' "$line" | grep -q -e '='; then
    varname=$(printf '%s\n' "$line" | sed -e 's/=.*//')
    varvalue=$(printf '%s\n' "$line" | sed -e 's/^[^=]*=//')
  fi

  # Read value of current variable if exists as Environment variable
  value=$(printf '%s\n' "${!varname}")
  # Otherwise use value from .env file
  [[ -z $value ]] && value=${varvalue}

  # Append configuration property to JS file
  echo "  $varname: \"$value\"," >> "$DESTINATION"
done < "$SOURCE"

echo "}" >> "$DESTINATION"
