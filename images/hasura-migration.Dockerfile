FROM hasura/graphql-engine:v1.3.3.cli-migrations-v2

COPY ./packages/hasura/migrations /hasura-migrations
COPY ./packages/hasura/metadata /hasura-metadata

CMD ["echo", "Done"]
