FROM node:14-alpine as build

RUN yarn set version berry

COPY ./ /app/
WORKDIR /app/

RUN yarn install --immutable --inline-builds

RUN yarn workspaces foreach -t run build

WORKDIR /app/packages/scrapper

CMD [ "yarn", "run", "start:prod" ]

FROM nginx:1.17.9-alpine AS production
RUN apk add --no-cache bash

COPY --from=build /app/packages/www/nginx/default.conf /etc/nginx/conf.d
COPY --from=build /app/packages/www/bin/env.sh /home/node/
COPY --from=build /app/packages/www/.env /home/node/
COPY --from=build /app/packages/www/build /home/node/build

CMD ["/bin/bash", "-c", "/home/node/env.sh /home/node/.env /home/node/build/env-config.js && nginx -g \"daemon off;\""]
