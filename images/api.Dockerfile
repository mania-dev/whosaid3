FROM node:14-alpine

RUN yarn set version berry

COPY ./ /app/
WORKDIR /app/

RUN yarn install --immutable --inline-builds

RUN yarn workspaces foreach -t run build

WORKDIR /app/packages/api

CMD [ "yarn", "run", "start:prod" ]
